<?php

/************************GERAL***************************/

    function operadorPerfilConsultar($idOperador){
        $connection = abrirConnection();  
        $params = array($idOperador);
        $dados = executeSP("spOperadorPerfil_Consultar", $params, $connection);
        return $dados;
    }

    function operadorParceirosConsultar($idOperador){
        $connection = abrirConnection();  
        $params = array($idOperador);
        $dados = executeSP("spOperadorParceiros_Consultar", $params, $connection);
        return $dados;
    }


    function perfilConsultar($idOperador){
        $connection = abrirConnection();  
        $params = array($idOperador);
        $dados = executeSP("spPerfil_Consultar", $params, $connection);
        return $dados;
    }

    function operadorConsultar($idOperador){
        $connection = abrirConnection();  
        $params = array($idOperador);
        $dados = executeSP("spOperador_Consultar", $params, $connection);
        return $dados;
    }



/************************Login***************************/
    // Login
    function login($Operador, $senha ){
        $connection = abrirConnection();  
        $params = array($Operador, $senha);
        $dados = executeSP("spOperador_Autenticar", $params, $connection);
        return $dados;
    }

/******************Cadastro**************************/	

function operador($tipoAcao, $idParceiroCorrente, $idOperador, $nomeOperador, $eMail, $senhaOperador, $ativoOperador, $tipoNivel, $idParceiro, $idPerfil){
    $connection = abrirConnection();   
    $params = array($tipoAcao, $idParceiroCorrente, $idOperador, $nomeOperador, $eMail, $senhaOperador, $ativoOperador, $tipoNivel, $idParceiro, $idPerfil);        
    $dados = executeSP("spOperador", $params, $connection);
    return $dados;
}

function doacaoExterna($tipoAcao, $idDoacaoExterna, $numeroDoacoes, $quantidadeQuilos, $dataEvento){
    $connection = abrirConnection();   
    $params = array($tipoAcao, $idDoacaoExterna, $numeroDoacoes, $quantidadeQuilos, $dataEvento);        
    $dados = executeSP("spDoacaoExterna", $params, $connection);
    return $dados;
}

function medida($tipoAcao, $idMedida, $siglaMedida, $nomeMedida){
    $connection = abrirConnection();   
    $params = array($tipoAcao, $idMedida, $siglaMedida, $nomeMedida);        
    $dados = executeSP("spMedida", $params, $connection);
    return $dados;
}

function parceirosGrupo($tipoAcao, $idParceirosGrupo, $nomeParceirosGrupo, $ativoParceirosGrupo, $nomeImagem, $ordemParceiros){
    $connection = abrirConnection();   
    $params = array($tipoAcao, $idParceirosGrupo, $nomeParceirosGrupo, $ativoParceirosGrupo, $nomeImagem, $ordemParceiros);        
    $dados = executeSP("spParceirosGrupo", $params, $connection);
    return $dados;
}

function parceiros($tipoAcao, $idParceiro, $nomeParceiro, $ativoParceiro, $ativoRetirada, $textoEndereco, $nomeImagem, $dataEntrega, $horaEntregaInicial, $horaEntregaFinal, $idUnidade, $ativoTrocados, $ativoEntrega, $ordemVisual, $idParceiroGrupo, $ativoProducao, $ativoHome,
                    $idUnidadeMantenedora, $percentualMantenedora, $idUnidadeFrete, $valorFreteLocal, $diasEntregaLocal, $valorFreteOutros, $diasEntregaOutros, $codigoIbgeLocal, $numeroCelular, $tipoTransacao                    
                ){
    $connection = abrirConnection();   
    $params = array($tipoAcao, $idParceiro, $nomeParceiro, $ativoParceiro, $ativoRetirada, $textoEndereco, $nomeImagem, $dataEntrega, $horaEntregaInicial, $horaEntregaFinal, $idUnidade, $ativoTrocados, $ativoEntrega, $ordemVisual, $idParceiroGrupo, $ativoProducao, $ativoHome,
                    $idUnidadeMantenedora, $percentualMantenedora, $idUnidadeFrete, $valorFreteLocal, $diasEntregaLocal, $valorFreteOutros, $diasEntregaOutros, $codigoIbgeLocal, $numeroCelular, $tipoTransacao);        
    $dados = executeSP("spParceiros", $params, $connection);
    return $dados;
}

function unidadeConsultar($tipoAcao, $idUnidade, $nomeUnidade, $ativoProducao){
    $connection = abrirConnection();   
    $params = array($tipoAcao, $idUnidade, $nomeUnidade, $ativoProducao);
    $dados = executeSP("spUnidade_Consultar", $params, $connection);
    return $dados;
}

function categoria($tipoAcao, $idCategoria, $nomeCategoria, $ordemCategoria, $nomeImagem, $ativoCategoria, $ativoEvidencia){
    $connection = abrirConnection();   
    $params = array($tipoAcao, $idCategoria, $nomeCategoria, $ordemCategoria, $nomeImagem, $ativoCategoria, $ativoEvidencia);
    $dados = executeSP("spCategoria", $params, $connection);
    return $dados;
}

function produtoCategoria($tipoAcao, $idCategoria, $idProduto, $idParceiroCorrente){
    $connection = abrirConnection();   
    $params = array($tipoAcao, $idCategoria, $idProduto, $idParceiroCorrente);
    $dados = executeSP("spProdutoCategoria", $params, $connection);
    return $dados;
}

function produto($tipoAcao, $idParceiroCorrente, $idProduto, $nomeProduto, $tipoProdutoPesquisa, $nomeImagem, $nomeImagemSecundaria, $idParceiro, $textoDescricao, $ativoMostrarTelaPrincipal, $tipoProduto, $ativoProduto, $tipoGiftCard, $nomeOperadoraRecarga, $ativoControleCodigo, $textoInstrucao, $ativoEventoNatal, $quantidadeApresentacao, $idMedida, $quantidadeMinima, $qrCode, $ordem){
    $connection = abrirConnection();   
    $params = array($tipoAcao, $idParceiroCorrente, $idProduto, $nomeProduto, $tipoProdutoPesquisa, $nomeImagem, $nomeImagemSecundaria, $idParceiro, $textoDescricao, $ativoMostrarTelaPrincipal, $tipoProduto, $ativoProduto, $tipoGiftCard, $nomeOperadoraRecarga, $ativoControleCodigo, $textoInstrucao, $ativoEventoNatal, $quantidadeApresentacao, $idMedida, $quantidadeMinima, $qrCode, $ordem);
    $dados = executeSP("spProduto", $params, $connection);
    return $dados;
}

function parceirosSelecionar($idParceiros){
    $connection = abrirConnection();   
    $params = array($idParceiros);
    $dados = executeSP("spParceiros_Selecionar", $params, $connection);
    return $dados;
}

function produtoImagem($tipoAcao, $idImagem, $idProduto, $nomeImagem, $nomeImagemSecundaria){
    $connection = abrirConnection();   
    $params = array($tipoAcao, $idImagem, $idProduto, $nomeImagem, $nomeImagemSecundaria);
    $dados = executeSP("spProdutoImagem", $params, $connection);
    return $dados;
}


function perfil($tipoAcao,$idPerfil,$nomePerfil, $ativoDashboard, $ativoCategoria,$ativoGrupoParceiros,$ativoMedidas,$ativoOperadores,
                $ativoParceiros,$ativoProdutos, $ativoProdutoPorCategoria, $ativoValidarCompra,$ativoHistoricoValidacao,$ativoComprasRealizadas,$ativoSaldoUnidades,$ativoEstoqueNotas,
                $ativoConsultaEstoqueDisponivel,$ativoPerfil,$ativoCompleto){
    $connection = abrirConnection();  
    $params = array($tipoAcao,$idPerfil,$nomePerfil, $ativoDashboard, $ativoCategoria,$ativoGrupoParceiros,$ativoMedidas,$ativoOperadores,
                    $ativoParceiros,$ativoProdutos, $ativoProdutoPorCategoria, $ativoValidarCompra,$ativoHistoricoValidacao,$ativoComprasRealizadas,$ativoSaldoUnidades,$ativoEstoqueNotas,
                    $ativoConsultaEstoqueDisponivel,$ativoPerfil,$ativoCompleto);
    $dados = executeSP("spPerfil", $params, $connection);
    return $dados;
}


/******************Estoque**************************/	




function estoqueNota($tipoAcao, $idEstoqueNota, $idOperador, $tipoNota, $textoDescricao, $dataInicio, $dataFinal, $idParceiro, $idParceiroCorrente){
    $connection = abrirConnection();   
    $params = array($tipoAcao, $idEstoqueNota, $idOperador, $tipoNota, $textoDescricao, $dataInicio, $dataFinal, $idParceiro, $idParceiroCorrente);
    $dados = executeSP("spEstoqueNota", $params, $connection);
    return $dados;
}

function estoqueNotaProduto($tipoAcao, $idEstoqueNota, $idProduto, $valorProduto, $percentualDesconto, $quantidadeProduto){
    $connection = abrirConnection();   
    $params = array($tipoAcao, $idEstoqueNota, $idProduto, $valorProduto, $percentualDesconto, $quantidadeProduto);
    $dados = executeSP("spEstoqueNotaProduto", $params, $connection);
    return $dados;
}

function estoqueDisponivelConsultar($tipoAcao, $idProduto, $nomeProduto, $percentualDesconto, $quantidadeDisponivel, $valorProduto, $idParceiroCorrente){
    $connection = abrirConnection();   
    $params = array($tipoAcao, $idProduto, $nomeProduto, $percentualDesconto, $quantidadeDisponivel, $valorProduto, $idParceiroCorrente);
    $dados = executeSP("spEstoqueDisponivel_Consultar", $params, $connection);
    return $dados;
}


/******************Movimento**************************/	


function compraValidar($tipoAcao, $nomeUsuario, $numeroCelular, $numeroCPF, $idCompra, $idOperador, $textoObservacao, $ativoProducao, $idCompraValidacao, $idParceiroCorrente){
    $connection = abrirConnection();   
    $params = array($tipoAcao, $nomeUsuario, $numeroCelular, $numeroCPF, $idCompra, $idOperador, $textoObservacao, $ativoProducao, $idCompraValidacao, $idParceiroCorrente);   
    $dados = executeSP("spCompra_Validar", $params, $connection);
    return $dados;
}

function compraValidacaoHistorico($tipoAcao, $nomeUsuario, $numeroCelular, $numeroCPF, $idCompra, $idOperador, $dataInicio, $dataFinal, $ativoProducao, $idCompraValidacao){
    $connection = abrirConnection();  
    $params = array($tipoAcao, $nomeUsuario, $numeroCelular, $numeroCPF, $idCompra, $idOperador, $dataInicio, $dataFinal, $ativoProducao, $idCompraValidacao);   
    $dados = executeSP("spCompraValidacao_Historico", $params, $connection);
    return $dados;
}

/******************Financeiro**************************/	

function compra($tipoAcao, $nomeUsuario, $numeroCelular, $numeroCPF, $idCompra, $idOperador, $dataInicio, $dataFinal, $ativoProducao, $idCompraValidacao, $textoEstorno, $idParceiroCorrente){
    $connection = abrirConnection();   
    $params = array($tipoAcao, $nomeUsuario, $numeroCelular, $numeroCPF, $idCompra, $idOperador, $dataInicio, $dataFinal, $ativoProducao, $idCompraValidacao, $textoEstorno, $idParceiroCorrente);   
    $dados = executeSP("spCompra", $params, $connection);
    return $dados;
}

function unidadeSaldo($tipoAcao, $idUnidade, $nomeUnidade, $ativoProducao, $idParceiroCorrente){
    $connection = abrirConnection();   
    $params = array($tipoAcao, $idUnidade, $nomeUnidade, $ativoProducao, $idParceiroCorrente);
    $dados = executeSP("spUnidadeSaldo", $params, $connection);
    return $dados;
}

function compraProdutoConsultar($tipoAcao, $id_usuario_pf, $idCarrinho, $idCompra){
    $connection = abrirConnection();   
    $params = array($tipoAcao, $id_usuario_pf, $idCarrinho, $idCompra);
    $dados = executeSP("spCompraProduto_Consultar", $params, $connection);
    return $dados;
}

function informarDataEnvio($idCompra, $id_usuario_pf){
    global $url_principal;
    $url = $url_principal.'informar_data_envio';

    $data = [
        "idCompra"=> $idCompra, 
        "id_usuario_pf"=> $id_usuario_pf, 
    ]; 

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false); 
    // curl_setopt($ch, CURLOPT_HTTPHEADER, Array('Authorization:'.$token.''));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

    $resposta = curl_exec($ch);
    curl_close($ch);             
    
    $respostaArray = json_decode($resposta, true);        
    
    if ((array_key_exists("code", $respostaArray)) && ($respostaArray['code'] == 200)) {       
        $dados = $respostaArray['data'];
    } else {
        $dados = null;
    }
    
    return $dados;
}

function informarDataEntrega($idCompra, $id_usuario_pf){
    global $url_principal;
    $url = $url_principal.'informar_data_entrega';
    

    $data = [
        "idCompra"=> $idCompra, 
        "id_usuario_pf"=> $id_usuario_pf, 
    ]; 

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false); 
    // curl_setopt($ch, CURLOPT_HTTPHEADER, Array('Authorization:'.$token.''));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

    $resposta = curl_exec($ch);
    curl_close($ch);             
    
    $respostaArray = json_decode($resposta, true);        
    
    if ((array_key_exists("code", $respostaArray)) && ($respostaArray['code'] == 200)) {       
        $dados = $respostaArray['data'];
    } else {
        $dados = null;
    }
    
    return $dados;
}

/******************Dashboard**************************/	


function dashboardGeral(){
    $connection = abrirConnection();   
    $params = array();   
    $dados = executeSP("spDashboard_Geral", $params, $connection);
    return $dados;
}

function dashboardQuantidade($dataAcao){
    $connection = abrirConnection();   
    $params = array($dataAcao);   
    $dados = executeSP("spDashboard_Quantidade", $params, $connection);
    return $dados;
}

function dashboardGenero($ativoProducao){
    $connection = abrirConnection();   
    $params = array( $ativoProducao);   
    $dados = executeSP("spDashboard_Genero", $params, $connection);
    return $dados;
}

function dashboardPessoa($dataAcao){
    $connection = abrirConnection();   
    $params = array($dataAcao);   
    $dados = executeSP("spDashboard_Pessoas", $params, $connection);
    return $dados;
}

function dashboardTurno($tipoTurno){
    $connection = abrirConnection();   
    $params = array($tipoTurno);   
    $dados = executeSP("spDashboard_Turno", $params, $connection);
    return $dados;
}




?>
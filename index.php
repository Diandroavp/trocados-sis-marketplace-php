<?php
    session_start();
    include("funcoes/funcoes.php"); 
    include("config/config.php"); 
?>
<!DOCTYPE html>
<html lang="en">
    <?php  include('head.php');?>
    <script>        
        function loaderTrocadosJs(){
            $("#overlay").css('display', 'block');

            var animation = bodymovin.loadAnimation({
                container: document.getElementById('lt'), 
                    path: 'funcoes/loader.json', 
                    renderer: 'svg', 
                    loop: true, 
                    autoplay: true, 
                    name: 'Trocados', 
                })
                window.onload = function() {
                    $("#overlay").css('display', 'none');
                };
        }    
    </script>

        <body>  
        <div id='overlay' style="display: none">
            <div id='card'>
                <div id='lt'></div>
            </div>
        </div> 		
        <?php
            //loaderTrocados();
			//splashAvisoAguarde();
            include('modell/index.php');
            include('src/geral/cabecalho/index.php');                                 
            include('app.php');
            include('src/geral/rodape/index.php');		
            include('js/scriptGlobais.php');		
        ?>
    </body>
     
</html>




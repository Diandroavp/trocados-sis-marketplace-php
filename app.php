<div class="m-3 margin-top">
<?php
    include "src/geral/login/login.sessions.php";
	//
	$_p = (isset($_REQUEST['_p'])) ? $_REQUEST['_p'] : null;


	if ($_SESSION["_SESSION_idOperador"] <> '') {
				
		switch($_p) {
		/*	GERAL	*/
			case "sai": /*Sair*/
				include "src/geral/login/login.processo.sair.php";
				//
				break;
		/*	CADASTRO	*/
			
			case "usu": /*Operador*/
				include "src/cadastro/operador/index.php";
				//
				break;
			
			case "doa": /*Compras Extenras*/
				include "src/cadastro/doacaoexterna/index.php";
				//
				break;	
			
			case "prod": /*Produto*/
				include "src/cadastro/produto/index.php";
				//
				break;
			case "pimg": /*Imagens do Produto*/
				include "src/cadastro/produtoImagem/index.php";
				//
				break;
			case "prca": /*Produto x Categoria*/
				include "src/cadastro/produtoCategoria/index.php";
				//
				break;	

		/*	ESTOQUE	*/	
			case "nota": /*Nota*/
				include "src/estoque/nota/index.php";
				//
				break;
			case "nprod": /*Nota*/
				include "src/estoque/notaProduto/index.php";
				//
				break;
		/*	MOVIMENTO	*/
			case "vali": /*validar Compra realizadas*/
				include "src/movimento/validar/index.php";
				//
				break;
			case "hisv": /*Histórico de Validação*/
				include "src/movimento/historico/index.php";
				//
				break;	
				/*	FINANCEIRO	*/		
			case "comp": /*Compra*/
				include "src/financeiro/compra/index.php";
				//
				break;
			case "unid": /*Unidade*/
				include "src/financeiro/unidade/index.php";
				//
				break;
		/*	DASHBOARD	*/
			case "dash": /*Dashboard Principal*/
				include "src/dashboard/index.php";
				//
				break;
		/*	CONSULTA	*/
			case "edisp": /*Estoque Disponível*/
				include "src/consulta/estoqueDisponivel/index.php";
				//
				break;
		/*	TABELA	*/
			case "med": /*Operador*/
				include "src/tabela/medida/index.php";
				//
				break;
			/*	MANUTENÇÃO	*/
			case "cate": /*Categoria*/
				include "src/manutencao/categoria/index.php";
				//
				break;
			case "gpar": /*Grupo de Parceiros*/
				include "src/manutencao/parceirosGrupo/index.php";
				//
				break;	
			case "par": /*Parceiros*/
				include "src/manutencao/parceiros/index.php";
				//
				break;
			case "perf": /*Perfil*/
				include "src/manutencao/perfil/index.php";
				//
				break;
			default:
					include "inicio.view.php";   
				break;
		}
		
	} else {        
		include "src/geral/login/index.php";    
	}

?>
</div>

<?php
	$CONFIG_DEVELOPER = strtoupper(gethostname());

	// echos($CONFIG_DEVELOPER);

    global $host, $user, $pass, $dbPortal, $formatoDataBanco, $ativoProducao, $tipoConexao, $url_principal, $img_produto_padrao, $img_produto_sec_padrao;

	$config['sess_expiration']= 0; /* nunca expirar a sessão sózinha  */
	$config['sess_expire_on_close'] = TRUE; /* finalizar sessão quando o navegador for fechado */

	if($CONFIG_DEVELOPER == 'DESKTOP-ET1IDTV') {
		$host = "35.223.4.246";
		$user = "diandro";
		$pass = "trocados2020";
		$dbPortal= "mkplace";    	
		$formatoDataBanco = "Y-m-d H:i:s";	
		$tipoConexao = 0;
		$ativoProducao = 0;			
		$url_principal = "http://localhost:8080/app/v2/";
		$img_produto_padrao	= 'https://i.ibb.co/YT1QvXh/cc35216defa2.jpg';
		$img_produto_sec_padrao	= 'https://i.ibb.co/7NZC7QK/cc35216defa2.jpg';
	} else {
		$user = getenv('CLOUDSQL_USER');
		$pass = getenv('CLOUDSQL_PASSWORD');
		$host = getenv('CLOUDSQL_DSN');
		$dbPortal = getenv('CLOUDSQL_DB');	
		$formatoDataBanco = "Y-m-d H:i:s";	
		$tipoConexao = 1;
		$ativoProducao = 1; # PRODUÇÃO
		#$ativoProducao = 0; # HOMOLOGAÇÃO			
		$url_principal = "https://apiappv3.trocados.com.br:8080/app/v2/";
		$img_produto_padrao	= 'https://i.ibb.co/YT1QvXh/cc35216defa2.jpg';
		$img_produto_sec_padrao	= 'https://i.ibb.co/7NZC7QK/cc35216defa2.jpg';
	}
	

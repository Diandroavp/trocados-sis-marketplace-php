<script>
    $(document).ready(function(){
	  $("#buscarModelo").on("keyup", function() {
		var value = $(this).val().toLowerCase();
		$("#tableModelo tr").filter(function() {
		  $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
		});
	  });
	});

    jQuery(function($) {        
        $(document).on('keypress', 'input.somente-numero', function(e) {
            var square = document.getElementById("textoPesquisa");
            var tipoPesquisa = document.getElementById("tipoPesquisa");
            var key = (window.event)?event.keyCode:e.which;
            
            if (tipoPesquisa.value == '<?=$paramPesquisa[0][0]?>') {            
                if((key > 47 && key < 58)) {
                    textoPesquisa.style.backgroundColor = "#ffffff";
                    textoPesquisa.placeholder = ""                
                    return true;                
                } else {
                    textoPesquisa.style.backgroundColor = "#FFD700";
                    textoPesquisa.placeholder = "Somente Números"
                    return (key == 8 || key == 0)?true:false;                
                }
            } else {
                textoPesquisa.style.backgroundColor = "#ffffff";
                textoPesquisa.placeholder = ""                
                return true;
            }
        });
    });
</script>   

<script>
    /** Envia Formulário*/
    function enviaFormularioSimples(form, autenticacao = 1){        
      var formulario = document.getElementById(form);
      let validacao = true;

      if (autenticacao){
          adicionarAutenticacao(form);
      }          
      
      $("#"+form+" input").each(function(){
        if(($(this).val() == "") && ($(this).attr("required"))){
          alert("O campo " + $(this).attr('name') + " precisa ser preenchido!");
          validacao = false;
        }
      });

      if (validacao){              
        formulario.submit();  
        return true;    
      } 
    }


    



    function adicionarAutenticacao(form){
        var idParceiroCorrente = '<?=(isset($_REQUEST['idParceiroCorrente'])) ? $_REQUEST['idParceiroCorrente'] : 0?>';
        
        var formulario = document.getElementById(form);

        if (idParceiroCorrente != '0') {
            var parceiro = document.createElement("input");
            parceiro.setAttribute('type', 'hidden');
            parceiro.setAttribute('name', 'idParceiroCorrente');
            parceiro.setAttribute('value', idParceiroCorrente);

            formulario.appendChild(parceiro);
        }
        
        
    }

    function enviaFormExcluir(texto, codigo){            
      if (confirm(texto)) {            
        return enviaFormularioSimples('modalExcluir' + codigo);     
      } else {
        return false;
      }
    }

    function enviaFormMenu(form, href){            
        
      var formulario = document.getElementById(form);

      var e = document.createElement("input");
      e.setAttribute('type', 'hidden');
      e.setAttribute('name', '_p');
      e.setAttribute('value', href);

      formulario.appendChild(e);

      enviaFormularioSimples(form);
    }
 
</script>
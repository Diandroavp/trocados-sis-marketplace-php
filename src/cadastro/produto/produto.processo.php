<?php	
	global $idParceiroCorrente;

    $textoDirecionar = "?_p=".$_REQUEST["_p"];	
	$idOperadorCorrente = $_SESSION["_SESSION_idOperador"];
	$_subrota = (isset($_REQUEST['_subrota'])) ? $_REQUEST['_subrota'] : null;

	// Tipo de Pesquisa
	$tipoPesquisa = (isset($_REQUEST['tipoPesquisa'])) ? $_REQUEST['tipoPesquisa'] : null;
	$textoPesquisa = (isset($_REQUEST['textoPesquisa'])) ? $_REQUEST['textoPesquisa'] : null;
	
	// CRUD
	$tipoAcao = (isset($_REQUEST['tipoAcao'])) ? $_REQUEST['tipoAcao'] : 'S';
	$idProduto = (isset($_REQUEST['idProduto'])) ? $_REQUEST['idProduto'] : null;  
	$nomeProduto = (isset($_REQUEST['nomeProduto'])) ? $_REQUEST['nomeProduto'] : null;  
	$tipoProdutoPesquisa = (isset($_REQUEST['tipoProdutoPesquisa'])) ? $_REQUEST['tipoProdutoPesquisa'] : 1;  
	$atualizarImagem = (isset($_REQUEST['atualizarImagem'])) ? $_REQUEST['atualizarImagem'] : null;  
	$nomeImagem = (isset($_REQUEST['nomeImagem'])) ? $_REQUEST['nomeImagem'] : null;  
	$nomeImagemSecundaria = (isset($_REQUEST['nomeImagemSecundaria'])) ? $_REQUEST['nomeImagemSecundaria'] : null;  	
	$idParceiro = (isset($_REQUEST['idParceiro'])) ? $_REQUEST['idParceiro'] : null;  	
	$textoDescricao = (isset($_REQUEST['textoDescricao'])) ? str_replace("'", "", $_REQUEST['textoDescricao']) : null;  
	$ativoMostrarTelaPrincipal = (isset($_REQUEST['ativoMostrarTelaPrincipal'])) ? $_REQUEST['ativoMostrarTelaPrincipal'] : 0;  
	$tipoProduto = (isset($_REQUEST['tipoProduto'])) ? $_REQUEST['tipoProduto'] : null;  
	$ativoProduto = (isset($_REQUEST['ativoProduto'])) ? $_REQUEST['ativoProduto'] : 0;  
	$tipoGiftCard = (isset($_REQUEST['tipoGiftCard'])) ? $_REQUEST['tipoGiftCard'] : null;  
	$nomeOperadoraRecarga = (isset($_REQUEST['nomeOperadoraRecarga'])) ? $_REQUEST['nomeOperadoraRecarga'] : null;  
	$ativoControleCodigo = (isset($_REQUEST['ativoControleCodigo'])) ? $_REQUEST['ativoControleCodigo'] : 0;  
	$textoInstrucao = (isset($_REQUEST['textoInstrucao'])) ? str_replace("'", "", $_REQUEST['textoInstrucao']) : null;  
	$ativoEventoNatal = (isset($_REQUEST['ativoEventoNatal'])) ? $_REQUEST['ativoEventoNatal'] : 0;  
	$quantidadeApresentacao = (isset($_REQUEST['quantidadeApresentacao'])) ? $_REQUEST['quantidadeApresentacao'] : null;  
	$idMedida = (isset($_REQUEST['idMedida'])) ? $_REQUEST['idMedida'] : null;  
	$quantidadeMinima = (isset($_REQUEST['quantidadeMinima'])) ? $_REQUEST['quantidadeMinima'] : null;  
	$qrCode = (isset($_REQUEST['qrCode'])) ? $_REQUEST['qrCode'] : null;  
	$ordem = (isset($_REQUEST['ordem'])) ? $_REQUEST['ordem'] : null;  
	
	// Retorno de Resposta
	$tipoResposta = (isset($_REQUEST['tipoResposta'])) ? $_REQUEST['tipoResposta'] : null;
	$textoRetorno = (isset($_REQUEST['textoRetorno'])) ? $_REQUEST['textoRetorno'] : null;
	$ativoRetorno = (isset($_REQUEST['ativoRetorno'])) ? $_REQUEST['ativoRetorno'] : null;	
	 

    if (isset($_REQUEST['tipoAcao']) && ($tipoAcao != 'S')){
		// VERIFICAR SE É PARA SUBMETER UMA IMAGEM
		if ($atualizarImagem) {			

			if (strpos($nomeImagem, 'jpeg') > 0){
				$dadosImagem = updateImagem(str_replace('data:image/jpeg;base64,', '', $nomeImagem));
			} else if (strpos($nomeImagem, 'png') > 0){
				$dadosImagem = updateImagem(str_replace('data:image/png;base64,', '', $nomeImagem));
			} else if (strpos($nomeImagem, 'jpg') > 0){
				$dadosImagem = updateImagem(str_replace('data:image/jpg;base64,', '', $nomeImagem));
			}
		
			$nomeImagem = $dadosImagem['data']['url'];
			$nomeImagemSecundaria = $dadosImagem['data']['thumb']['url'];  
		} 

		$dadosResposta = produto($tipoAcao, $idParceiroCorrente, $idProduto, $nomeProduto, $tipoProdutoPesquisa, $nomeImagem, $nomeImagemSecundaria, $idParceiro, $textoDescricao, $ativoMostrarTelaPrincipal, $tipoProduto, $ativoProduto, $tipoGiftCard, $nomeOperadoraRecarga, $ativoControleCodigo, $textoInstrucao, $ativoEventoNatal, $quantidadeApresentacao, $idMedida, $quantidadeMinima, $qrCode, $ordem);
		
		if ($dadosResposta and ($dadosResposta[0]['ativoRetorno'])){
			//AUDITORIA
			Include "produto.auditoria.php";	
		}			
		//
		include "produto.processo.resposta.php";
    }
	if (isset($_REQUEST['tipoResposta'])){
		showMessage(($ativoRetorno == 1) ? "S" : "E", $_CONFIGURACAO_TITULO." - Informativo", $textoRetorno);
	}	

	// Montando tipo de Pesquisa
	$paramPesquisa = array();
	$paramPesquisa[] = array('ID', 'ID');
	$paramPesquisa[] = array('Nome', 'Nome');
	if (!$tipoPesquisa){
		$tipoPesquisa = $paramPesquisa[1][0];
	}	

	// Montando Tipo Produto
	$paramTipoProduto = array();
	$paramTipoProduto[] = array('tipoProduto'=> 'p', 'nomeTipoProduto'	=> 'Produto');
	$paramTipoProduto[] = array('tipoProduto'=> 's', 'nomeTipoProduto'	=> 'Serviço');
	$paramTipoProduto[] = array('tipoProduto'=> 'vc', 'nomeTipoProduto'	=> 'Vale Compra');
	$paramTipoProduto[] = array('tipoProduto'=> 'g', 'nomeTipoProduto'	=> 'GiftCard');
	$paramTipoProduto[] = array('tipoProduto'=> 'r', 'nomeTipoProduto'	=> 'Recarga');
	
	// Montando Tipo GiftCard
	$paramTipoGiftCard = array();
	$paramTipoGiftCard[] = array('tipoGiftCard'=> '', 'nomeTipoGiftCard'	=> '');
	$paramTipoGiftCard[] = array('tipoGiftCard'=> 'g', 'nomeTipoGiftCard'	=> 'Google Play');
	$paramTipoGiftCard[] = array('tipoGiftCard'=> 'n', 'nomeTipoGiftCard'	=> 'Netflix');
	$paramTipoGiftCard[] = array('tipoGiftCard'=> 's', 'nomeTipoGiftCard'	=> 'Spotify');
	$paramTipoGiftCard[] = array('tipoGiftCard'=> 'u', 'nomeTipoGiftCard'	=> 'Uber');	
	

	// Montando Tipo Recarga
	$paramTipoOperadora = array();
	$paramTipoOperadora[] = array('nomeOperadoraRecarga'	=> '');
	$paramTipoOperadora[] = array('nomeOperadoraRecarga'	=> 'CLARO');
	$paramTipoOperadora[] = array('nomeOperadoraRecarga'	=> 'OI');
	$paramTipoOperadora[] = array('nomeOperadoraRecarga'	=> 'TIM');
	$paramTipoOperadora[] = array('nomeOperadoraRecarga'	=> 'VIVO');
	
	
	// 
	$textoAviso = '';
	// Abrindo Parceiros para pesquisa
	//$dadosParceiros = parceirosSelecionar(null);
	$dadosMedida = medida('S', null, null, null);

	if (($tipoAcao == 'S')) {
		// Dados da grid principal
		if (($_subrota = 'editar') && ($idProduto)) {
			$dadosProduto = produto($tipoAcao, $idParceiroCorrente,	$idProduto,	null, null,	null, null, null, null, null, null, null, null, null, null, null, null,	null, null,	null, null, null);										
		} else {
			$dadosProduto = produto(	$tipoAcao, 
									$idParceiroCorrente,
									(($textoPesquisa != '') && ($tipoPesquisa == 'ID')) ? $textoPesquisa : null,
									(($textoPesquisa != '') && ($tipoPesquisa == 'Nome')) ? $textoPesquisa : null,
									$tipoProdutoPesquisa,
									$nomeImagem, 
									$nomeImagemSecundaria,
									$idParceiro, 									 
									$textoDescricao,
									$ativoMostrarTelaPrincipal, 
									$tipoProduto, 
									$ativoProduto, 
									$tipoGiftCard, 
									$nomeOperadoraRecarga, 
									$ativoControleCodigo, 
									$textoInstrucao,
									$ativoEventoNatal,
									$quantidadeApresentacao,
									$idMedida,
									$quantidadeMinima,
									$qrCode,
									$ordem
								);		
		}
			
		include "./funcoes/croperjs/index.php";
		iniciarEditorImagem('imagemCarregada', 250, 250, 'nomeImagem', 'atualizarImagem');
	}

	$numeroLinhas = 0;
	$idProdutoMax = 0;
?>


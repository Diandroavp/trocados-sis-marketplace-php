<div class="container " style="overflow-y: auto; min-height:450px;">    
    <div class="page-header">
        <h4>Estoque/Produtos</h4>      
    </div>
	<div class="well">
        <form id="formPesquisa" action="<?=$textoDirecionar?>" method="post" autocomplete="off">
            <input type="hidden" name="tipoAcao"  value="S" />
            <input type="hidden" name="idParceiroCorrente"  value="<?=$idParceiroCorrente?>" />   
            <div class="row">
                <div class="col-sm-2">
                    <label for="Nome">Tipo de Pesquisa</label>
					<div class="form-group">
					  <select class="form-control" id="tipoPesquisa" name="tipoPesquisa" required>							
						<?php foreach($paramPesquisa as $item) {?>
						  <option value="<?=$item[0]?>" <?=(($item[0] == $tipoPesquisa) ? 'selected':'')?>><?=$item[1]?></option>
						<?php }?>  
					  </select>
					</div>
                </div>					
				<div class="col-sm-6">
                    <label for="Nome">Texto para pesquisa</label>
					<div class="form-group">
						<input class="form-control somente-numero" name="textoPesquisa" id="textoPesquisa"  value="<?=$textoPesquisa?>" />  
					</div>
                </div>	
                <div class="col-sm-2">
                    <label for="Nome">Produtos</label>
					<div class="form-group">                    
					<select class="form-control" id="tipoProdutoPesquisa" name="tipoProdutoPesquisa" >                                             
                        <option value="1" <?=(($tipoProdutoPesquisa == 1) ? 'selected':'')?>> Ativos </option>
                        <option value="2" <?=(($tipoProdutoPesquisa == 2) ? 'selected':'')?>> Inativos </option>
                        <option value="3" <?=(($tipoProdutoPesquisa == 3) ? 'selected':'')?>> Todos </option>    
                    </select>
                    
                    </div>
                </div>				
				<div class="col-sm-2">
					<label for="Nome"> &nbsp; </label>
					<div class="form-group text-right">
						<button type="button" onclick="return enviaFormularioSimples('formPesquisa')"  class="btn btn-primary" > Localizar </button>
					</div>						
				</div>  							
            </div>
        </form>		        
    </div>
    <div class="panel panel-default">
        <div class="panel-heading ">
			<strong>Listagem dos Produtos</strong>
        </div>
		<?php if($dadosProduto) {?>
			<input class="form-control" id="buscarModelo" type="text" placeholder="Buscar..">
		<?php }?>	
        <div class="table-responsive" style="overflow-y: auto; max-height:350px;">
            <table class="table table-hover table-striped sortable"  >
                <thead>
                    <tr>
                        <th><strong>ID</strong></th>
                        <th><strong>Nome</strong></th>
                        <th><strong>Qtd Apresentação</strong></th>
                        <th><strong>Medida</strong></th>
                        <th><strong>Parceiro</strong></th>
                        <th><strong>Tipo</strong></th>
                        <th><strong>Ativo</strong></th>
                        <th><strong>Home</strong></th>
                        <th><strong>Qr</strong></th>
                        <th><strong></strong></th>						
                    </tr>
                </thead>                
                <tbody id="tableModelo">
                    <?php foreach($dadosProduto as $item) {
                        $textoAviso = '';
                        //
                        if (!$item['ativoProduto']){
                            $textoAviso = $textoAviso.'- Produto Desativado!';	
                        }?>
                    <tr class=" <?=(!$item['ativoProduto']) ? 'danger' : '' ?>" title="<?=$textoAviso?>">
                        <td><?=$item['idProduto']?></td>
                        <td><?=$item['nomeProduto']?></td>
                        <td><?=$item['quantidadeApresentacao']?></td>                        
                        <td><?=$item['siglaMedida']?></td>                        
                        <td><?=$item['nomeParceiro']?></td>
                        <td><?=$item['tipoProduto']?></td>
                        <td><?=simOuNao($item['ativoProduto'])?></td>
                        <td><?=simOuNao($item['ativoMostrarTelaPrincipal'])?></td>
                        <td>
                            <?php if ($item['qrCode'] <> '') {?>
                                <button class="btn btn-default"  title="Exibir QR Code" onclick="javascript:abrirQrCode('<?=$item['qrCode']?>', '<?=$item['nomeProduto']?>')"><i class='fas fa-qrcode'></i></button>
                            <?php }?>
                        </td>
                        <td>
                            <button type="button" class="btn btn-primary" onclick="return enviaFormularioSimples('formEditar<?=$item['idProduto']?>')"  title="Editar registro!">Editar</button>
                            <button type="button" class="btn btn-danger" onclick="enviaFormExcluir('Deseja excluir o registro <?=$item['idProduto']?> - <?=$item['nomeProduto']?>?', '<?=$item['idProduto']?>')"  title="Excluir Registro">Excluir</button>                                                        
                            <button type="button" class="btn btn-primary" onclick="return enviaFormularioSimples('formImagems<?=$item['idProduto']?>')"  title="Acessar as imagens">Imagens</button>                            
                        </td>
                    </tr>
                    <?php 
                        $numeroLinhas++; 

                        if ($idProdutoMax < $item['idProduto']){
                            $idProdutoMax = $item['idProduto'];                            
                        }                        
                    }?>
                </tbody>
            </table>
        </div>
        <br>
        <div class="panel-footer">
            <?=$numeroLinhas." Registros encontrados..."?>
			<label for="Nome"> &nbsp; </label>
			<div class="form-group text-right">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalAdicionar">+ Novo Contrato</button>						                
			</div>	
        </div>
    </div>    
</div>
<?php
	// include "produto.editar.view.php";
    include "produto.adicionar.view.php";
    include "produto.excluir.view.php";
?>

<script>

	function abrirQrCode(qrCode, descricaoQrCode) {
		   //config="scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no"	
		   //config+="width=400,"
		   //config+="height=1000"
		   config=""
		  
		   //var window=open("src/estoque/produto/impressao/impressao.qrcode.php?qrCode="+qrCode,'janela', 'width=400, height=400, top=100, left=699, scrollbars=no, status=no, toolbar=no, location=no, menubar=no, resizable=no, fullscreen=no');
		   //window.focus();


           var myWindow = window.open("src/cadastro/produto/impressao/impressao.qrcode.php?qrCode="+qrCode + "&descricaoQrCode="+descricaoQrCode,'janela', 'width=400, height=300, top=100, left=699, scrollbars=no, status=no, toolbar=no, location=no, menubar=no, resizable=no, fullscreen=no');
           //myWindow.document.write('<title>My PDF File Title</title>');
           myWindow.focus();
	}
</script>   

<?php foreach($dadosProduto as $item) {?>
    <form role="form"  id="formImagems<?=$item['idProduto']?>" action="?_p=pimg" method="post">					                           		
        <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
        <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
        <input type="hidden" name="idProduto"  value="<?=$item['idProduto']?>" />
        <input type="hidden" name="nomeProduto"  value="<?=$item['nomeProduto']?>" />
    </form>

    <form role="form"  id="formEditar<?=$item['idProduto']?>" action="<?=$textoDirecionar?>" method="post">					                           		
        <input type="hidden" name="_subrota"  value="editar" />
        <input type="hidden" name="idProduto"  value="<?=$item['idProduto']?>" />
        <input type="hidden" name="nomeProduto"  value="<?=$item['nomeProduto']?>" />        
        <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
        <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
    </form>
<?}?>

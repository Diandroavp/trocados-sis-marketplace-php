
<!DOCTYPE html>
<html lang="en">
     <head>
          <title>Mercado Trocados</title>
          <link href="../../../../css/bootstrap.min.css" rel="stylesheet">
          <script type="text/javascript" src="../../../../js/jquery.min.js"></script>    
		<script type="text/javascript" src="../../../../js/bootstrap.min.js"></script> 
          <link href="../../../../css/padrao.css" rel="stylesheet">
          <!--FaveIcon-->
		<link rel="shortcut icon" href="../../../../imagens/icon.ico" type="image/x-icon"/>
     </head>
     <?php 

          $qrCode = (isset($_REQUEST['qrCode'])) ? $_REQUEST['qrCode'] : null;  
          $descricaoQrCode = (isset($_REQUEST['descricaoQrCode'])) ? $_REQUEST['descricaoQrCode'] : null; 
     ?>
     <body> 
          <div id="printableArea">
               <center>
                    <h4><strong><?=$descricaoQrCode?></strong></h4>
                    <br>
                    <iframe src="impressao.qrcode.phpqrcode.php?qrCode=<?=$qrCode?>" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" width="100" height="150" top="50"></iframe>              
               </center>     
          </div>
          <br>

          <div class="modal-footer">
               <center><button type="button" class="btn btn-success" onclick="printDiv('printableArea')">Imprimir</button></center>
          </div>
     </body>
     
</html>




<script>
     function printDiv(divName) {
          var printContents = document.getElementById(divName).innerHTML;
          var originalContents = document.body.innerHTML;

          document.body.innerHTML = printContents;

          if (window.print()){
               window.close();
          }    
          
          document.body.innerHTML = originalContents;      
     }
</script>



<!-- Esse trecho é importante para colocar os IDs e Nomes para os Inputs -->
<div class="modal fade " data-backdrop="static" id="myModalAdicionar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-md">
        <div class="modal-content" >
            <form role="form" name="modalForm" id="modalFormAdicionar" action="<?=$textoDirecionar?>" method="post" autocomplete="off">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×   </span><span class="sr-only">Fechar</span>

                    </button>
                    <h4 class="modal-title" id="myModalLabel">Novo Registro</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">		
                        <input type="hidden" name="tipoAcao"  value="I" />
                        <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
	                    <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />    
	                    <input type="hidden" name="idParceiro"  value="<?=$idParceiroCorrente?>" />    
                        
                        <fieldset>
                                <legend>Dados do Produto</legend>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="thumbnail col col-12 img-rounded" id="ativoIMG" data-toggle="tooltip" data-placement="top" >
                                            <img id="imagemCarregada" style="width: 250px;  height: 250px;" value="<?=$img_produto_padrao?>" />                                      
                                            <div class="caption">
                                                <center>
                                                    <p><label for="nomeImagem">Imagem Home</label></p>  
                                                    <input type="hidden" name="nomeImagem" id="nomeImagem" value="<?=$img_produto_padrao?>" require/>
                                                    <input type="hidden" name="nomeImagemSecundaria" id="nomeImagemSecundaria" value="<?=$img_produto_sec_padrao?>" require/>
                                                    <input type="hidden" name="atualizarImagem" id="atualizarImagem"  />
                                                    <label class="btn btn-primary" readonly="yes" onclick="editarImagem()"> Importar</label>                                        
                                                </center>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="nomeProduto">Nome do Produto</label>
                                                <input type="text" class="form-control"  name="nomeProduto" id="nomeProduto"   required/>                               
                                            </div>   
                                            <div class="col-md-12">
                                                <label for="ordem">Ordem de Apresentação</label>
                                                <input type="text" class="form-control"  name="ordem"  value="0" />                               
                                            </div>
                                            <div class="col-md-6">
                                                <label for="quantidadeMinima">Quantidade Mínima em Estoque</label>
                                                <input type="text" class="form-control"  name="quantidadeMinima"  value="1" />                               
                                            </div>   
                                            <div class="col-md-6">
                                                <label for="quantidadeApresentacao">Quantidade Apresentação</label>
                                                <input type="text" class="form-control"  name="quantidadeApresentacao"  value="1" />                               
                                            </div>   
                                            <div class="col-md-12">
                                                <label for="idMedida">Medida</label>
                                                <div class="form-group">
                                                    <select class="form-control"  name="idMedida" required>                     
                                                        <?php foreach($dadosMedida as $itemP) {?>
                                                        <option value="<?=$itemP['idMedida']?>" ><?=$itemP['siglaMedida']?></option>
                                                        <?php }?>  
                                                    </select>
                                                </div>                                                                  
                                            </div>                                                                  	                                
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">                                                                            
                                                <div class="checkbox">   
                                                    <br>                                      
                                                    <label><input type="checkbox" name="ativoMostrarTelaPrincipal"  value="1" > Mostrar na Home</label>                                          
                                                </div>
                                            </div>
                                            <div class="col-md-6">                                                                            
                                                <div class="checkbox">   
                                                    <br>                                      
                                                    <label><input type="checkbox" name="ativoControleCodigo"  value="1" > Controlar por Código</label>                                          
                                                </div>
                                            </div>
                                            <div class="col-md-6">                                                                            
                                                <div class="checkbox">   
                                                    <br>                                      
                                                    <label><input type="checkbox" name="ativoEventoNatal"  value="1" > Especial Natal</label>                                          
                                                </div>
                                            </div>              
                                            <div class="col-md-6">                                    
                                                <div class="checkbox">   
                                                    <br>                                      
                                                    <label><input type="checkbox" name="ativoProduto"  value="1" checked> Produto Ativo</label>                                          
                                                </div>
                                            </div>                                                                 	                                
                                        </div>
                                    </div>
                                </div>

                                    
                            </fieldset>     

                            <fieldset>
                                <legend>Parceiro</legend>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label for="idParceiro">Parceiro</label>
                                        <input class="form-control" Disabled  value="<?=$item['nomeParceiro']?>" />                                                                  
                                    </div>                    	                                
                                </div>                                   
                            </fieldset>  

                            <fieldset>
                                <legend>Informações Adicionais</legend>
                                <div class="row">                                    
                                    <!--<div class="col-sm-4">
                                        <label for="nomeImagemSecundaria">Imagem Home Secundária</label>
                                        <input class="form-control"  name="nomeImagemSecundaria"   required/>                               
                                    </div>  -->
                                    <div class="col-sm-12">
                                        <label for="tipoProduto">Tipo Produto</label>
                                        <div class="form-group">
                                            <select class="form-control"  name="tipoProduto" disabled>                     
                                                <?php foreach($paramTipoProduto as $itemP) {?>
                                                <option value="<?=$itemP['tipoProduto']?>" ><?=$itemP['nomeTipoProduto']?></option>
                                                <?php }?>  
                                            </select>
                                        </div>                                      
                                    </div>   
                                    <input type="hidden" name="tipoProduto"  value="p" />                                                                     	                                
                                </div>
                                <div class="row">
                                    <div class="col-sm-9">
                                        <label for="qrCode">Qr Code </label>
                                        <input class="form-control"  name="qrCode" id="qrCode" placeholder="Inicie o texto com um numero: Ex.: 000texto "  maxlength="200"/>                               
                                    </div>  
                                    <div class="col-sm-3">
                                        <label for="qrCode">Gerar Novo Qr </label><br>
                                        <button type="button" class="btn btn-default" onclick="gerarQr('<?=$idProdutoMax?>', 'qrCode')"><i class='fas fa-qrcode'></i></button>                       
                                    </div>                                                                                          	                                
                                </div>

                                <div class="row">                                    
                                    <div class="col-sm-6">
                                        <label for="tipoGiftCard">Tipo GiftCard ( Caso o tipo de Produto seja GiftCard )</label>                                        
                                        <div class="form-group">
                                            <select class="form-control"  name="tipoGiftCard" >                     
                                                <?php foreach($paramTipoGiftCard as $itemP) {?>
                                                <option value="<?=$itemP['tipoGiftCard']?>" ><?=$itemP['nomeTipoGiftCard']?></option>
                                                <?php }?>  
                                            </select>
                                        </div>                              
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="nomeOperadoraRecarga">Tipo Operadora ( Caso o tipo de Produto seja Recarga )</label>
                                        <div class="form-group">
                                            <select class="form-control"  name="nomeOperadoraRecarga" >                     
                                                <?php foreach($paramTipoOperadora as $itemP) {?>
                                                <option value="<?=$itemP['nomeOperadoraRecarga']?>" ><?=$itemP['nomeOperadoraRecarga']?></option>
                                                <?php }?>  
                                            </select>
                                        </div>                              
                                    </div>                               	                                
                                </div>
                                   
                            </fieldset> 
                            
                            <fieldset>
                                <legend>Descrições e Instruções</legend>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="textoDescricao">Descrição</label>
                                        <textarea class="form-control"  name="textoDescricao"  rows="5"></textarea>                              
                                    </div>                                                 	                                                                
                                    <div class="col-md-6">
                                        <label for="textoInstrucao">Instrução</label>
                                        <textarea class="form-control"  name="textoInstrucao"  rows="5"></textarea>                               
                                    </div>                                                        	                                
                                </div>                                   
                            </fieldset> 
                   
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="button" onclick="return enviaFormularioSimples('modalFormAdicionar')" class="btn btn-success" >Salvar Mudanças</button>
                </div>
            </form>
        </div>
    </div>
</div>


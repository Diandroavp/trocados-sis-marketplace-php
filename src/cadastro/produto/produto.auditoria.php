<?php 
    //AUDITORIA
    $descricaoAuditoria = 'Produto ['.$idOperadorCorrente.';'.$idProduto.']';
    //					
    $tipoAuditoria  = $tipoAcao;	    					
    //
    $textoAuditoria = 'Cadastro dos Produtos => ';
    $textoAuditoria = $textoAuditoria.'idOperadorCorrente: '.$idOperadorCorrente;
    $textoAuditoria = $textoAuditoria.';textoDirecionar: '.$textoDirecionar;
    $textoAuditoria = $textoAuditoria.';tipoAcao: '.$tipoAcao;	
    $textoAuditoria = $textoAuditoria.';tipoResposta: '.$tipoResposta;			
    $textoAuditoria = $textoAuditoria.';textoRetorno: '.$textoRetorno;			
    $textoAuditoria = $textoAuditoria.';ativoRetorno: '.$ativoRetorno;	
    $textoAuditoria = $textoAuditoria.';tipoProdutoPesquisa: '.$tipoProdutoPesquisa;	    
    $textoAuditoria = $textoAuditoria.';idProduto: '.$idProduto;			
    $textoAuditoria = $textoAuditoria.';nomeProduto: '.$nomeProduto;			
    $textoAuditoria = $textoAuditoria.';nomeImagem: '.$nomeImagem;				
    $textoAuditoria = $textoAuditoria.';nomeImagemSecundaria: '.$nomeImagemSecundaria;				
    $textoAuditoria = $textoAuditoria.';idParceiro: '.$idParceiro;				
    $textoAuditoria = $textoAuditoria.';textoDescricao: '.$textoDescricao;				
    $textoAuditoria = $textoAuditoria.';ativoMostrarTelaPrincipal: '.$ativoMostrarTelaPrincipal;				
    $textoAuditoria = $textoAuditoria.';quantidadeDisponivel: '.$quantidadeDisponivel;				
    $textoAuditoria = $textoAuditoria.';tipoGiftCard: '.$tipoGiftCard;				
    $textoAuditoria = $textoAuditoria.';nomeOperadoraRecarga: '.$nomeOperadoraRecarga;				
    $textoAuditoria = $textoAuditoria.';ativoControleCodigo: '.$ativoControleCodigo;				
    $textoAuditoria = $textoAuditoria.';textoInstrucao: '.$textoInstrucao;				
    $textoAuditoria = $textoAuditoria.';quantidadeApresentacao: '.$quantidadeApresentacao;				
    $textoAuditoria = $textoAuditoria.';quantidadeMinima: '.$quantidadeMinima;		
    $textoAuditoria = $textoAuditoria.';idParceiroCorrente: '.$idParceiroCorrente;			
        
    inserirAuditoria($descricaoAuditoria, $tipoAuditoria, $textoAuditoria);
?>
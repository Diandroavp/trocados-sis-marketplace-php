<!-- Esse trecho é importante para colocar os IDs e Nomes para os Inputs -->
<?php foreach($dadosProduto as $item) { ?>
<div class="container " style="overflow-y: auto; min-height:450px;">    
    <div class="page-header">
        <h4>Editando Registro [<?=$item['idProduto']?> - <?=$item['nomeProduto']?>]</h4>      
    </div>	
    <div class="panel panel-default">
        <form role="form" name="modalForm" id="modalFormEditar<?=$item['idProduto']?>" action="<?=$textoDirecionar?>" method="post">
        
            <div class="panel-body">
                <div class="form-group">                            		
                    <input type="hidden" name="tipoAcao"  value="E" />
                    <input type="hidden" name="_subrota"  value="editar" />
                    <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
                    <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />                            
                    <input type="hidden" name="idProduto"  value="<?=$item['idProduto']?>" />  

                    <fieldset>
                        <legend>Dados do Produto</legend>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="thumbnail col col-12 img-rounded" id="ativoIMG" data-toggle="tooltip" data-placement="top" >
                                    <img id="imagemCarregada" style="width: 250px;  height: 250px;" src="<?=($item['nomeImagemSecundaria'] != "") ? $item['nomeImagemSecundaria'] : '' ?>" />                                      
                                    <div class="caption">
                                        <center>
                                            <p><label for="nomeImagem">Imagem Home</label></p>  
                                            <input type="hidden" name="nomeImagem" id="nomeImagem" value="<?=$item['nomeImagem']?>" require/>
                                            <input type="hidden" name="atualizarImagem" id="atualizarImagem"  />
                                            <label class="btn btn-primary" readonly="yes" onclick="editarImagem('<?=$item['nomeImagem']?>')"> Importar</label>                                        
                                        </center>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label for="Código">Código</label>
                                        <input class="form-control" Disabled  value="<?=$item['idProduto']?>" />                               
                                    </div>
                                    <div class="col-md-10">
                                        <label for="nomeProduto">Nome do Produto</label>
                                        <input class="form-control"  name="nomeProduto"  value="<?=$item['nomeProduto']?>" required/>                               
                                    </div>
                                    <div class="col-md-4">
                                        <label for="ordem">Ordem de apresentacao</label>
                                        <input class="form-control"  name="ordem"  value="<?=$item['ordem']?>" />                               
                                    </div>  
                                    <div class="col-md-4">
                                        <label for="quantidadeMinima">Quantidade Mínima em Estoque</label>
                                        <input class="form-control"  name="quantidadeMinima"  value="<?=$item['quantidadeMinima']?>" />                               
                                    </div>  
                                    <div class="col-md-4">
                                        <label for="quantidadeApresentacao">Qtd Apresentação</label>
                                        <input class="form-control"  name="quantidadeApresentacao"  value="<?=$item['quantidadeApresentacao']?>" />                               
                                    </div>   
                                    <div class="col-md-12">
                                        <label for="idMedida">Medida</label>
                                        <div class="form-group">
                                            <select class="form-control"  name="idMedida" required>                     
                                                <?php foreach($dadosMedida as $itemP) {?>
                                                <option value="<?=$itemP['idMedida']?>" <?=(($itemP['idMedida'] == $item['idMedida']) ? 'selected':'')?> ><?=$itemP['siglaMedida']?></option>
                                                <?php }?>  
                                            </select>
                                        </div>                                                                  
                                    </div>                                      	                                
                                </div>

                                <div class="row">
                                    <div class="col-md-6">                                                                            
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoMostrarTelaPrincipal"  value="1" <?=($item['ativoMostrarTelaPrincipal'] ? 'checked' : '' )?>/> Mostrar na Home</label>                                          
                                        </div>
                                    </div>
                                    <div class="col-md-6">                                                                            
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoControleCodigo"  value="1" <?=($item['ativoControleCodigo'] ? 'checked' : '' )?> /> Controlar por Código</label>                                          
                                        </div>
                                    </div>
                                    <div class="col-md-6">                                                                            
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoEventoNatal"  value="1" <?=($item['ativoEventoNatal'] ? 'checked' : '' )?> /> Especial Natal</label>                                          
                                        </div>
                                    </div>              
                                    <div class="col-md-6">                                    
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoProduto"  value="1" <?=($item['ativoProduto'] ? 'checked' : '' )?> /> Produto Ativo</label>                                          
                                        </div>
                                    </div>                                     
                                </div> 
                            </div>
                        </div>
 
                           
                    </fieldset>     

                    <fieldset>
                        <legend>Parceiro</legend>
                        <div class="row">
                            <div class="col-sm-12">
                                <label for="idParceiro">Parceiro</label>
                                <input class="form-control" Disabled  value="<?=$item['nomeParceiro']?>" />                                                                  
                            </div>                    	                                
                        </div>                                   
                    </fieldset>  
                   
                    <fieldset>
                        <legend>Informações Adicionais</legend>
                        <div class="row">                            
<!--
                            <div class="thumbnail col col-sm-4 img-rounded" id="ativoIMG" data-toggle="tooltip" data-placement="top" >
                                <img id="imagemCarregadaSecundaria" style="width: 150px;  height: 200px;" src="<?=($imagemCarregadaSecundaria != "") ? $imagemCarregadaSecundaria : '' ?>" />                                      
                                <div class="caption">
                                    <center>
                                        <label for="nomeImagemSecundaria">Imagem Home Secundária</label>  
                                        <input type="hidden" name="nomeImagemSecundaria" id="nomeImagemSecundaria" value="<?=($imagemCarregadaSecundaria != "") ? $imagemCarregadaSecundaria : '' ?>" required />
                                        <label class="btn btn-primary" readonly="yes" onclick="editarImagem()"><i class="glyphicon glyphicon-folder-open" aria-hidden="true" ></i> Importar</label>                                        
                                    </center>
                                </div>
                            </div>   
                            -->
                            <div class="col-sm-12">
                                <label for="tipoProduto">Tipo Produto</label>
                                <div class="form-group">
                                    <select class="form-control"  name="tipoProduto" disabled>                     
                                        <?php foreach($paramTipoProduto as $itemP) {?>
                                        <option value="<?=$itemP['tipoProduto']?>" <?=(($itemP['tipoProduto'] == $item['tipoProduto']) ? 'selected':'')?>><?=$itemP['nomeTipoProduto']?></option>
                                        <?php }?>  
                                    </select>
                                </div>                                      
                            </div>   
                            <input type="hidden" name="tipoProduto"  value="<?=$item['tipoProduto']?>" />                                                              	                                
                        </div>
                        <div class="row">
                            <div class="col-sm-9">
                                <label for="qrCode">Qr Code </label>
                                <input class="form-control"  name="qrCode" id="qrCode<?=$item['idProduto']?>" placeholder="Inicie o texto com um numero: Ex.: 000texto " value="<?=$item['qrCode']?>"   maxlength="200"/>                               
                            </div> 
                            <div class="col-sm-3">
                                <label for="qrCode">Gerar Novo Qr </label><br>
                                <button type="button" class="btn btn-default" onclick="gerarQr('<?=$idProdutoMax?>', 'qrCode<?=$item['idProduto']?>')"><i class='fas fa-qrcode'></i></button>                       
                            </div>                                                                                          	                                
                        </div>

                        <div class="row">
                            
                            <div class="col-sm-6">
                                <label for="tipoGiftCard">Tipo GiftCard ( Caso o tipo de Produto seja GiftCard )</label>
                                <select class="form-control"  name="tipoGiftCard" >                     
                                    <?php foreach($paramTipoGiftCard as $itemP) {?>
                                    <option value="<?=$itemP['tipoGiftCard']?>" <?=(($itemP['tipoGiftCard'] == $item['tipoGiftCard']) ? 'selected':'')?>><?=$itemP['nomeTipoGiftCard']?></option>
                                    <?php }?>  
                                </select>                                        
                            </div>
                            <div class="col-sm-6">
                                <label for="nomeOperadoraRecarga">Tipo Operadora ( Caso o tipo de Produto seja Recarga )</label>
                                <select class="form-control"  name="nomeOperadoraRecarga" >                     
                                    <?php foreach($paramTipoOperadora as $itemP) {?>
                                    <option value="<?=$itemP['nomeOperadoraRecarga']?>" <?=(($itemP['nomeOperadoraRecarga'] == $item['nomeOperadoraRecarga']) ? 'selected':'')?>><?=$itemP['nomeOperadoraRecarga']?></option>
                                    <?php }?>  
                                </select>                                        
                            </div>                               	                                
                        </div>
                            
                    </fieldset> 
                    
                    <fieldset>
                        <legend>Descrições e Instruções</legend>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="textoDescricao">Descrição</label>
                                <textarea class="form-control"  name="textoDescricao"  rows="10"><?=$item['textoDescricao']?></textarea>                              
                            </div>                                                 	                                                        
                            <div class="col-md-6">
                                <label for="textoInstrucao">Instrução</label>
                                <textarea class="form-control"  name="textoInstrucao"  rows="10"><?=$item['textoInstrucao']?></textarea>                               
                            </div>                                                        	                                
                        </div>                                   
                    </fieldset> 

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="return enviaFormularioSimples('formVoltar')"  title="Voltar para Produtos">      Voltar      </button>
                <button type="button" class="btn btn-success"  onclick="return enviaFormularioSimples('modalFormEditar<?=$item['idProduto']?>')"  >Salvar Mudanças</button>
            </div>
        </form>
        
        
    </div>    
</div>

<?php  }?>


<form role="form" name="modalForm" id="formVoltar" action="?_p=prod" method="post">					                           		
    <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
    <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
</form>

<script>

    function gerarQr(idProdutoMax, idElemento){        
        var proximoId = "000" + "<?=idate("U")?>" +(idProdutoMax + 1);        
        $('#' + idElemento).val(proximoId);
    }

</script>   


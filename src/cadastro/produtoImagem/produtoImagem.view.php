<div class="container " style="overflow-y: auto; min-height:450px;">
    <div class="page-header">
        <h4>Estoque/Produtos/Imagens</h4>      
    </div>
	<div class="well">
        <form role="form" name="modalForm" action="<?=$textoDirecionar?>" method="post" autocomplete="off">
        <input type="hidden" name="tipoAcao"  value="S" />
            <div class="row">
                <div class="col-sm-4">
                    <label for="Nome">ID do Produto</label>
					<div class="form-group">
                        <span><?=$idProduto?></span>
					</div>
                </div>						
                <div class="col-sm-8">
                    <label for="Nome">Produto</label>
					<div class="form-group">
                        <span><?=$nomeProduto?></span>
					</div>
                </div>																
            </div>
        </form>		
    </div>
	
    <div class="panel panel-default">
        <div class="panel-heading ">
			<strong>Listagem das Imagens do Produto</strong>
        </div>
		<?php if($dadosProdutoImagem) {?>
			<input class="form-control" id="buscarModelo" type="text" placeholder="Buscar..">
		<?php }?>	
        <div class="table-responsive" style="overflow-y: auto; max-height:350px;">
            <table class="table table-hover table-striped sortable"  >
                <thead>
                    <tr>
                        <th><strong>ID</strong></th>
                        <th><strong>link da Imagem</strong></th>                        
                        <th><strong>link da Imagem Thumb</strong></th>                        
                        <th><strong></strong></th>						
                    </tr>
                </thead>                
                <tbody id="tableModelo">
                    <?php foreach($dadosProdutoImagem as $item) {
                        $textoAviso = '';
                        $tipoAlerta = '';?>
                    <tr class=" <?=$tipoAlerta?>" title="<?=$textoAviso?>">
                        <td><?=$item['idImagem']?></td>
                        <td><?=$item['nomeImagem']?></td>
                        <td><?=$item['nomeImagemSecundaria']?></td>
                         <td>                            
                            <button type="button" class="btn btn-primary" onclick="return enviaFormularioSimples('formEditar<?=$item['idImagem']?>')"  title="Editar registro!">Editar</button>
                            <button type="button" class="btn btn-danger" onclick="enviaFormExcluir('Deseja excluir a imagem <?=$item['nomeImagem']?>?', '<?=$item['idImagem']?>')"  title="Excluir Registro">Excluir</button>
                        </td>
                    </tr>
                    <?php $numeroLinhas++; }?>
                </tbody>
            </table>
        </div>
        <br>
        <div class="panel-footer">
            <?=$numeroLinhas." Registros encontrados..."?>
			<label for="Nome"> &nbsp; </label>
			<div class="form-group text-right">
            <button type="button" class="btn btn-default" onclick="return enviaFormularioSimples('formVoltar')"  title="Voltar para Produtos">      Voltar      </button>
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalAdicionar">+ Novo Registro</button>						
			</div>	
        </div>
    </div>    
</div>
<?php	
    include "produtoImagem.adicionar.view.php";
    include "produtoImagem.excluir.view.php";
?>

<?php foreach($dadosProdutoImagem as $item) {?>    
    <form role="form"  id="formEditar<?=$item['idImagem']?>" action="<?=$textoDirecionar?>" method="post">					                           		
        <input type="hidden" name="_subrota"  value="editar" />
        <input type="hidden" name="idProduto"  value="<?=$item['idProduto']?>" />
        <input type="hidden" name="nomeProduto"  value="<?=$item['nomeProduto']?>" />  
        <input type="hidden" name="idImagem"  value="<?=$item['idImagem']?>" />
        <input type="hidden" name="nomeImagem"  value="<?=$item['nomeImagem']?>" />        
        <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
        <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
    </form>
<?}?>


<form role="form" name="modalForm" id="formVoltar" action="?_p=prod" method="post">					                           		
    <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
    <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
</form>

<!-- Esse trecho é importante para colocar os IDs e Nomes para os Inputs -->
<div class="modal fade " data-backdrop="static" id="myModalAdicionar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-md">
        <div class="modal-content" >
            <form role="form" name="modalForm" id="modalFormAdicionar" action="<?=$textoDirecionar?>" method="post" autocomplete="off">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×   </span><span class="sr-only">Fechar</span>

                    </button>
                    <h4 class="modal-title" id="myModalLabel">Novo Registro</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">		
                        <input type="hidden" name="tipoAcao"  value="I" />
                        <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
                        <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
                        <input type="hidden" name="nomeProduto"  value="<?=$nomeProduto?>" />
                        <input type="hidden" name="idProduto"  value="<?=$idProduto?>" />
                        <input type="hidden" name="idParceiro"  value="<?=$idParceiroCorrente?>" />
                        
                        <fieldset>
                                <legend>Dados da Imagem</legend>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label for="Código">ID do Produto</label>
                                        <input class="form-control" Disabled  value="<?=$idProduto?>" />                               
                                    </div>
                                    <div class="col-sm-8">
                                        <label for="numeroParcela">Nome do Produto</label>
                                        <input class="form-control" Disabled value="<?=$nomeProduto?>" required/>
                                    </div>                                        
                                </div>                                        
                                <br>
                                <div class="row">               
                                    <div class="col-md-12">
                                        <div class="thumbnail col col-12 img-rounded" id="ativoIMG" data-toggle="tooltip" data-placement="top" >
                                            <img id="imagemCarregada" style="width: 250px;  height: 250px;" value="<?=$img_produto_padrao?>" />                                      
                                            <div class="caption">
                                                <center>
                                                    <p><label for="nomeImagem">Imagem Home</label></p>  
                                                    <input type="hidden" name="nomeImagem" id="nomeImagem" value="<?=$img_produto_padrao?>" require/>
                                                    <input type="hidden" name="nomeImagemSecundaria" id="nomeImagemSecundaria" value="<?=$img_produto_sec_padrao?>" require/>
                                                    <input type="hidden" name="atualizarImagem" id="atualizarImagem"  />
                                                    <label class="btn btn-primary" readonly="yes" onclick="editarImagem()"> Importar</label>                                        
                                                </center>
                                            </div>
                                        </div>
                                    </div>                            	                                
                                </div>                                        
                            </fieldset>       
                   
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="button" onclick="return enviaFormularioSimples('modalFormAdicionar')" class="btn btn-success" >Salvar Mudanças</button>
                </div>
            </form>
        </div>
    </div>
</div>
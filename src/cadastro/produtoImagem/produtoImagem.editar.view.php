<!-- Esse trecho é importante para colocar os IDs e Nomes para os Inputs -->
<?php foreach($dadosProdutoImagem as $item) {?>
	<div class="container " style="overflow-y: auto; min-height:450px;">    
        <div class="page-header">
            <h4 class="modal-title" id="myModalLabel">Editando Imagem [<?=$item['idImagem']?>]</h4>   
        </div>	
        <div class="panel panel-default">
            <form role="form" name="modalForm" id="modalFormEditar<?=$item['idImagem']?>" action="<?=$textoDirecionar?>" method="post">                
                <div class="panel-body">
                    <div class="form-group">                            		
                        <input type="hidden" name="tipoAcao"  value="E" />
                        <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
                        <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
                        <input type="hidden" name="idProduto"  value="<?=$idProduto?>" />
                        <input type="hidden" name="idImagem"  value="<?=$item['idImagem']?>" />
                        <input type="hidden" name="nomeProduto"  value="<?=$nomeProduto?>" />
                        <input type="hidden" name="_subrota"  value="editar" />

                        <fieldset>
                            <legend>Dados da Imagem</legend>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="Código">ID do Produto</label>
                                    <input class="form-control" Disabled  value="<?=$idProduto?>" />                               
                                </div>
                                <div class="col-sm-8">
                                    <label for="numeroParcela">Nome do Produto</label>
                                    <input class="form-control" Disabled value="<?=$nomeProduto?>" required/>
                                </div>   
                                <div class="col-sm-2">
                                    <label for="Código">ID</label>
                                    <input class="form-control" Disabled  value="<?=$item['idImagem']?>" />                               
                                </div>                          	                                
                            </div>        
                            <br>
                            <div class="row">               
                                <div class="col-md-12">
                                    <div class="thumbnail col col-12 img-rounded" id="ativoIMG" data-toggle="tooltip" data-placement="top" >
                                        <img id="imagemCarregada" style="width: 250px;  height: 250px;" src="<?=($item['nomeImagemSecundaria'] != "") ? $item['nomeImagemSecundaria'] : '' ?>" />                                      
                                        <div class="caption">
                                            <center>
                                                <p><label for="nomeImagem">Imagem Home</label></p>  
                                                <input type="hidden" name="nomeImagem" id="nomeImagem" value="<?=$item['nomeImagem']?>" require/>
                                                <input type="hidden" name="atualizarImagem" id="atualizarImagem"  />
                                                <label class="btn btn-primary" readonly="yes" onclick="editarImagem('<?=$item['nomeImagem']?>')"> Importar</label>                                        
                                            </center>
                                        </div>
                                    </div>
                                </div>                            	                                
                            </div>                                    
                        </fieldset>                            
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="return enviaFormularioSimples('formVoltar')"  title="Voltar para Produtos">      Voltar      </button>
                    <button type="button" class="btn btn-success"  onclick="return enviaFormularioSimples('modalFormEditar<?=$item['idImagem']?>')"  >Salvar Mudanças</button>
                </div>
            </form>
		</div>		
	</div>

<?php  }?>

<form role="form" name="modalForm" id="formVoltar" action="?_p=pimg" method="post">					                           		
    <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
    <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
    <input type="hidden" name="idProduto"  value="<?=$idProduto?>" />
    <input type="hidden" name="nomeProduto"  value="<?=$nomeProduto?>" />
</form>

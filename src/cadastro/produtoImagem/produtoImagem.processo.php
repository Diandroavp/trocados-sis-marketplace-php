<?php	
    $textoDirecionar = "?_p=".$_REQUEST["_p"];	
	$idOperadorCorrente = $_SESSION["_SESSION_idOperador"];
	$_subrota = (isset($_REQUEST['_subrota'])) ? $_REQUEST['_subrota'] : null;

	// Tipo de Pesquisa Da Tela Anterior
	$tipoPesquisa = (isset($_REQUEST['tipoPesquisa'])) ? $_REQUEST['tipoPesquisa'] : null;
	$textoPesquisa = (isset($_REQUEST['textoPesquisa'])) ? $_REQUEST['textoPesquisa'] : null;
	$idProduto = (isset($_REQUEST['idProduto'])) ? $_REQUEST['idProduto'] : null; 
	$nomeProduto = (isset($_REQUEST['nomeProduto'])) ? $_REQUEST['nomeProduto'] : null; 

	// CRUD
	$tipoAcao = (isset($_REQUEST['tipoAcao'])) ? $_REQUEST['tipoAcao'] : 'S';
	$idImagem = (isset($_REQUEST['idImagem']) && ($_REQUEST['idImagem'] <> '')) ? $_REQUEST['idImagem'] : null;  
	$nomeImagem = (isset($_REQUEST['nomeImagem'])) ? $_REQUEST['nomeImagem'] : null;  
	$nomeImagemSecundaria = (isset($_REQUEST['nomeImagemSecundaria'])) ? $_REQUEST['nomeImagemSecundaria'] : null;  
	$atualizarImagem = (isset($_REQUEST['atualizarImagem'])) ? $_REQUEST['atualizarImagem'] : null;  
	
	// Retorno de Resposta
	$tipoResposta = (isset($_REQUEST['tipoResposta'])) ? $_REQUEST['tipoResposta'] : null;
	$textoRetorno = (isset($_REQUEST['textoRetorno'])) ? $_REQUEST['textoRetorno'] : null;
	$ativoRetorno = (isset($_REQUEST['ativoRetorno'])) ? $_REQUEST['ativoRetorno'] : null;	
	 

    if (isset($_REQUEST['tipoAcao']) && ($tipoAcao != 'S')){
		
		// VERIFICAR SE É PARA SUBMETER UMA IMAGEM
		if ($atualizarImagem) {			
			if (strpos($nomeImagem, 'jpeg') > 0){
				$dadosImagem = updateImagem(str_replace('data:image/jpeg;base64,', '', $nomeImagem));
			} else if (strpos($nomeImagem, 'png') > 0){
				$dadosImagem = updateImagem(str_replace('data:image/png;base64,', '', $nomeImagem));
			} else if (strpos($nomeImagem, 'jpg') > 0){
				$dadosImagem = updateImagem(str_replace('data:image/jpg;base64,', '', $nomeImagem));
			}
			$nomeImagem = $dadosImagem['data']['url'];		
			$nomeImagemSecundaria = $dadosImagem['data']['thumb']['url'];  	
		} 

		$dadosResposta = produtoImagem($tipoAcao, $idImagem, $idProduto, $nomeImagem, $nomeImagemSecundaria);
		
		if ($dadosResposta and ($dadosResposta[0]['ativoRetorno'])){
			//AUDITORIA
			include "produtoImagem.auditoria.php";
		}
		//
		include "produtoImagem.processo.resposta.php";
    }
	if (isset($_REQUEST['tipoResposta'])){
		showMessage(($ativoRetorno == 1) ? "S" : "E", $_CONFIGURACAO_TITULO." - Informativo", $textoRetorno);
	}	


	// 
	$textoAviso = '';

	if (($tipoAcao == 'S')) {
		// Dados da grid principal		
		$dadosProdutoImagem = produtoImagem($tipoAcao, $idImagem, $idProduto, $nomeImagem, $nomeImagemSecundaria);	
		
		include "./funcoes/croperjs/index.php";
        iniciarEditorImagem('imagemCarregada', 250, 250, 'nomeImagem', 'atualizarImagem');
	}
	
    $numeroLinhas = 0;
?>


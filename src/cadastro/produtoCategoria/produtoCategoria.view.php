<div class="container " style="overflow-y: auto; min-height:450px; ">
    <div class="page-header">
        <h4>Movimento/Produto por Categoria</h4>      
    </div>
	<div class="well">
        <form role="form" name="modalForm" id="formPesquisa" action="<?=$textoDirecionar?>" method="post" autocomplete="off">
            <input type="hidden" name="tipoAcao"  value="S" />
            <div class="row">				
                <div class="col-sm-5">
                    <label for="Nome">Produto</label>
					<div class="form-group">
					  <select class="form-control" id="idProdutoPesquisa" name="idProdutoPesquisa" >							
                        <option <?=(($idProdutoPesquisa == null) ? 'selected':'')?>></option>
						<?php foreach($dadosProduto as $item) {?>
						  <option value="<?=$item['idProduto']?>" <?=(($item['idProduto'] == $idProdutoPesquisa) ? 'selected':'')?>><?=$item['nomeProduto']?></option>
						<?php }?>  
					  </select>
					</div>
                </div>		
                <div class="col-sm-5">
                    <label for="Nome">Categoria</label>
					<div class="form-group">
					  <select class="form-control" id="idCategoriaPesquisa" name="idCategoriaPesquisa" >							
                        <option <?=(($idCategoriaPesquisa == null) ? 'selected':'')?>></option>
						<?php foreach($dadosCategoria as $item) {?>
						  <option value="<?=$item['idCategoria']?>" <?=(($item['idCategoria'] == $idCategoriaPesquisa) ? 'selected':'')?>><?=$item['nomeCategoria']?></option>
						<?php }?>  
					  </select>
					</div>
                </div>		
				<div class="col-sm-2">
					<label for="Nome"> &nbsp; </label>
					<div class="form-group text-right">
						<button type="button" onclick="return enviaFormularioSimples('formPesquisa')" class="btn btn-primary"  > Localizar </button>
					</div>						
				</div>  							
            </div>
        </form>		
    </div>
	
    <div class="panel panel-default">
        <div class="panel-heading ">
			<strong>Listagem dos Produtos por Categoria</strong>
        </div>
		<?php if($dadosProdutoCategoria) {?>
			<input class="form-control" id="buscarModelo" type="text" placeholder="Buscar..">
		<?php }?>	
        <div class="table-responsive" style="overflow-y: auto; max-height:350px;">
            <table class="table table-hover table-striped sortable"  >
                <thead>
                    <tr>
                        <th>                                                                                              
                           <center><label><input type="checkbox" name="checkAll" id="checkAll" /></label></center>
                        </th>
                        <th><strong>Código</strong></th>
                        <th><strong>Produto</strong></th>
                        <th><strong>Código</strong></th>
                        <th><strong>Categoria</strong></th>
                        <th><strong>Vinculado</strong></th>
                        <th><strong></strong></th>						
                    </tr>
                </thead>                
                <tbody id="tableModelo">
                    <?php foreach($dadosProdutoCategoria as $item) {?>
                    <tr>
                        <td>
                            <center><label><input type="checkbox"  name="check[]" id="check<?=$item['idProduto'].'T'.$item['idCategoria']?>" value="1" /></label> </center>
                        </td>
                        <td><?=$item['idProduto']?></td>
                        <td><?=$item['nomeProduto']?></td>
                        <td><?=$item['idCategoria']?></td>
                        <td><?=$item['nomeCategoria']?></td>       
                        <td><?=SimOuNao($item['ativoEncontrado'])?></td>       
                        <td>
                            <?php if($item['ativoEncontrado']) {?>
                                <button type="button" class="btn btn-danger" onclick="enviaFormExcluir('Deseja excluir o produto <?=$item['nomeProduto']?> da categoria <?=$item['nomeCategoria']?>?', '<?=$item['idProduto'].$item['idCategoria']?>')"  title="Excluir Registro">Excluir</button>
                            <?php }?>
                        </td>
                    </tr>
                    <?php $numeroLinhas++; }?>
                </tbody>                
            </table>            
        </div>
        <br>
        <div class="panel-footer">
            <?=$numeroLinhas." Registros encontrados..."?>
			<label for="Nome"> &nbsp; </label>
			<div class="form-group text-right">
                <button type="button" class="btn btn-primary" id="run_incluir" >Adicionar</button>	
				<button type="button" class="btn btn-danger" id="run_deletar" >Remover</button>						
			</div>	
        </div>
    </div>    
</div>
<form role="form" id="formIncluirTodos" action="<?=$textoDirecionar?>" method="post" autocomplete="off">
    <input type="hidden" name="tipoAcao"  value="IT" />
    <input type="hidden" name="idProdutoPesquisa"  value="<?=$idProdutoPesquisa?>" />
	<input type="hidden" name="idCategoriaPesquisa"  value="<?=$idCategoriaPesquisa?>" />
</form>
<form role="form" id="formExcluirTodos" action="<?=$textoDirecionar?>" method="post" autocomplete="off">
    <input type="hidden" name="tipoAcao"  value="DT" />
    <input type="hidden" name="idProdutoPesquisa"  value="<?=$idProdutoPesquisa?>" />
	<input type="hidden" name="idCategoriaPesquisa"  value="<?=$idCategoriaPesquisa?>" />
</form>
<?php
   include "produtoCategoria.excluir.view.php";
?>
<script>
     $(document).ready(function () {
        /*SELECIONAR TODOS OS REGISTROS*/
        $("#checkAll").on("click", function () {            
            var checkAll = document.getElementById('checkAll');
            var els = document.getElementsByName('check[]');
            for (var i = 0; i < els.length; i++) {
                if ($('#' + els[i].id).is(":visible")) {
                    els[i].checked = checkAll.checked;
                }
            }
        });


        /*DELETAR TODOS OS RELACIONAMENTOS SELECIONADOS*/
        $("#run_deletar").on("click", function () {
            existeRegistro = 0;

            var els = document.getElementsByName('check[]');

            for (var i = 0; i < els.length; i++) {
                if (els[i].checked) {
                    existeRegistro = 1;
                }
            }
            if (existeRegistro) {
                if (window.confirm("Voce deseja remover todas as relações selecionadas selecionada(s)?")) {

                    var parent = document.getElementById("formExcluirTodos");

                    for (var i = 0; i < els.length; i++) {
                        if (els[i].checked) {
                            var input = document.createElement("input");
                            input.setAttribute('type', 'hidden');
                            input.setAttribute('name', 'idall[]');
                            input.setAttribute('value', els[i].id.substring(5, (els[i].id.length)));

                            parent.appendChild(input);
                        }
                    }

                    enviaFormularioSimples('formExcluirTodos');
                }
            } else {
                alert('Nenhuma registro selecionada!');
            }

        });

        /*INCLUIR TODOS OS RELACIONAMENTOS SELECIONADOS*/
        $("#run_incluir").on("click", function () {
            existeRegistro = 0;

            var els = document.getElementsByName('check[]');

            for (var i = 0; i < els.length; i++) {
                if (els[i].checked) {
                    existeRegistro = 1;
                }
            }
            if (existeRegistro) {
                if (window.confirm("Voce deseja incluir todas as relações selecionadas selecionada(s)?")) {

                    var parent = document.getElementById("formIncluirTodos");

                    for (var i = 0; i < els.length; i++) {
                        if (els[i].checked) {
                            var input = document.createElement("input");
                            input.setAttribute('type', 'hidden');
                            input.setAttribute('name', 'idall[]');
                            input.setAttribute('value', els[i].id.substring(5, (els[i].id.length)));

                            parent.appendChild(input);
                        }
                    }

                    enviaFormularioSimples('formIncluirTodos');
                }
            } else {
                alert('Nenhuma registro selecionada!');
            }

        });
    });
</script>


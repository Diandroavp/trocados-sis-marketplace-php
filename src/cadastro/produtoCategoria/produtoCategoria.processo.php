<?php		
	$textoDirecionar = "?_p=".$_REQUEST["_p"];	
	$idOperadorCorrente = $_SESSION["_SESSION_idOperador"];
	// Tipo de Pesquisa
	$idProdutoPesquisa = (isset($_REQUEST['idProdutoPesquisa']) && ($_REQUEST['idProdutoPesquisa'] <> '')) ? $_REQUEST['idProdutoPesquisa'] : null;
	$idCategoriaPesquisa = (isset($_REQUEST['idCategoriaPesquisa']) && ($_REQUEST['idCategoriaPesquisa'] <> '')) ? $_REQUEST['idCategoriaPesquisa'] : null;
	// CRUD
	$tipoAcao = (isset($_REQUEST['tipoAcao'])) ? $_REQUEST['tipoAcao'] : 'S';
	$idCategoria = (isset($_REQUEST['idCategoria'])) ? $_REQUEST['idCategoria'] : null;  
	$idProduto = (isset($_REQUEST['idProduto'])) ? $_REQUEST['idProduto'] : null;  	
	// Retorno de Resposta
	$tipoResposta = (isset($_REQUEST['tipoResposta'])) ? $_REQUEST['tipoResposta'] : null;
	$textoRetorno = (isset($_REQUEST['textoRetorno'])) ? $_REQUEST['textoRetorno'] : null;
	$ativoRetorno = (isset($_REQUEST['ativoRetorno'])) ? $_REQUEST['ativoRetorno'] : null;	


    if (isset($_REQUEST['tipoAcao']) && ($tipoAcao != 'S')){
		
		switch ($tipoAcao) {

			case "IT":
				if (is_array($_REQUEST['idall'])) {
					foreach ($_REQUEST['idall'] as $item) {

						$idall = explode('T', $item);						
						$idProduto = $idall[0];
						$idCategoria = $idall[1];

						$dadosResposta = produtoCategoria('I', $idCategoria, $idProduto, $idParceiroCorrente);
						if ($dadosResposta) {
							if ($dadosResposta[0]['textoRetorno'] <> '') {
								$textoRetorno = $textoRetorno.'<br> Produto: '.$idProduto.' - Categoria: '.$idCategoria.' - Retorno: '.$dadosResposta[0]['textoRetorno'];
							}
						}
					}
					//AUDITORIA
					include "produtoCategoria.auditoria.php";
				}
				
				break;	
			case "DT":
				if (is_array($_REQUEST['idall'])) {
					foreach ($_REQUEST['idall'] as $item) {

						$idall = explode('T', $item);						
						$idProduto = $idall[0];
						$idCategoria = $idall[1];

						$dadosResposta = produtoCategoria('D', $idCategoria, $idProduto, $idParceiroCorrente);
						if ($dadosResposta) {
							if ($dadosResposta[0]['textoRetorno'] <> '') {
								$textoRetorno = $textoRetorno.'<br> Produto: '.$idProduto.' - Categoria: '.$idCategoria.' - Retorno: '.$dadosResposta[0]['textoRetorno'];
							}
						}
					}
					//AUDITORIA
					include "produtoCategoria.auditoria.php";
				}
				
				break;
			default:
				$dadosResposta = produtoCategoria('D', $idCategoria, $idProduto, $idParceiroCorrente);
				if ($dadosResposta and ($dadosResposta[0]['ativoRetorno'])){

					$textoRetorno = $textoRetorno.'<br> Produto: '.$idProduto.' - Categoria: '.$idCategoria.' - Retorno: '.$dadosResposta[0]['textoRetorno'];
					//AUDITORIA
					include "produtoCategoria.auditoria.php";
				}
			break;
		
		}
		//
		include "produtoCategoria.processo.resposta.php";
    }
	if (isset($_REQUEST['tipoResposta'])){
		showMessage(($ativoRetorno == 1) ? "S" : "E", $_CONFIGURACAO_TITULO." - Informativo", $textoRetorno);
	}	

	// 
	if (($tipoAcao == 'S')) {
		// Dados da grid principal
		$dadosProdutoCategoria = produtoCategoria($tipoAcao, $idCategoriaPesquisa, $idProdutoPesquisa, $idParceiroCorrente);

		$dadosProduto = produto($tipoAcao, $idParceiroCorrente, null,null,null,null,null,null,null,null,null,null, null,null,null,null,null,null,null, null, null, null	);		
		$dadosCategoria = categoria($tipoAcao, null,null,null,null,null,null);
	}
		
	

    $numeroLinhas = 0;
	$totalValorCompra = 0;
?>


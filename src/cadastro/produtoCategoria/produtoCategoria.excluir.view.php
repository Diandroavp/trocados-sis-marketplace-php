
<!-- Esse trecho é importante para colocar os IDs e Nomes para os Inputs -->
<div class="hide">
    <?php foreach($dadosProdutoCategoria as $item) {?>	
        <form role="form" id="modalExcluir<?=$item['idProduto'].$item['idCategoria']?>" action="<?=$textoDirecionar?>" method="post">
          <input type="hidden" name="tipoAcao"  value="D" />
          <input type="hidden" name="idProdutoPesquisa"  value="<?=$idProdutoPesquisa?>" />
	        <input type="hidden" name="idCategoriaPesquisa"  value="<?=$idCategoriaPesquisa?>" />
          <input type="hidden" name="idProduto"  value="<?=$item['idProduto']?>" />            
          <input type="hidden" name="idCategoria" id="idCategoria" value="<?=$item['idCategoria']?>" />            
        </form>
    <?php  }?>
</div>

<?php	
    $textoDirecionar = "?_p=".$_REQUEST["_p"];	
	$idOperadorCorrente = $_SESSION["_SESSION_idOperador"];
	// Tipo de Pesquisa
	$tipoPesquisa = (isset($_REQUEST['tipoPesquisa'])) ? $_REQUEST['tipoPesquisa'] : null;
	$textoPesquisa = (isset($_REQUEST['textoPesquisa'])) ? $_REQUEST['textoPesquisa'] : null;
	// CRUD
	$tipoAcao = (isset($_REQUEST['tipoAcao'])) ? $_REQUEST['tipoAcao'] : 'S';
	
	$idDoacaoExterna = (isset($_REQUEST['idDoacaoExterna'])) ? $_REQUEST['idDoacaoExterna'] : null;  
	$dataEvento = (isset($_REQUEST['dataEvento'])) ? $_REQUEST['dataEvento'] : date('Y-m-d');  
	$numeroDoacoes = (isset($_REQUEST['numeroDoacoes'])) ? $_REQUEST['numeroDoacoes'] : null;  
	$quantidadeQuilos = (isset($_REQUEST['quantidadeQuilos'])) ? $_REQUEST['quantidadeQuilos'] : null;  
	  
	// Retorno de Resposta
	$tipoResposta = (isset($_REQUEST['tipoResposta'])) ? $_REQUEST['tipoResposta'] : null;
	$textoRetorno = (isset($_REQUEST['textoRetorno'])) ? $_REQUEST['textoRetorno'] : null;
	$ativoRetorno = (isset($_REQUEST['ativoRetorno'])) ? $_REQUEST['ativoRetorno'] : null;	
	

    if (isset($_REQUEST['tipoAcao']) && ($tipoAcao != 'S')){
		
		$dadosResposta = doacaoExterna($tipoAcao, 
										$idDoacaoExterna, 
										$numeroDoacoes, 
										$quantidadeQuilos, 
										$dataEvento);
								
		if ($dadosResposta and ($dadosResposta[0]['ativoRetorno'])){
			//AUDITORIA
			include "doacaoexterna.auditoria.php";
		}
		//
		include "doacaoexterna.processo.resposta.php";
    }
	if (isset($_REQUEST['tipoResposta'])){
		showMessage(($ativoRetorno == 1) ? "S" : "E", $_CONFIGURACAO_TITULO." - Informativo", $textoRetorno);
	}	

	// Montando tipo de Pesquisa
	$paramPesquisa = array();
	$paramPesquisa[] = array('idDoacaoExterna', 'Código');

	// 
	if (($tipoAcao == 'S')) {
		// Dados da grid principal
		$dadosDoacaoExterna = doacaoExterna($tipoAcao, 
										(($textoPesquisa != '') && ($tipoPesquisa == 'idDoacaoExterna')) ? $textoPesquisa : null, 										
										$numeroDoacoes, 
										$quantidadeQuilos,
										$dataEvento);
	}
		
    $numeroLinhas = 0;
?>


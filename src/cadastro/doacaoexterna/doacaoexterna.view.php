<div class="container " style="overflow-y: auto; min-height:450px; ">
    <div class="page-header">
        <h4>Cadastro/Compras Externas</h4>      
    </div>
	<div class="well">
        <form role="form" name="modalForm" id="formPesquisa" action="<?=$textoDirecionar?>" method="post" autocomplete="off">
        <input type="hidden" name="tipoAcao"  value="S" />
        <input type="hidden" name="idParceiroCorrente"  value="<?=$idParceiroCorrente?>" />  
            <div class="row">				
                <div class="col-sm-2">
                    <label for="Nome">Tipo de Pesquisa</label>
					<div class="form-group">
					  <select class="form-control" id="tipoPesquisa" name="tipoPesquisa" required>							
						<?php foreach($paramPesquisa as $item) {?>
						  <option value="<?=$item[0]?>" <?=(($item[0] == $tipoPesquisa) ? 'selected':'')?>><?=$item[1]?></option>
						<?php }?>  
					  </select>
					</div>
                </div>					
				<div class="col-sm-8">
                    <label for="Nome">Texto para pesquisa</label>
					<div class="form-group">
						<input class="form-control somente-numero" name="textoPesquisa" id="textoPesquisa" value="<?=$textoPesquisa?>" />  
					</div>
                </div>				
				<div class="col-sm-2">
					<label for="Nome"> &nbsp; </label>
					<div class="form-group text-right">
						<button type="button" onclick="return enviaFormularioSimples('formPesquisa')" class="btn btn-primary" > Localizar </button>
					</div>						
				</div>  							
            </div>
        </form>		
    </div>
	
    <div class="panel panel-default">
        <div class="panel-heading ">
			<strong>Listagem das Doaçõex Externas</strong>
        </div>
		<?php if($dadosDoacaoExterna) {?>
			<input class="form-control" id="buscarModelo" type="text" placeholder="Buscar..">
		<?php }?>	
        <div class="table-responsive" style="overflow-y: auto; max-height:350px;">
            <table class="table table-hover table-striped sortable"  >
                <thead>
                    <tr>
                        <th><strong>Código</strong></th>
                        <th><strong>Data do Evento</strong></th>
                        <th><strong>Número Compras</strong></th>
                        <th><strong>Quantidade Compras</strong></th>                        
                        <th><strong></strong></th>						
                    </tr>
                </thead>                
                <tbody id="tableModelo">
                    <?php foreach($dadosDoacaoExterna as $item) {?>
                    <tr>
                        <td><?=$item['idDoacaoExterna']?></td>
                        <td><?=date_dd_mm_yyyy($item['dataEvento'])?></td>
                        <td><?=$item['numeroDoacoes']?></td>
                        <td><?=$item['quantidadeQuilos']?></td>
                        <td>     
                            <button class="btn btn-primary" data-toggle="modal" data-target="#ModeloEditar<?=$item['idDoacaoExterna']?>" title="Editar Registro" >Editar</button>                                                    
                            <button type="button" class="btn btn-danger" onclick="enviaFormExcluir('Deseja excluir o registro <?=$item['idDoacaoExterna']?>?', '<?=$item['idDoacaoExterna']?>')"  title="Excluir Registro">Excluir</button>
                        </td>
                    </tr>
                    <?php $numeroLinhas++; }?>
                </tbody>
            </table>
        </div>
        <br>
        <div class="panel-footer">
            <?=$numeroLinhas." Registros encontrados..."?>
			<label for="Nome"> &nbsp; </label>
			<div class="form-group text-right">
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalAdicionar">+ Novo Registro</button>						
			</div>	
        </div>
    </div>    
</div>
<?php
	include "doacaoexterna.editar.view.php";
    include "doacaoexterna.adicionar.view.php";
    include "doacaoexterna.excluir.view.php";
?>
 


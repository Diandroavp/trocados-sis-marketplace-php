<!-- Esse trecho é importante para colocar os IDs e Nomes para os Inputs -->
<div class="modal fade " data-backdrop="static" id="myModalAdicionar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-md">
        <div class="modal-content" >
            <form role="form" name="modalForm" id="modalFormAdicionar" action="<?=$textoDirecionar?>" method="post" autocomplete="off">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×   </span><span class="sr-only">Fechar</span>

                    </button>
                    <h4 class="modal-title" id="myModalLabel">Novo Registro</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">		
                        <input type="hidden" name="tipoAcao"  value="I" />
                        <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
	                    <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
                        <input type="hidden" name="idParceiro"  value="<?=$idParceiroCorrente?>" />
                        
                        <fieldset>
                            <legend>Dados do Operador</legend>
                            <div class="row">
                                <div class="col-sm-8">
                                    <label for="nomeOperador">Operador</label>
                                    <input class="form-control" name="nomeOperador" id="nomeOperador" value="" required />
                                </div>
                                <div class="col-sm-4">
                                    <label for="Senha">Senha</label>
                                    <input class="form-control" type="password" name="senhaOperador" id="senhaOperador" value="" required  autocomplete="off" />                               
                                </div>                                	                                
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="eMail">e-Mail</label>
                                    <input class="form-control" name="eMail" id="eMail" value="" required />                                  
                                </div>                                    
                            </div>   
                            <div class="row">                                    
                                    <div class="col-sm-12">
                                        <label for="idPerfil">Perfil</label>
                                        <div class="form-group">
                                            <select class="form-control"  name="idPerfil" >                     
                                                <?php foreach($dadosPerfilConsultar as $itemP) {?>
                                                <option value="<?=$itemP['idPerfil']?>" ><?=$itemP['nomePerfil']?></option>
                                                <?php }?>  
                                            </select>
                                        </div>                                      
                                    </div>                                
                            </div>
                            <div class="row">                                    
                                <div class="col-sm-8">                                    
                                    <label for="Nome">Nível</label>
                                    <div class="form-group">
                                        <select class="form-control" id="tipoNivel" name="tipoNivel" required>							                                                
                                            <option value="b" selected>Básico</option>         
                                            <option value="s" >Supervisor</option>                                         
                                            <option value="a" >Administrador</option>                                                
                                            <option value="c" >Convidado</option>                                                
                                            <option value="e" >Estoque</option>                                                 
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">                                                                        
                                    <div class="checkbox">
                                        <br>                                                                                    
                                        <label><input type="checkbox" name="ativoOperador" id="ativoOperador" value="1" checked> Ativo</label>                                          
                                    </div>
                                </div>                                	                                
                            </div>   
                        </fieldset>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="button" onclick="return enviaFormularioSimples('modalFormAdicionar')" class="btn btn-success" >Salvar Mudanças</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Esse trecho é importante para colocar os IDs e Nomes para os Inputs -->
<?php foreach($dadosOperador as $item) {?>
	<div class="modal fade" data-backdrop="static" id="ModeloEditar<?=$item['idOperador']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-ms">
			<div class="modal-content">
				<form role="form" name="modalForm" id="modalFormEditar<?=$item['idOperador']?>" action="<?=$textoDirecionar?>" method="post">
					<div class="modal-header bg-primary">
						<button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×   </span><span class="sr-only">Fechar</span>

						</button>
						<h4 class="modal-title" id="myModalLabel">Editando Registro [<?=$item['nomeOperador']?>]</h4>
					</div>
					<div class="modal-body">
                        <div class="form-group">		
                            <input type="hidden" name="tipoAcao"  value="E" />
                            <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
                            <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
                            <input type="hidden" name="idOperador"  value="<?=$item['idOperador']?>" /> 
                        
                            <fieldset>
                                <legend>Dados do Operador</legend>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label for="Código">Código</label>
                                        <input class="form-control" Disabled  value="<?=$item['idOperador']?>" />                               
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="nomeOperador">Operador</label>
                                        <input class="form-control" name="nomeOperador" id="nomeOperador" value="<?=$item['nomeOperador']?>" required />
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="Senha">Senha</label>
                                        <input class="form-control" type="password" name="senhaOperador" id="senhaOperador" value="<?=$item['senhaOperador']?>" required />                               
                                    </div>                                	                                
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label for="eMail">e-Mail</label>
                                        <input class="form-control" name="eMail" id="eMail" value="<?=$item['eMail']?>" required />                               
                                    </div>                                    
                                </div>   
                                <div class="row">                                    
                                    <div class="col-sm-12">
                                        <label for="idPerfil">Perfil</label>
                                        <div class="form-group">
                                            <select class="form-control"  name="idPerfil" >                     
                                                <?php foreach($dadosPerfilConsultar as $itemP) {?>
                                                <option value="<?=$itemP['idPerfil']?>" <?=(($itemP['idPerfil'] == $item['idPerfil']) ? 'selected':'')?>><?=$itemP['nomePerfil']?></option>
                                                <?php }?>  
                                            </select>
                                        </div>                                      
                                    </div>  
                                    <div class="col-sm-8">                                    
                                        <label for="Nome">Nível</label>
                                        <div class="form-group">
                                            <select class="form-control" id="tipoNivel" name="tipoNivel" required>							                                                
                                                <option value="b" <?=(('b' == $item['tipoNivel']) ? 'selected':'')?>>Básico</option>         
                                                <option value="s" <?=(('s' == $item['tipoNivel']) ? 'selected':'')?>>Supervisor</option>                                         
                                                <option value="a" <?=(('a' == $item['tipoNivel']) ? 'selected':'')?>>Administrador</option>                                                
                                                <option value="c" <?=(('c' == $item['tipoNivel']) ? 'selected':'')?>>Convidado</option>                                                
                                                <option value="e" <?=(('e' == $item['tipoNivel']) ? 'selected':'')?>>Estoque</option>                                                
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">                                                                            
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoOperador" id="ativoOperador" value="1" <?=($item['ativoOperador'] ? 'checked' : '' )?>> Ativo</label>                                          
                                        </div>
                                    </div>                                	                                
                                    
                                </div>   
                            </fieldset>
                        </div>
                    </div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
						<button type="button" class="btn btn-success"  onclick="return enviaFormularioSimples('modalFormEditar<?=$item['idOperador']?>')"  >Salvar Mudanças</button>
					</div>
				</form>
			</div>
		</div>
	</div>

<?php  }?>


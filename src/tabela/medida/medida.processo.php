<?php	
    $textoDirecionar = "?_p=".$_REQUEST["_p"];	
	$idOperadorCorrente = $_SESSION["_SESSION_idOperador"];
	// Tipo de Pesquisa
	$tipoPesquisa = (isset($_REQUEST['tipoPesquisa'])) ? $_REQUEST['tipoPesquisa'] : null;
	$textoPesquisa = (isset($_REQUEST['textoPesquisa'])) ? $_REQUEST['textoPesquisa'] : null;
	// CRUD
	$tipoAcao = (isset($_REQUEST['tipoAcao'])) ? $_REQUEST['tipoAcao'] : 'S';
	
	$idMedida = (isset($_REQUEST['idMedida'])) ? $_REQUEST['idMedida'] : null;  
	$siglaMedida = (isset($_REQUEST['siglaMedida'])) ? $_REQUEST['siglaMedida'] : null;  
	$nomeMedida = (isset($_REQUEST['nomeMedida'])) ? $_REQUEST['nomeMedida'] : null;  
	  
	// Retorno de Resposta
	$tipoResposta = (isset($_REQUEST['tipoResposta'])) ? $_REQUEST['tipoResposta'] : null;
	$textoRetorno = (isset($_REQUEST['textoRetorno'])) ? $_REQUEST['textoRetorno'] : null;
	$ativoRetorno = (isset($_REQUEST['ativoRetorno'])) ? $_REQUEST['ativoRetorno'] : null;	
	

    if (isset($_REQUEST['tipoAcao']) && ($tipoAcao != 'S')){
		
		$dadosResposta = medida( $tipoAcao, 
								$idMedida, 
								$siglaMedida, 
								$nomeMedida);
								
		if ($dadosResposta and ($dadosResposta[0]['ativoRetorno'])){
			//AUDITORIA
			include "medida.auditoria.php";
		}
		//
		include "medida.processo.resposta.php";
    }
	if (isset($_REQUEST['tipoResposta'])){
		showMessage(($ativoRetorno == 1) ? "S" : "E", $_CONFIGURACAO_TITULO." - Informativo", $textoRetorno);
	}	

	// Montando tipo de Pesquisa
	$paramPesquisa = array();
	$paramPesquisa[] = array('idMedida', 'Código');
	$paramPesquisa[] = array('siglaMedida', 'Sigla');
	$paramPesquisa[] = array('nomeMedida', 'Nome');
	if (!$tipoPesquisa){
		$tipoPesquisa = $paramPesquisa[2][0];
	}	

	// 
	if (($tipoAcao == 'S')) {
		// Dados da grid principal
		$dadosMedida = medida($tipoAcao, 
							(($textoPesquisa != '') && ($tipoPesquisa == 'idMedida')) ? $textoPesquisa : null,
							(($textoPesquisa != '') && ($tipoPesquisa == 'siglaMedida')) ? $textoPesquisa : null,
							(($textoPesquisa != '') && ($tipoPesquisa == 'nomeMedida')) ? $textoPesquisa : null);
	}
		
    $numeroLinhas = 0;
?>


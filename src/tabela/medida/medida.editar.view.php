<!-- Esse trecho é importante para colocar os IDs e Nomes para os Inputs -->
<?php foreach($dadosMedida as $item) {?>
	<div class="modal fade" data-backdrop="static" id="ModeloEditar<?=$item['idMedida']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<form role="form" name="modalForm" id="modalFormEditar<?=$item['idMedida']?>" action="<?=$textoDirecionar?>" method="post">
					<div class="modal-header bg-primary">
						<button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×   </span><span class="sr-only">Fechar</span>

						</button>
						<h4 class="modal-title" id="myModalLabel">Editando Registro [<?=$item['idMedida']?>]</h4>
					</div>
					<div class="modal-body">
                        <div class="form-group">		
                            <input type="hidden" name="tipoAcao"  value="E" />
                            <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
                            <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
                            <input type="hidden" name="idMedida"  value="<?=$item['idMedida']?>" />
                        
                            <fieldset>
                                <legend>Dados da Medida</legend>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label for="Código">Código</label>
                                        <input class="form-control" Disabled  value="<?=$item['idMedida']?>" />                               
                                    </div>
                                </div>
                                <div class="row">                                    
                                    <div class="col-sm-4">
                                        <label for="siglaMedida">Sigla</label>
                                        <input class="form-control" type="text" name="siglaMedida" value="<?=$item['siglaMedida']?>" required  autocomplete="off" />                               
                                    </div>
                                    <div class="col-sm-8">
                                        <label for="nomeMedida">Descrição</label>
                                        <input class="form-control" type="text" name="nomeMedida" value="<?=$item['nomeMedida']?>" required  autocomplete="off" />                               
                                    </div>                               	                                
                                </div>                                
                            </fieldset>
                        </div>
                    </div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
						<button type="button" class="btn btn-success"  onclick="return enviaFormularioSimples('modalFormEditar<?=$item['idMedida']?>')"  >Salvar Mudanças</button>
					</div>
				</form>
			</div>
		</div>
	</div>

<?php  }?>

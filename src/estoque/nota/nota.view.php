<div class="container " style="overflow-y: auto; min-height:450px; ">
    <div class="page-header">
        <h4>Movimento/Notas</h4>      
    </div>
	<div class="well">
        <form role="form" name="modalForm" id="formPesquisa" action="<?=$textoDirecionar?>" method="post" autocomplete="off">
            <input type="hidden" name="tipoAcao"  value="S" />
            <input type="hidden" name="idParceiroCorrente"  value="<?=$idParceiroCorrente?>" />  
            <div class="row">				
                <div class="col-sm-6">
                    <label for="dataInicio">Data Inicial</label>
					<div class="form-group">
                        <input type="date" class="form-control" name="dataInicio" id="dataInicio" value="<?=$dataInicio?>" />  
					</div>
                </div>					
                <div class="col-sm-6">
                    <label for="dataFinal">Data Final</label>
					<div class="form-group">
                        <input type="date" class="form-control" name="dataFinal" id="dataFinal" value="<?=$dataFinal?>" />  
					</div>
                </div>					
            </div>
            <div class="row">				
                <div class="col-sm-2">
                    <label for="Nome">Tipo de Pesquisa</label>
					<div class="form-group">
					  <select class="form-control" id="tipoPesquisa" name="tipoPesquisa" required>							
						<?php foreach($paramPesquisa as $item) {?>
						  <option value="<?=$item[0]?>" <?=(($item[0] == $tipoPesquisa) ? 'selected':'')?>><?=$item[1]?></option>
						<?php }?>  
					  </select>
					</div>
                </div>					
				<div class="col-sm-8">
                    <label for="Nome">Texto para pesquisa</label>
					<div class="form-group">
						<input class="form-control somente-numero" name="textoPesquisa" id="textoPesquisa" value="<?=$textoPesquisa?>" />  
					</div>
                </div>				
				<div class="col-sm-2">
					<label for="Nome"> &nbsp; </label>
					<div class="form-group text-right">
						<button type="button" onclick="return enviaFormularioSimples('formPesquisa')" class="btn btn-primary"  > Localizar </button>
					</div>						
				</div>  							
            </div>
        </form>		
    </div>
	
    <div class="panel panel-default">
        <div class="panel-heading ">
			<strong>Listagem das Notas</strong>
        </div>
		<?php if($dadosEstoqueNota) {?>
			<input class="form-control" id="buscarModelo" type="text" placeholder="Buscar..">
		<?php }?>	
        <div class="table-responsive" style="overflow-y: auto; max-height:350px;">
            <table class="table table-hover table-striped sortable"  >
                <thead>
                    <tr>
                        <th><strong>Código</strong></th>
                        <th><strong>Data Nota</strong></th>                        
                        <th><strong>Operador</strong></th>
                        <th><strong>Tipo da Nota</strong></th>                                                
                        <th><strong>Fechamento</strong></th>                                                
                        <th><strong></strong></th>						
                    </tr>
                </thead>                
                <tbody id="tableModelo">
                    <?php foreach($dadosEstoqueNota as $item) {?>
                    <tr  title="Descrição da Nota: <?=$item['textoDescricao']?>">
                        <td><?=$item['idEstoqueNota']?></td>
                        <td><?=date_dd_mm_yyyy_hh_mm_ss($item['dataNota'])?></td>                                                
                        <td><?=$item['nomeOperador']?></td>
                        <td><?=$item['nomeTipoNota']?></td>   
                        <td><?=date_dd_mm_yyyy_hh_mm_ss($item['dataFechamento'])?></td>                         
                        <td>
                            <?php if(!$item['dataFechamento']) {?>
                                <button class="btn btn-primary" data-toggle="modal" data-target="#ModeloEditar<?=$item['idEstoqueNota']?>" title="Editar Registro" >Editar</button>                                                    
                                <button type="button" class="btn btn-danger" onclick="enviaFormExcluir('Deseja excluir o registro <?=$item['idEstoqueNota']?>?', '<?=$item['idEstoqueNota']?>')"  title="Excluir Registro">Excluir</button>
                            <?php }?>                            
                            <button type="button" class="btn btn-primary" onclick="return enviaFormularioSimples('formProdutos<?=$item['idEstoqueNota']?>')"  title="Produtos da Nota">Produtos</button>
                            <?php if(!$item['dataFechamento']) {?>
                                <button class="btn btn-warning" onclick="enviaformFechar('Deseja fechar a nota <?=$item['idEstoqueNota']?>?', '<?=$item['idEstoqueNota']?>')" title="Fechar Nota" >Fechar</button>                                                                                                            
                            <?php } else {?>
                                <button class="btn btn-warning" onclick="enviaformAbrir('Deseja abrir a nota <?=$item['idEstoqueNota']?>?', '<?=$item['idEstoqueNota']?>')" title="Abrir Nota" >Abrir</button>                                                                                                            
                            <?php }?>
                        </td>
                    </tr>
                    <?php $numeroLinhas++; }?>
                </tbody>
            </table>
        </div>
        <br>
        <div class="panel-footer">
            <?=$numeroLinhas." Registros encontrados..."?>
			<label for="Nome"> &nbsp; </label>
			<div class="form-group text-right">
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalAdicionar">+ Novo Registro</button>						
			</div>	
        </div>
    </div>    
</div>
<?php
    include "nota.adicionar.view.php";
    include "nota.editar.view.php";
    include "nota.excluir.view.php";
    include "nota.fechar.view.php";
    include "nota.abrir.view.php";
?>


<?php foreach($dadosEstoqueNota as $item) {?>
    <form role="form"  id="formProdutos<?=$item['idEstoqueNota']?>" action="?_p=nprod" method="post">					                           		
        <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
        <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
        <input type="hidden" name="idEstoqueNota"  value="<?=$item['idEstoqueNota']?>" />
        <input type="hidden" name="dataNota"  value="<?=$item['dataNota']?>" />
        <input type="hidden" name="nomeTipoNota"  value="<?=$item['nomeTipoNota']?>" />
        <input type="hidden" name="tipoNota"  value="<?=$item['tipoNota']?>" />
        <input type="hidden" name="dataInicio"  value="<?=$dataInicio?>" />
        <input type="hidden" name="dataFinal"  value="<?=$dataFinal?>" />
    </form>
<?}?>
<?php	
	$textoDirecionar = "?_p=".$_REQUEST["_p"];	
	$idOperadorCorrente = $_SESSION["_SESSION_idOperador"];
	// Tipo de Pesquisa
	$tipoPesquisa = (isset($_REQUEST['tipoPesquisa'])) ? $_REQUEST['tipoPesquisa'] : null;
	$textoPesquisa = (isset($_REQUEST['textoPesquisa'])) ? $_REQUEST['textoPesquisa'] : null;
	// CRUD
	$tipoAcao = (isset($_REQUEST['tipoAcao'])) ? $_REQUEST['tipoAcao'] : 'S';

	$idEstoqueNota = (isset($_REQUEST['idEstoqueNota'])) ? $_REQUEST['idEstoqueNota'] : null;  	
	$idParceiro = (isset($_REQUEST['idParceiro'])) ? $_REQUEST['idParceiro'] : null;  	
	$dataInicio = (isset($_REQUEST['dataInicio'])) ? $_REQUEST['dataInicio'] : date('Y-m-d');  	
	$dataFinal = (isset($_REQUEST['dataFinal'])) ? $_REQUEST['dataFinal'] : date('Y-m-d');  	
	$textoDescricao = (isset($_REQUEST['textoDescricao'])) ? $_REQUEST['textoDescricao'] : null;   	
	$tipoNota = (isset($_REQUEST['tipoNota'])) ? $_REQUEST['tipoNota'] : null;   
	$idOperador = $idOperadorCorrente;
	
	
	// Retorno de Resposta
	$tipoResposta = (isset($_REQUEST['tipoResposta'])) ? $_REQUEST['tipoResposta'] : null;
	$textoRetorno = (isset($_REQUEST['textoRetorno'])) ? $_REQUEST['textoRetorno'] : null;
	$ativoRetorno = (isset($_REQUEST['ativoRetorno'])) ? $_REQUEST['ativoRetorno'] : null;	


    if (isset($_REQUEST['tipoAcao']) && ($tipoAcao != 'S')){
		
		$dadosResposta = estoqueNota($tipoAcao, 
									$idEstoqueNota, 									
									$idOperador, 
									$tipoNota, 
									$textoDescricao, 
									convertDataParaBanco($dataInicio),
									convertDataParaBanco($dataFinal), 
									$idParceiro, 
									$idParceiroCorrente);
								
		if ($dadosResposta and ($dadosResposta[0]['ativoRetorno'])){
			//AUDITORIA
			include "nota.auditoria.php";
		}
		//
		include "nota.processo.resposta.php";
    }
	if (isset($_REQUEST['tipoResposta'])){
		showMessage(($ativoRetorno == 1) ? "S" : "E", $_CONFIGURACAO_TITULO." - Informativo", $textoRetorno);
	}	

	// Montando tipo de Pesquisa
	$paramPesquisa = array();
	$paramPesquisa[] = array('idEstoqueNota', 'Código');
	
	// 
	if (($tipoAcao == 'S')) {
		// Dados da grid principal
		$dadosEstoqueNota = estoqueNota( $tipoAcao, 
									(($textoPesquisa != '') && ($tipoPesquisa == 'idEstoqueNota')) ? $textoPesquisa : null, 
									$idOperador, 
									$tipoNota,
									$textoDescricao,
									convertDataParaBanco($dataInicio),
									convertDataParaBanco($dataFinal),
									$idParceiro, 
									$idParceiroCorrente);
	}
		
	$numeroLinhas = 0;
?>


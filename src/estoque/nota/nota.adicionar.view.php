<!-- Esse trecho é importante para colocar os IDs e Nomes para os Inputs -->
<div class="modal fade " data-backdrop="static" id="myModalAdicionar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-md">
        <div class="modal-content" >
            <form role="form" name="modalForm" id="modalFormAdicionar" action="<?=$textoDirecionar?>" method="post" autocomplete="off">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×   </span><span class="sr-only">Fechar</span>

                    </button>
                    <h4 class="modal-title" id="myModalLabel">Novo Registro</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">		
                        <input type="hidden" name="tipoAcao"  value="I" />
                        <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
	                    <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
                        <input type="hidden" name="dataInicio"  value="<?=$dataInicio?>" />
                        <input type="hidden" name="dataFinal"  value="<?=$dataFinal?>" />
                        <input type="hidden" name="idParceiro"  value="<?=$idParceiroCorrente?>" />
                        
                        <fieldset>
                            <legend>Dados da Nota</legend>
                            <div class="row">
                                <div class="col-sm-12">
                                        <label for="tipoNota">Tipo da Nota</label>
                                        <div class="form-group">
                                            <select class="form-control" id="tipoNota" name="tipoNota" required>							                                                
                                                <option value="1" selected>Entrada</option>         
                                                <option value="2" >Perda</option>                                         
                                                <option value="3" >Inventário ( Esse tipo de nota não permite reversão após o fechamento )</option>                                                
                                                
                                            </select>
                                        </div>                      
                                </div>                                
                                <div class="col-sm-12">
                                    <label for="textoDescricao">Descrição da Nota | Máximo de 500 caracteres </label>
                                    <textarea class="form-control"  name="textoDescricao"  rows="10" maxlength="500"></textarea>                              
                                </div>                                   
                                
                            </div>
                                 
                        </fieldset>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="button" onclick="return enviaFormularioSimples('modalFormAdicionar')" class="btn btn-success" >Salvar Mudanças</button>
                </div>
            </form>
        </div>
    </div>
</div>
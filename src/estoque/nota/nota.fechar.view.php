
<!-- Esse trecho é importante para colocar os IDs e Nomes para os Inputs -->
<div class="hide">
    <?php foreach($dadosEstoqueNota as $item) {?>	
        <form role="form" id="modalFechar<?=$item['idEstoqueNota']?>" action="<?=$textoDirecionar?>" method="post">
          <input type="hidden" name="tipoAcao"  value="F" />
          <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
          <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
          <input type="hidden" name="dataInicio"  value="<?=$dataInicio?>" />
          <input type="hidden" name="dataFinal"  value="<?=$dataFinal?>" />
          <input type="hidden" name="idEstoqueNota" id="idEstoqueNota" value="<?=$item['idEstoqueNota']?>" />    
          <input type="hidden" name="tipoNota" id="tipoNota" value="<?=$item['tipoNota']?>" />             
        </form>
    <?php  }?>
</div>

<script type="text/JavaScript">
    function enviaformFechar(texto, codigo){
          var excluir = confirm(texto);
            
          if (excluir) {
            enviaFormularioSimples('modalFechar' + codigo);           
          } else {
            return false;
          }
        }
</script>
<!-- Esse trecho é importante para colocar os IDs e Nomes para os Inputs -->
<?php foreach($dadosEstoqueNota as $item) {?>
	<div class="modal fade" data-backdrop="static" id="ModeloEditar<?=$item['idEstoqueNota']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<form role="form" name="modalForm" id="modalFormEditar<?=$item['idEstoqueNota']?>" action="<?=$textoDirecionar?>" method="post">
					<div class="modal-header bg-primary">
						<button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×   </span><span class="sr-only">Fechar</span>

						</button>
						<h4 class="modal-title" id="myModalLabel">Editando Registro [<?=$item['idEstoqueNota']?>]</h4>
					</div>
					<div class="modal-body">
                        <div class="form-group">		
                            <input type="hidden" name="tipoAcao"  value="E" />
                            <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
                            <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
                            <input type="hidden" name="dataInicio"  value="<?=$dataInicio?>" />
                            <input type="hidden" name="dataFinal"  value="<?=$dataFinal?>" />
                            <input type="hidden" name="idEstoqueNota"  value="<?=$item['idEstoqueNota']?>" />
                        
                            <fieldset>
                                <legend>Dados da Nota</legend>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label for="Código">Código</label>
                                        <input class="form-control" Disabled  value="<?=$item['idEstoqueNota']?>" />                               
                                    </div>
                                    <div class="col-sm-10">
                                        <label for="tipoNota">Tipo da Nota</label>
                                        <div class="form-group">
                                            <select class="form-control" id="tipoNota" name="tipoNota" required>							                                                
                                                <option value="1" <?=(('1' == $item['tipoNota']) ? 'selected':'')?>>Entrada</option>         
                                                <option value="2" <?=(('2' == $item['tipoNota']) ? 'selected':'')?>>Perda</option>                                         
                                                <option value="3" <?=(('3' == $item['tipoNota']) ? 'selected':'')?>>Inventário ( Esse tipo de nota não permite reversão após o fechamento )</option>                                                
                                                
                                            </select>
                                        </div>                                 
                                    </div>
                                    <div class="col-sm-12">
                                        <label for="dataNota">Data da Nota</label>
                                        <input class="form-control" Disabled  value="<?=date_dd_mm_yyyy_hh_mm_ss($item['dataNota'])?>" />                               
                                    </div>
                                </div>
                                <div class="row">                                    
                                    <div class="col-sm-12">
                                        <label for="textoDescricao">Descrição da Nota | Máximo de 500 caracteres </label>
                                        <textarea class="form-control"  name="textoDescricao"  rows="10" maxlength="500"><?=$item['textoDescricao']?></textarea>                              
                                    </div>                            	                                
                                </div>                                
                            </fieldset>
                        </div>
                    </div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
						<button type="button" class="btn btn-success"  onclick="return enviaFormularioSimples('modalFormEditar<?=$item['idEstoqueNota']?>')"  >Salvar Mudanças</button>
					</div>
				</form>
			</div>
		</div>
	</div>

<?php  }?>

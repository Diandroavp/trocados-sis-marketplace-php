<div class="container " style="overflow-y: auto; min-height:450px;">
    <div class="page-header">
        <h4>Estoque/Notas/Produtos</h4>      
    </div>
	<div class="well">
        <form role="form" name="modalForm" action="<?=$textoDirecionar?>" method="post" autocomplete="off">
        <input type="hidden" name="tipoAcao"  value="S" />
            <div class="row">
            <div class="col-sm-2">
                    <label for="Nome">Código da Nota</label>
					<div class="form-group">
                        <span><?=$idEstoqueNota?></span>
					</div>
                </div>				
                <div class="col-sm-4">
                    <label for="Nome">Data da Nota</label>
					<div class="form-group">
                        <span><?=date_dd_mm_yyyy_hh_mm_ss($dataNota)?></span>
					</div>
                </div>						
                <div class="col-sm-6">
                    <label for="Nome">Tipo da Nota</label>
					<div class="form-group">
                        <span><?=$nomeTipoNota?></span>
					</div>
                </div>																
            </div>
        </form>		
    </div>
	
    <div class="panel panel-default">
        <div class="panel-heading ">
			<strong>Listagem dos Produtos da Nota</strong>
        </div>
		<?php if($dadosEstoqueNotaProduto) {?>
			<input class="form-control" id="buscarModelo" type="text" placeholder="Buscar..">
		<?php }?>	
        <div class="table-responsive" style="overflow-y: auto; max-height:350px;">
            <table class="table table-hover table-striped sortable"  >
                <thead>
                    <tr>
                        <th><strong>Código Produto</strong></th>
                        <th><strong>Nome</strong></th>                        
                        <th><strong>Valor</strong></th>                        
                        <th><strong>Desconto</strong></th>                        
                        <th><strong>Quantidade</strong></th>                        
                        <th><strong></strong></th>						
                    </tr>
                </thead>                
                <tbody id="tableModelo">
                    <?php foreach($dadosEstoqueNotaProduto as $item) {
                        $textoAviso = '';
                        $tipoAlerta = '';?>
                    <tr class=" <?=$tipoAlerta?>" title="<?=$textoAviso?>">
                        <td><?=$item['idProduto']?></td>
                        <td><?=$item['nomeProduto']?></td>
                        <td><?=formatar_moeda($item['valorProduto'],2)?></td>
                        <td><?=formatar_percentual($item['percentualDesconto'],2)?></td>
                        <td><?=$item['quantidadeProduto']?></td>
                         <td>
                            <button class="btn btn-primary" data-toggle="modal" data-target="#ModeloEditar<?=$item['idProduto']?>" title="Editar Registro" >Editar</button>
                            <button type="button" class="btn btn-danger" onclick="enviaFormExcluir('Deseja excluir o Produto <?=$item['nomeProduto']?>?', '<?=$item['idProduto']?>')"  title="Excluir Registro">Excluir</button>
                        </td>
                    </tr>
                    <?php $numeroLinhas++; }?>
                </tbody>
            </table>
        </div>
        <br>
        <div class="panel-footer">
            <?=$numeroLinhas." Registros encontrados..."?>
			<label for="Nome"> &nbsp; </label>
			<div class="form-group text-right">
            <button type="button" class="btn btn-default" onclick="return enviaFormularioSimples('formVoltar')"  title="Voltar para Produtos">      Voltar      </button>
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalAdicionar">+ Novo Registro</button>						
			</div>	
        </div>
    </div>    
</div>
<?php
	include "notaProduto.editar.view.php";
    include "notaProduto.adicionar.view.php";
    include "notaProduto.excluir.view.php";
?>

  


<form role="form" name="modalForm" id="formVoltar" action="?_p=nota" method="post">					                           		
    <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
    <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
    <input type="hidden" name="dataInicio"  value="<?=$dataInicio?>" />
    <input type="hidden" name="dataFinal"  value="<?=$dataFinal?>" />
</form>

<?php	
    $textoDirecionar = "?_p=".$_REQUEST["_p"];	
	$idOperadorCorrente = $_SESSION["_SESSION_idOperador"];

	// Tipo de Pesquisa Da Tela Anterior
	$tipoPesquisa = (isset($_REQUEST['tipoPesquisa'])) ? $_REQUEST['tipoPesquisa'] : null;
	$textoPesquisa = (isset($_REQUEST['textoPesquisa'])) ? $_REQUEST['textoPesquisa'] : null;
	$idEstoqueNota = (isset($_REQUEST['idEstoqueNota'])) ? $_REQUEST['idEstoqueNota'] : null; 
	$dataNota = (isset($_REQUEST['dataNota'])) ? $_REQUEST['dataNota'] : null; 
	$nomeTipoNota = (isset($_REQUEST['nomeTipoNota'])) ? $_REQUEST['nomeTipoNota'] : null; 
	$tipoNota = (isset($_REQUEST['tipoNota'])) ? $_REQUEST['tipoNota'] : null; 
	$dataInicio = (isset($_REQUEST['dataInicio'])) ? $_REQUEST['dataInicio'] : date('Y-m-d');  	
	$dataFinal = (isset($_REQUEST['dataFinal'])) ? $_REQUEST['dataFinal'] : date('Y-m-d');  

	// CRUD
	$tipoAcao = (isset($_REQUEST['tipoAcao'])) ? $_REQUEST['tipoAcao'] : 'S';
	$idEstoqueNota = (isset($_REQUEST['idEstoqueNota'])) ? $_REQUEST['idEstoqueNota'] : null;  	
	$idProduto = (isset($_REQUEST['idProduto'])) ? $_REQUEST['idProduto'] : null;   	
	$valorProduto = (isset($_REQUEST['valorProduto'])) ? $_REQUEST['valorProduto'] : null;   
	$percentualDesconto = (isset($_REQUEST['percentualDesconto'])) ? $_REQUEST['percentualDesconto'] : null;   
	$quantidadeProduto = (isset($_REQUEST['quantidadeProduto'])) ? $_REQUEST['quantidadeProduto'] : null;
	
	// Retorno de Resposta
	$tipoResposta = (isset($_REQUEST['tipoResposta'])) ? $_REQUEST['tipoResposta'] : null;
	$textoRetorno = (isset($_REQUEST['textoRetorno'])) ? $_REQUEST['textoRetorno'] : null;
	$ativoRetorno = (isset($_REQUEST['ativoRetorno'])) ? $_REQUEST['ativoRetorno'] : null;	
	 

    if (isset($_REQUEST['tipoAcao']) && ($tipoAcao != 'S')){
		
		$dadosResposta = estoqueNotaProduto($tipoAcao, 
											$idEstoqueNota, 
											$idProduto, 
											$valorProduto, 
											$percentualDesconto, 
											$quantidadeProduto);
		
		if ($dadosResposta and ($dadosResposta[0]['ativoRetorno'])){
			//AUDITORIA
			include "notaProduto.auditoria.php";
		}
		//
		include "notaProduto.processo.resposta.php";
    }
	if (isset($_REQUEST['tipoResposta'])){
		showMessage(($ativoRetorno == 1) ? "S" : "E", $_CONFIGURACAO_TITULO." - Informativo", $textoRetorno);
	}	


	// 
	$textoAviso = '';

	if (($tipoAcao == 'S')) {
		// Dados da grid principal
		$dadosEstoqueNotaProduto = estoqueNotaProduto(	$tipoAcao, 
														$idEstoqueNota, 
														$idProduto, 
														$valorProduto, 
														$percentualDesconto, 
														$quantidadeProduto);
		
		// SE O TIPO DA NOTA FOR PERDA, ABRE OS PRODUTOS DISPONÍVEIS
		if ($tipoNota == 2) {
			$dadosProduto = estoqueDisponivelConsultar($tipoAcao, null,	null,null,null,null, $idParceiroCorrente);	
		} else {
			$dadosProduto = produto( $tipoAcao, $idParceiroCorrente, null,null,1,null,null,null,null,null,null,null, null,null,null,null,null,null,null, null, null	, null);		
		}															
	}
	
    $numeroLinhas = 0;
?>


<!-- Esse trecho é importante para colocar os IDs e Nomes para os Inputs -->
<div class="modal fade " data-backdrop="static" id="myModalAdicionar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-md">
        <div class="modal-content" >
            <form role="form" name="modalForm" id="modalFormAdicionar" action="<?=$textoDirecionar?>" method="post" autocomplete="off">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×   </span><span class="sr-only">Fechar</span>

                    </button>
                    <h4 class="modal-title" id="myModalLabel">Novo Registro</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">		
                        <input type="hidden" name="tipoAcao"  value="I" />
                        <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
                        <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
                        <input type="hidden" name="idEstoqueNota"  value="<?=$idEstoqueNota?>" />
                        <input type="hidden" name="dataNota"  value="<?=$dataNota?>" />
                        <input type="hidden" name="nomeTipoNota"  value="<?=$nomeTipoNota?>" />
                        <input type="hidden" name="tipoNota"  value="<?=$tipoNota?>" />
                        <input type="hidden" name="dataInicio"  value="<?=$dataInicio?>" />
                        <input type="hidden" name="dataFinal"  value="<?=$dataFinal?>" />
                        
                        <fieldset>
                                <legend>Dados do Produto</legend>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label for="Código">Código da Nota</label>
                                        <input class="form-control" Disabled  value="<?=$idEstoqueNota?>" />                               
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="Nome">Data da Nota</label>
                                        <input class="form-control" Disabled  value="<?=date_dd_mm_yyyy_hh_mm_ss($dataNota)?>" />                                                                  
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="numeroParcela">Tipo da Nota</label>
                                        <input class="form-control" Disabled value="<?=$nomeTipoNota?>" required/>
                                    </div>                            	                                
                                </div>        
                                <div class="row">      
                                    <div class="col-sm-12">
                                        <label for="idProduto">Produto</label>
                                        <div class="form-group">
                                            <select class="form-control "  name="idProduto" required>                     
                                                <?php foreach($dadosProduto as $itemP) {?>
                                                <option value="<?=$itemP['idProduto']?>" ><?=$itemP['nomeProduto']?> - <?=$itemP['quantidadeApresentacao']?> <?=$itemP['siglaMedida']?></option>
                                                <?php }?>  
                                            </select>
                                        </div>                                                                  
                                    </div>     
                                    <div class="col-sm-4">
                                        <label for="valorProduto">Valor</label>
                                        <input class="form-control"  type="number" placeholder = "1.00" step = "0.01" min = "0" name="valorProduto" id="valorProduto" value="0" required/>                               
                                    </div>   
                                    <div class="col-sm-4">
                                        <label for="percentualDesconto">Desconto %</label>
                                        <input class="form-control"  type="number" placeholder = "1.00" step = "0.01" min = "0" name="percentualDesconto" id="percentualDesconto" value="0" required/>                               
                                    </div>   
                                    <div class="col-sm-4">
                                        <label for="quantidadeProduto">Quantidade</label>
                                        <input class="form-control"  type="number" name="quantidadeProduto" id="quantidadeProduto" value="1"  required/>                               
                                    </div>   
                                </div>                                    
                            </fieldset>       
                   
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="button" onclick="return enviaFormularioSimples('modalFormAdicionar')" class="btn btn-success" >Salvar Mudanças</button>
                </div>
            </form>
        </div>
    </div>
</div>
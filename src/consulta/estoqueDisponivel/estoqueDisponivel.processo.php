<?php	
    $textoDirecionar = "?_p=".$_REQUEST["_p"];	
	$idOperadorCorrente = $_SESSION["_SESSION_idOperador"];
	// Tipo de Pesquisa
	$tipoPesquisa = (isset($_REQUEST['tipoPesquisa'])) ? $_REQUEST['tipoPesquisa'] : null;
	$textoPesquisa = (isset($_REQUEST['textoPesquisa'])) ? $_REQUEST['textoPesquisa'] : null;
	// CRUD
	$tipoAcao = (isset($_REQUEST['tipoAcao'])) ? $_REQUEST['tipoAcao'] : 'S';

	$quantidadeDisponivel = null;
	
	// Montando tipo de Pesquisa
	$paramPesquisa = array();
	$paramPesquisa[] = array('idProduto', 'Código');
	$paramPesquisa[] = array('nomeProduto', 'Nome');
	$paramPesquisa[] = array('percentualDesconto', 'Desconto');	
	$paramPesquisa[] = array('valorProduto', 'Valor');
	if (!$tipoPesquisa){
		$tipoPesquisa = $paramPesquisa[1][0];
	}	

	// 
	if (($tipoAcao == 'S')) {
		// Dados da grid principal
		$dadosEstoqueDisponivel = estoqueDisponivelConsultar($tipoAcao, 
															(($textoPesquisa != '') && ($tipoPesquisa == 'idProduto')) ? $textoPesquisa : null,
															(($textoPesquisa != '') && ($tipoPesquisa == 'nomeProduto')) ? $textoPesquisa : null,
															(($textoPesquisa != '') && ($tipoPesquisa == 'percentualDesconto')) ? $textoPesquisa : null,
															$quantidadeDisponivel,
															(($textoPesquisa != '') && ($tipoPesquisa == 'valorProduto')) ? $textoPesquisa : null,
															$idParceiroCorrente
														
														);
	}
	
    $numeroLinhas = 0;
?>


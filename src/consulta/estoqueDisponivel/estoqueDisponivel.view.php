<div class="container " style="overflow-y: auto; min-height:450px; ">
    <div class="page-header">
        <h4>Consulta/Estoque Disponível</h4>      
    </div>
	<div class="well">
        <form role="form" name="modalForm" id="formPesquisa" action="<?=$textoDirecionar?>" method="post" autocomplete="off">
        <input type="hidden" name="tipoAcao"  value="S" />
        <input type="hidden" name="idParceiroCorrente"  value="<?=$idParceiroCorrente?>" />  
            <div class="row">				
                <div class="col-sm-2">
                    <label for="Nome">Tipo de Pesquisa</label>
					<div class="form-group">
					  <select class="form-control" id="tipoPesquisa" name="tipoPesquisa" required>							
						<?php foreach($paramPesquisa as $item) {?>
						  <option value="<?=$item[0]?>" <?=(($item[0] == $tipoPesquisa) ? 'selected':'')?>><?=$item[1]?></option>
						<?php }?>  
					  </select>
					</div>
                </div>					
				<div class="col-sm-8">
                    <label for="Nome">Texto para pesquisa</label>
					<div class="form-group">
						<input class="form-control somente-numero" name="textoPesquisa" id="textoPesquisa" value="<?=$textoPesquisa?>" />  
					</div>
                </div>				
				<div class="col-sm-2">
					<label for="Nome"> &nbsp; </label>
					<div class="form-group text-right">
						<button type="button" onclick="return enviaFormularioSimples('formPesquisa')" class="btn btn-primary" > Localizar </button>
					</div>						
				</div>  							
            </div>
        </form>		
    </div>
	
    <div class="panel panel-default">
        <div class="panel-heading ">
			<strong>Listagem dos Produtos</strong>
        </div>
		<?php if($dadosEstoqueDisponivel) {?>
			<input class="form-control" id="buscarModelo" type="text" placeholder="Buscar..">
		<?php }?>	
        <div class="table-responsive" style="overflow-y: auto; max-height:350px;">
            <table class="table table-hover table-striped sortable"  >
                <thead>
                    <tr>
                        <th><strong><center>Alertas</center></strong></th>
                        <th><strong>Código</strong></th>
                        <th><strong>Produto</strong></th>
                        <th><strong>Disponível</strong></th>
                        <th><strong>Mínimo</strong></th>                        
                        <th><strong>Valor Bruto</strong></th>
                        <th><strong>Desconto</strong></th>     
                        <th><strong>Valor Líquido</strong></th>    
                        				
                    </tr>
                </thead>                
                <tbody id="tableModelo">
                    <?php foreach($dadosEstoqueDisponivel as $item) {?>
                    <tr class="
                                <?php if ($item['quantidadeDisponivel'] == 0) {?>
                                    alert alert-danger
                                <?php } elseif ($item['ativoMinimo']) {?>
                                    alert alert-warning
                                <?php } ?> 
                        ">
                        <td>
                            <center>
                                <?php if ($item['quantidadeDisponivel'] == 0) {?>
                                    <i class="fas fa-battery-empty" style="font-size:12px; color:red !important" title="Produto não existem no estoque!"></i>
                                <?php } elseif ($item['ativoMinimo']) {?>
                                    <i class="fas fa-battery-quarter" style="font-size:12px; color:orange !important" title="A quantidade Mínima desse produto foi atingida!"></i>
                                <?php } ?>                                

                            </center>
                        </td>
                        <td><?=$item['idProduto']?></td>
                        <td><?=$item['nomeProduto']?></td>
                        <td><?=$item['quantidadeDisponivel']?></td>
                        <td><?=$item['quantidadeMinima']?></td>
                        <td><?=formatar_moeda($item['valorProduto'],2)?></td>
                        <td><?=formatar_percentual($item['percentualDesconto'],2)?></td>
                        <td><?=formatar_moeda($item['valorProdutoLiquido'],2)?></td>                        
                    </tr>
                    <?php $numeroLinhas++; }?>
                </tbody>
            </table>
        </div>
        <br>
        <div class="panel-footer">
            <?=$numeroLinhas." Registros encontrados..."?>
			<label for="Nome"> &nbsp; </label>
			<div class="form-group text-right">
								
			</div>	
        </div>
    </div>    
</div>


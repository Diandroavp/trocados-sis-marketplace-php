<div class="container " >
    <div class="page-header">
        <h4>Operador Corrente</h4>      
    </div>
	<div class="well">
        <?php foreach($dadosOperador as $item) {?>
            <div class="row">				
                <div class="col-sm-4">
                    <label for="Nome">Operador</label>
					<div class="form-group">
                        <?=$item['nomeOperador']?>
					</div>
                </div>					
				<div class="col-sm-8">
                    <label for="Nome">e-Mail</label>
					<div class="form-group">
                        <?=$item['eMail']?>
					</div>
                </div>															
            </div>
        <?php }?>
    </div>
    
    <?php
        $_p = (isset($_REQUEST['_p'])) ? $_REQUEST['_p'] : null;
        $tipoAcesso = (isset($_SESSION["_SESSION_tipoAcesso"])) ? $_SESSION["_SESSION_tipoAcesso"] : null;
        switch($tipoAcesso) {
            case "b": 
                include "tile/tile.basico.view.php";
            break;

            case "a": 
                include "tile/tile.administrador.view.php";
            break; 
            
            case "s": 
                include "tile/tile.supervisor.view.php";
            break;             
        }

    ?> 

</div>
	


<div class="container">    
    <div class="well">
        <form role="form" action="<?=$textoDirecionar?>" method="post" autocomplete="off">
            <div class="row">				
                <div class="col-sm-10">
                    <label for="Nome">Parceiro(s)</label>
                    <div class="form-group">
                        <select class="form-control" id="idParceiroCorrente" name="idParceiroCorrente" >							                    
                        <?php foreach($dadosOperadorParceiros as $item) {?>
                            <option value="<?=$item['idParceiro']?>" <?=(($item['idParceiro'] == $idParceiroCorrente) ? 'selected':'')?>><?=$item['nomeParceiro']?></option>
                        <?php }?>  
                        </select>
                    </div>
                </div>		            
                <div class="col-sm-2">
                    <label for="Nome"> &nbsp; </label>
                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-primary"  > Ativar </button>
                    </div>						
                </div>  							
            </div>
        </form>		
    </div>    
</div>	

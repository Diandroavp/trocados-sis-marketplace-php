<div class="container padding">
    <div class="row">
        <div class="col-sm-3 col-md-4 col-md-offset-4">
        </div>
        <div class="col-sm-6 col-md-4 col-md-offset-4">			         
            <div class="account-wall">
				<div class="panel-heading ">
                    <span class="text-center login-title"><h3><strong><?=$_CONFIGURACAO_NOME_PRINCIPAL?></strong></h3></span>
                    <center><img src="imagens/logo-trocados-green.svg" alt="" style="width: 100%;  max-width: 100px" ></center>
					
				</div>  
          
                <form class="form-signin" action="?" name="form_login" method="post">
                    <input type="text" class="form-control" placeholder="Operador/e-Mail" name="nomeOperador" required autofocus>
                    <input type="password" class="form-control" placeholder="Senha" name="nomeSenha" required>
                    <button class="btn btn-lg btn-primary btn-block" type="submit">
                        Acessar
                    </button>
                    <!--<label class="checkbox pull-left">
                        <input type="checkbox" value="remember-me">
                        Lembre-me
                    </label>                -->
                </form>
            </div>
        </div>
        <div class="col-sm-3 col-md-4 col-md-offset-4">
        </div>		
    </div>	
</div>

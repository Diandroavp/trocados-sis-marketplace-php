<?php foreach ($dadosPerfilOperador as $item) {?>
    <ul class="nav navbar-nav navbar-right ">        
        
        <?php if ($item['ativoDashboard']) {?>
            <!--    DASHBOARD   -->
            <li class="nav-item <?=(($_p == 'dash')) ? 'active' : '' ?>">
                <a class="nav-link" href="#" onclick="return enviaFormMenu('formMenu', 'dash')">
                    Dashboard <span class="sr-only">(current)</span>
                </a>                
            </li>   
        <?php }?>
        
        <?php if ($item['ativoCadastro']) {?>    
            <!--    CADASTRO   -->
            <li class="dropdown <?=(($_p == 'usu') || ($_p == 'doa') ||  ($_p == 'prca') || ($_p == 'prod') ) ? 'active' : '' ?>">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" >Cadastro <span class="caret"></span></a>
                <ul class="dropdown-menu ">
                    <?php if ($item['ativoCadastroGeral']) {?>    
                        <!--    GERAL   -->
                        <li class="navbar-text">
                            <strong>Geral</strong>
                        </li>
                        
                        <!--<li class="<?=(($_p == 'doa') ) ? 'active' : '' ?>">
                            <a title="Compras Externas" href="?_p=doa" active >
                                Compras Externas
                            </a>
                        </li> -->                        
                        <?php if ($item['ativoOperadores']) {?>   
                            <li class="<?=(($_p == 'usu') ) ? 'active' : '' ?>">
                                <a title="Cadastro dos Operadores" href="#" onclick="return enviaFormMenu('formMenu', 'usu')"  >
                                    Operadores
                                </a>
                            </li>
                        <?php }?>                        
                        <?php if ($item['ativoProdutos']) {?>   
                            <li class="<?=(($_p == 'prod')) ? 'active' : '' ?>">
                                <a title="Cadastro dos Produtos"  href="#" onclick="return enviaFormMenu('formMenu', 'prod')">
                                    Produtos
                                </a>
                            </li> 
                        <?php }?>
                    <?php }?>

                    <?php if ($item['ativoCadastroRelacionamento']) {?>   
                        <!--    RELACIONAMENTO   -->
                        <li class="navbar-text">
                            <strong>Relacionamentos</strong>
                        </li>
                        <?php if ($item['ativoProdutoPorCategoria']) {?>   
                            <li class="<?=(($_p == 'prca') ) ? 'active' : '' ?>">
                                <a title="Medidas" href="#" onclick="return enviaFormMenu('formMenu', 'prca')"  >
                                    Produto por Categoria
                                </a>
                            </li> 
                        <?php }?>                        
                    <?php }?>
                </ul>
            </li>
        <?php }?>

        <?php if ($item['ativoMovimento']) {?>   
            <!--    MOVIMENTO   -->
            <li class="dropdown <?=(($_p == 'vali') || ($_p == 'hisv')) ? 'active' : '' ?>">
                <a class="dropdown-toggle " data-toggle="dropdown" href="#">Movimento <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <?php if ($item['ativoValidarCompra']) {?>   
                        <li class="<?=(($_p == 'vali')) ? 'active' : '' ?>">
                            <a title="Validação de doações efetuadas." href="#" onclick="return enviaFormMenu('formMenu', 'vali')">
                                Validar Compra
                            </a>
                        </li> 
                    <?php }?>
                    <?php if ($item['ativoHistoricoValidacao']) {?>   
                        <li class="<?=(($_p == 'hisv')) ? 'active' : '' ?>">
                            <a title="Histórico de Validações" href="#" onclick="return enviaFormMenu('formMenu', 'hisv')">
                                Histórico de Validação
                            </a>
                        </li>
                    <?php }?>
                </ul>
            </li>
        <?php }?>

        <?php if ($item['ativoFinanceiro']) {?> 
            <!--    FINANCEIRO   -->
            <li class="dropdown <?=(($_p == 'comp')) ? 'active' : '' ?>">
                <a class="dropdown-toggle " data-toggle="dropdown" href="#">Financeiro <span class="caret"></span></a>
                <ul class="dropdown-menu">            
                    <?php if ($item['ativoComprasRealizadas']) {?>   
                        <li class="<?=(($_p == 'comp')) ? 'active' : '' ?>">
                            <a title="Compras Realizadas" href="#" onclick="return enviaFormMenu('formMenu', 'comp')">
                                Compras Realizadas
                            </a>
                        </li>
                    <?php }?>
                    <?php if ($item['ativoSaldoUnidades']) {?>   
                        <li class="<?=(($_p == 'unid')) ? 'active' : '' ?>">
                            <a title="Saldo das Unidades" href="#" onclick="return enviaFormMenu('formMenu', 'unid')">
                                Saldo das Unidades
                            </a>
                        </li>
                    <?php }?>
                </ul>
            </li>
        <?php }?>
        
        <?php if ($item['ativoEstoque']) {?> 
            <!--    ESTOQUE   -->
            <li class="dropdown <?=(($_p == 'nota')) ? 'active' : '' ?>">
                <a class="dropdown-toggle " data-toggle="dropdown" href="#">Estoque <span class="caret"></span></a>
                <ul class="dropdown-menu">             
                    <?php if ($item['ativoEstoqueNotas']) {?>   
                        <li class="<?=(($_p == 'nota')) ? 'active' : '' ?>">
                            <a title="Nota de Produtos" href="#" onclick="return enviaFormMenu('formMenu', 'nota')">
                                Notas
                            </a>
                        </li>  
                    <?php }?> 
                </ul>
            </li>
        <?php }?>

        <?php if ($item['ativoConsulta']) {?> 
            <!--    CONSULTA   -->
            <li class="dropdown <?=(($_p == 'edisp')) ? 'active' : '' ?>">
                <a class="dropdown-toggle " data-toggle="dropdown" href="#">Consulta <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <?php if ($item['ativoConsultaEstoqueDisponivel']) {?>   
                        <li class="<?=(($_p == 'edisp')) ? 'active' : '' ?>">
                            <a title="Acompanhamento dos Produtos Disponíveis em Estoque" href="#" onclick="return enviaFormMenu('formMenu', 'edisp')">
                                Estoque Disponível
                            </a>
                        </li>   
                    <?php }?>                                          
                </ul>
            </li>
        <?php }?>

        <?php if ($item['ativoManutencao']) {?>    
            <!--    MANUTENÇÃO   -->
            <li class="dropdown <?=(($_p == 'gpar')|| ($_p == 'par')|| ($_p == 'cate') || ($_p == 'perf') ) ? 'active' : '' ?>">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" >Manutenção <span class="caret"></span></a>
                <ul class="dropdown-menu ">                   
                    <?php if ($item['ativoCategoria']) {?>   
                        <li class="<?=(($_p == 'cate') ) ? 'active' : '' ?>">
                            <a title="Cadastro das Categorias" href="#" onclick="return enviaFormMenu('formMenu', 'cate')"  >
                                Categoria
                            </a>
                        </li> 
                    <?php }?>                    
                    <?php if ($item['ativoGrupoParceiros']) {?>   
                        <li class="<?=(($_p == 'gpar') ) ? 'active' : '' ?>">
                            <a title="Grupo de Parceiros" href="#" onclick="return enviaFormMenu('formMenu', 'gpar')"  >
                                Grupo de Parceiros
                            </a>
                        </li>   
                    <?php }?>                    
                    <?php if ($item['ativoParceiros']) {?>   
                        <li class="<?=(($_p == 'par') ) ? 'active' : '' ?>">
                            <a title="Parceiros" href="#" onclick="return enviaFormMenu('formMenu', 'par')" >
                                Parceiros
                            </a>
                        </li>
                    <?php }?>                      
                    <?php if ($item['ativoPerfil']) {?>   
                        <li class="<?=(($_p == 'perf') ) ? 'active' : '' ?>">
                            <a title="Perfil" href="#" onclick="return enviaFormMenu('formMenu', 'perf')" >
                                Perfil
                            </a>
                        </li>
                    <?php }?>                      
                </ul>
            </li>
        <?php }?>

        <?php if ($item['ativoTabela']) {?>    
            <!--    TABELAS   -->
            <li class="dropdown <?=(($_p == 'med')) ? 'active' : '' ?>">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" >Tabelas <span class="caret"></span></a>
                <ul class="dropdown-menu ">                   
                    <?php if ($item['ativoMedidas']) {?>   
                        <li class="<?=(($_p == 'med') ) ? 'active' : '' ?>">
                            <a title="Medidas" href="#" onclick="return enviaFormMenu('formMenu', 'med')" >
                                Medidas
                            </a>
                        </li>  
                    <?php }?>                        
                </ul>
            </li>
        <?php }?>

        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class='fas fa-user-alt'></i> <?= $_SESSION["_SESSION_nomeOperador"]?><span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="?_p=sai"><i class='fas fa-sign-in-alt'></i></span> Sair</a></li>
            </ul>
        </li>
    </ul>
<?php }?>

<form role="form" id="formMenu" action="?" method="post">	
</form>

<?php
    $_p = (isset($_REQUEST['_p'])) ? $_REQUEST['_p'] : null;
    $idOperador = (isset($_SESSION["_SESSION_idOperador"])) ? $_SESSION["_SESSION_idOperador"] : null;
    
    if ($idOperador) {
        $dadosPerfilOperador =  operadorPerfilConsultar($idOperador);
        if ($dadosPerfilOperador) {
            include "menu/menu.operador.view.php";
        }
    } else {
        include "menu/menu.inicial.view.php";
    }

?> 
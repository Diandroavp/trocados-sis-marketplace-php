<?php	
	$textoDirecionar = "?_p=".$_REQUEST["_p"];	
	$idOperadorCorrente = $_SESSION["_SESSION_idOperador"];
	// Tipo de Pesquisa
	$tipoPesquisa = (isset($_REQUEST['tipoPesquisa'])) ? $_REQUEST['tipoPesquisa'] : null;
	$textoPesquisa = (isset($_REQUEST['textoPesquisa'])) ? $_REQUEST['textoPesquisa'] : null;
	// CRUD
	$tipoAcao = (isset($_REQUEST['tipoAcao'])) ? $_REQUEST['tipoAcao'] : 'S';
	$nomeUsuario = (isset($_REQUEST['nomeUsuario'])) ? $_REQUEST['nomeUsuario'] : null;  
	$numeroCelular = (isset($_REQUEST['numeroCelular'])) ? $_REQUEST['numeroCelular'] : null;  
	$numeroCPF = (isset($_REQUEST['numeroCPF'])) ? $_REQUEST['numeroCPF'] : null;  
	$idCompra = (isset($_REQUEST['idCompra'])) ? $_REQUEST['idCompra'] : null;  	
	$idOperador = (isset($_REQUEST['idOperador']) && ($_REQUEST['idOperador'] <> '')) ? $_REQUEST['idOperador'] : null;  	
	$dataInicio = (isset($_REQUEST['dataInicio'])) ? $_REQUEST['dataInicio'] : date('Y-m-d');  	
	$dataFinal = (isset($_REQUEST['dataFinal'])) ? $_REQUEST['dataFinal'] : date('Y-m-d');  	
	$textoObservacao = (isset($_REQUEST['textoObservacao'])) ? $_REQUEST['textoObservacao'] : null;   	
	$textoEstorno = (isset($_REQUEST['textoEstorno'])) ? $_REQUEST['textoEstorno'] : null;   
	$idOperadorEstorno = $idOperadorCorrente;
	
	$id_usuario_pf = (isset($_REQUEST['id_usuario_pf'])) ? $_REQUEST['id_usuario_pf'] : null;  	

	
	// Retorno de Resposta
	$tipoResposta = (isset($_REQUEST['tipoResposta'])) ? $_REQUEST['tipoResposta'] : null;
	$textoRetorno = (isset($_REQUEST['textoRetorno'])) ? $_REQUEST['textoRetorno'] : null;
	$ativoRetorno = (isset($_REQUEST['ativoRetorno'])) ? $_REQUEST['ativoRetorno'] : null;	


    if (isset($_REQUEST['tipoAcao']) && ($tipoAcao != 'S')){
		if ($tipoAcao == 'EX') {
			$dadosResposta = compra($tipoAcao, $nomeUsuario, $numeroCelular, 
									$numeroCPF, 
									$idCompra, 
									$idOperador, 
									convertDataParaBanco($dataInicio),
									convertDataParaBanco($dataFinal),
									$ativoProducao,
									$idOperadorCorrente,
									$textoEstorno,
									$idParceiroCorrente);

			$textoRetorno = $dadosResposta[0]['ativoRetorno'];
			$ativoRetorno = $dadosResposta[0]['ativoRetorno'];						


			if ($dadosResposta and ($dadosResposta[0]['ativoRetorno'])){
				//AUDITORIA
				include "compra.auditoria.php";
			}
			//
		} else if ($tipoAcao == 'ENV') {
			informarDataEnvio($idCompra, $id_usuario_pf);
			$textoRetorno = 'Ação realizada com sucesso!';
			$ativoRetorno = 1;						
		} else if ($tipoAcao == 'ENT') {
			informarDataEntrega($idCompra, $id_usuario_pf);
			$textoRetorno = 'Ação realizada com sucesso!';
			$ativoRetorno = 1;
		}		
		include "compra.processo.resposta.php";
    }
	if (isset($_REQUEST['tipoResposta'])){
		showMessage(($ativoRetorno == 1) ? "S" : "E", $_CONFIGURACAO_TITULO." - Informativo", $textoRetorno);
	}	

	// Montando tipo de Pesquisa
	$paramPesquisa = array();
	$paramPesquisa[] = array('numeroCelular', 'Celular');
	$paramPesquisa[] = array('nomeUsuario', 'Usuário');
	$paramPesquisa[] = array('numeroCPF', 'CPF');
	if (!$tipoPesquisa){
		$tipoPesquisa = $paramPesquisa[1][0];
	}	

	// 
	if (($tipoAcao == 'S')) {
		// Dados da grid principal
		$dadosCompra = compra( $tipoAcao, 
								(($textoPesquisa != '') && ($tipoPesquisa == 'nomeUsuario')) ? $textoPesquisa : null, 
								(($textoPesquisa != '') && ($tipoPesquisa == 'numeroCelular')) ? $textoPesquisa : null, 
								(($textoPesquisa != '') && ($tipoPesquisa == 'numeroCPF')) ? $textoPesquisa : null,
								$idCompra, 
								$idOperador, 
								convertDataParaBanco($dataInicio),
								convertDataParaBanco($dataFinal),
								$ativoProducao,
								$idOperadorEstorno,
								$textoEstorno,
								$idParceiroCorrente);
									
		$dadosOperador = operador('s', $idParceiroCorrente, null,null, null,null, null,null, null, null);
	}
		
	
    $numeroLinhas = 0;
	$totalValorCompra = 0;
?>


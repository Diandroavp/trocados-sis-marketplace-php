<!-- Esse trecho é importante para colocar os IDs e Nomes para os Inputs -->
<?php foreach($dadosCompra as $item) {
    $dadosCompraProduto = compraProdutoConsultar('s', null, null, $item['idCompra']);
    $totalValorCompraProduto = 0;
    ?>
	<div class="modal fade" data-backdrop="static" id="ModeloCompra<?=$item['idCompra']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
                <form role="form" name="modalForm" id="modalFormCompra<?=$item['idCompra']?>" action="<?=$textoDirecionar?>" method="post">
					<div class="modal-header bg-primary">
						<button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×   </span><span class="sr-only">Fechar</span>

						</button>
						<h4 class="modal-title" id="myModalLabel">Informação da Compra [<?=$item['nome']?> - <?=$item['idCompra']?>]</h4>
					</div>
					<div class="modal-body">
                        <div class="form-group">		
                            <input type="hidden" name="tipoAcao"  value="EX" />
                            <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
                            <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
                            <input type="hidden" name="idCompra"  value="<?=$item['idCompra']?>" />

                            <input type="hidden" name="dataInicio"  value="<?=$dataInicio?>" />
                            <input type="hidden" name="dataFinal"  value="<?=$dataFinal?>" />
                            <input type="hidden" name="idOperador"  value="<?=$idOperador?>" />
                        
                            <fieldset>
                                <legend>Dados da Compra</legend>                                
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label for="textoObservacao">Usuário </label>
                                        <div class="form-group">
                                            <?=$item['nome']?>                                    
                                        </div>
                                    </div> 
                                    <div class="col-sm-4">
                                        <label for="textoObservacao">Usuário </label>
                                        <div class="form-group">
                                            <?=$item['cpf']?>                                    
                                        </div>
                                    </div>  
                                    <div class="col-sm-4">
                                        <label for="textoObservacao">Data da Compra </label>
                                        <div class="form-group">
                                            <?=date_dd_mm_yyyy($item['dataCompra'])?>                                       
                                        </div>                                                                      
                                    </div>                                    
                                </div>  
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label for="textoObservacao">Parceiro </label>
                                        <div class="form-group">
                                            <?=$item['nomeParceiro']?>                                    
                                        </div>
                                    </div>                                                           
                                </div>                                      
                            </fieldset>

                            <fieldset>
                                <legend>Produtos</legend>                                
                                <table class="table table-hover table-striped sortable"  >
                                    <thead>
                                        <tr>
                                            <th><strong>Código</strong></th>
                                            <th><strong>Descrição</strong></th>
                                            <th><strong>Qtd</strong></th>
                                            <th><strong>Bruto</strong></th>
                                            <th><strong>Desconto</strong></th>
                                            <th ><strong>Líquido</strong></th>      
                                            <th class="right"><strong>Total</strong></th>      
                                                                                                     						
                                        </tr>
                                    </thead>                
                                    <tbody >
                                        <?php foreach($dadosCompraProduto as $itemProduto) {?>
                                        <tr>
                                            <td><?=$itemProduto['idProduto']?></td> 
                                            <td><?=$itemProduto['nomeProduto']?></td>   
                                            <td><?=$itemProduto['numeroQuantidade']?></td>                      
                                            <td><?=formatar_moeda($itemProduto['valorBrutoProduto'], 2)    ?></td>     
                                            <td><?=formatar_percentual($itemProduto['percentualDesconto'], 2)    ?></td>                                                                    
                                            <td ><?=formatar_moeda($itemProduto['valorProduto'], 2)    ?></td>                  
                                            <td class="right"><?=formatar_moeda($itemProduto['numeroQuantidade']*$itemProduto['valorProduto'], 2)    ?></td>                  
                                            
                                        </tr>
                                        <?php 
                                            $totalValorCompraProduto += $itemProduto['numeroQuantidade']*$itemProduto['valorProduto'];                                                                    
                                        }?>
                                    </tbody>
                                    <tfoot >
                                        <tr>
                                            <td>Total:</td>                        
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="right"><?=formatar_moeda($totalValorCompraProduto, 2)?></td>                                                                    
                                                              
                                        </tr>
                                    </tfoot>
                                </table>                                    
                            </fieldset>

                            <fieldset>
                                <legend>Total Valores</legend>                                
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label for="textoObservacao">Valor dos Produtos </label>
                                        <div class="form-group">
                                            <?=formatar_moeda($item['valorLiquido'],2)?>                                    
                                        </div>
                                    </div>  
                                    <div class="col-sm-4">
                                        <label for="textoObservacao">Valor de Frete </label>
                                        <div class="form-group">
                                            <?=formatar_moeda($item['valorFrete'],2)?>                                    
                                        </div>
                                                                      
                                    </div>                                    
                                    <div class="col-sm-4">
                                        <label for="textoObservacao">Valor Total</label>
                                        <div class="form-group">
                                            <?=formatar_moeda($item['valorTotalCompra'],2)?>                                    
                                        </div>                                                                      
                                    </div>                                    
                                </div>  
                                
                            </fieldset>
                            <br>

                            <fieldset>
                                <legend>Split de Pagamentos</legend>                                

                                <div class="row">
                                    <div class="col-sm-8">
                                        <label for="textoObservacao">Parceiro da Venda </label>
                                        <div class="form-group">
                                            <?=$item['nomeParceiro']?>                                    
                                        </div>
                                    </div>  
                                    <div class="col-sm-4">
                                        <label for="textoObservacao">Valor </label>
                                        <div class="form-group">
                                            <?=formatar_moeda($item['valorCliente'],2)?>                                    
                                        </div>
                                                                      
                                    </div>                                                                        
                                </div>  

                                <div class="row">
                                    <div class="col-sm-8">
                                        <label for="textoObservacao">Mantenedora Financeira </label>
                                        <div class="form-group">
                                            <?=$item['nomeUnidadeMantenedora']?>                                    
                                        </div>
                                    </div>  
                                    <div class="col-sm-4">
                                        <label for="textoObservacao">Valor </label>
                                        <div class="form-group">
                                            <?=formatar_moeda($item['valorMantenedora'],2)?>                                    
                                        </div>
                                                                      
                                    </div>                                                                        
                                </div>  

                                <div class="row">
                                    <div class="col-sm-8">
                                        <label for="textoObservacao">Responsável Pelo Frete </label>
                                        <div class="form-group">
                                            <?=$item['nomeUnidadeFrete']?>                                    
                                        </div>
                                    </div>  
                                    <div class="col-sm-4">
                                        <label for="textoObservacao">Valor </label>
                                        <div class="form-group">
                                            <?=formatar_moeda($item['valorFrete'],2)?>                                    
                                        </div>
                                                                      
                                    </div>                                                                        
                                </div>  
                                
                            </fieldset>
                            <br>

                            <fieldset>
                                <legend>Dados para Entrega</legend>                                

                                <div class="row">
                                    <div class="col-sm-4">
                                        <label for="textoObservacao">Previsão de Entrega </label>
                                        <div class="form-group">
                                            <?=date_dd_mm_yyyy_hh_mm_ss($item['dataEntrega'])?>
                                        </div>
                                    </div>  
                                    <div class="col-sm-4">
                                        <label for="textoObservacao">Data de Envio Para Entrega </label>
                                        <div class="form-group">
                                            <?=date_dd_mm_yyyy_hh_mm_ss($item['dataEnvio'])?>
                                        </div>
                                    </div>  
                                    <div class="col-sm-4">
                                        <label for="textoObservacao">Entrega</label>
                                        <div class="form-group">
                                            <?=date_dd_mm_yyyy_hh_mm_ss($item['dataEntregaEfetiva'])?>
                                        </div>
                                    </div>  
                                </div>  
                                <div class="row">
                                <div class="col-sm-6">
                                        <label for="nomeEndereco">Endereço </label>
                                        <div class="form-group">
                                            <?=$item['nomeEndereco']?>                                    
                                        </div>
                                    </div> 
                                    <div class="col-sm-2">
                                        <label for="numeroEndereco">Número </label>
                                        <div class="form-group">
                                            <?=$item['numeroEndereco']?>                                    
                                        </div>
                                    </div>  
                                    <div class="col-sm-4">
                                        <label for="nomeBairro">Bairro </label>
                                        <div class="form-group">
                                            <?=$item['nomeBairro']?>                                         
                                        </div>
                                                                      
                                    </div>                                    
                                    <div class="col-sm-4">
                                        <label for="nomeComplemento">Complemento </label>
                                        <div class="form-group">
                                            <?=$item['nomeComplemento']?>                                    
                                        </div>
                                    </div>  
                                    <div class="col-sm-4">
                                        <label for="numeroCep">CEP </label>
                                        <div class="form-group">
                                            <?=$item['numeroCep']?>                                         
                                        </div>
                                                                      
                                    </div>  
                                    <div class="col-sm-2">
                                        <label for="nomeCidade">Cidade </label>
                                        <div class="form-group">
                                            <?=$item['nomeCidade']?>                                    
                                        </div>
                                    </div>  
                                    <div class="col-sm-2">
                                        <label for="nomeUF">UF </label>
                                        <div class="form-group">
                                            <?=$item['nomeUF']?>                                         
                                        </div>
                                                                      
                                    </div>                                    
                                </div>  
                                
                            </fieldset>
                            <br>

                            <fieldset>
                                <legend>Dados da Validação da compra</legend>                                
                                <div class="row">
                                    <div class="col-sm-8">
                                        <label for="textoObservacao">Operador que validou a doação </label>
                                        <div class="form-group">
                                            <?=$item['nomeOperadorValidacao']?>                                    
                                        </div>
                                    </div>  
                                    <div class="col-sm-4">
                                        <label for="textoObservacao">Data da Validação </label>
                                        <div class="form-group">
                                            <?=date_dd_mm_yyyy($item['dataValidacao'])?>                                       
                                        </div>
                                                                      
                                    </div>                                    
                                </div>  
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label for="textoObservacao">Observação </label>
                                        <textarea class="form-control"  name="textoObservacao" id="textoObservacao" rows="5" maxlength="500" disabled><?=$item['textoObservacao']?></textarea>                              
                                    </div>                                    
                                </div>                                      
                            </fieldset>

                            <fieldset>
                                <legend>Dados do Vale Compras</legend>                                
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label for="textoObservacao">Código ID </label>
                                        <div class="form-group">
                                            <?=$item['id']?>                                    
                                        </div>
                                    </div>                                      
                                    <div class="col-sm-5">
                                        <label for="textoObservacao">Data de Uso </label>
                                        <div class="form-group">
                                            <?=date_dd_mm_yyyy_hh_mm_ss($item['data_uso'])?>                                       
                                        </div>                                                                      
                                    </div>    
                                    <div class="col-sm-5">
                                        <label for="textoObservacao">Data de Cancelamento </label>
                                        <div class="form-group">
                                            <?=date_dd_mm_yyyy_hh_mm_ss($item['data_cancelamento'])?>                                       
                                        </div>                                                                      
                                    </div>                                    
                                </div>                                  
                            </fieldset>

                            

                            <fieldset>
                                <legend>Dados do Estorno</legend>                                
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label for="textoEstorno">Justificativa (Obrigatório) | Máximo de 500 caracteres </label>
                                        <textarea class="form-control"  name="textoEstorno" id="textoEstorno<?=$item['idCompra']?>" rows="5" maxlength="500" <?=(($item['dataEstorno']) ? 'disabled':'required')?> ><?=$item['textoEstorno']?></textarea>                              
                                    </div>                                    
                                </div>                                      
                            </fieldset>

                        </div>
                    </div>

					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <?php if ($item['ativoEntrega'] && !$item['dataEnvio']) {?>
                            <button type="button" class="btn btn-primary" autofocus  onclick="return informarEnvioParaEntrega('frmInformarEnvio<?=$item['idCompra']?>', '<?=$item['idCompra']?>')"  >Informar Envio Para Entrega</button>
                        <?}?>
                        <?php if ($item['ativoEntrega'] && !$item['dataEntregaEfetiva']) {?>
                            <button type="button" class="btn btn-primary" autofocus  onclick="return informarEntrega('frmInformarEntrega<?=$item['idCompra']?>', '<?=$item['idCompra']?>')"  >Informar Entrega</button>
                        <?}?>
                        <?php if (!$item['dataEstorno']) {?>
                            <button type="button" class="btn btn-danger" autofocus  onclick="return salvarEnviarForm('modalFormCompra<?=$item['idCompra']?>', '<?=$item['idCompra']?>')"  >Estornar Compra</button>
                        <?}?>
					</div>
                </form>
			</div>
		</div>
	</div>


    
    <form role="form" name="frmInformarEnvio<?=$item['idCompra']?>" id="frmInformarEnvio<?=$item['idCompra']?>" action="<?=$textoDirecionar?>" method="post">
        <input type="hidden" name="_route" id="_route" value="" />
        <input type="hidden" name="idCompra"  value="<?=$item['idCompra']?>" />
        <input type="hidden" name="id_usuario_pf"  value="<?=$item['id_usuario_pf']?>" />
        <input type="hidden" name="tipoAcao"  value="ENV" />
    </form> 

    <form role="form" name="frmInformarEntrega<?=$item['idCompra']?>" id="frmInformarEntrega<?=$item['idCompra']?>" action="<?=$textoDirecionar?>" method="post">
        <input type="hidden" name="_route" id="_route" value="" />
        <input type="hidden" name="idCompra"  value="<?=$item['idCompra']?>" />
        <input type="hidden" name="id_usuario_pf"  value="<?=$item['id_usuario_pf']?>" />
        <input type="hidden" name="tipoAcao"  value="ENT" />
    </form> 

<?php  }?>

<script>
    function salvarEnviarForm(form, idCompra){  
        let textoEstorno = document.getElementById('textoEstorno' + idCompra);
        
        if ( textoEstorno.value.length > 15 ){
            if (confirm("Você confirma o estorno da doação: " + idCompra +"?")){
                return enviaFormularioSimples(form);           
            }            
        } else {
            alert('informe uma justificativa válida! ( mais de 15 caracteres )');
            return false;
        }
        
    }

    function informarEnvioParaEntrega(form, idCompra){                  
        if (confirm("Confirma o envio da Compra : " + idCompra +"? A ação não poderá ser desfeita!")){
                return enviaFormularioSimples(form);           
        }           
    }

    function informarEntrega(form, idCompra){                  
        if (confirm("Confirma a entrega da Compra : " + idCompra +"? A ação não poderá ser desfeita!")){
                return enviaFormularioSimples(form);           
        }           
    }
</script>

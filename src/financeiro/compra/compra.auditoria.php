<?php 
    //AUDITORIA
    $descricaoAuditoria = 'Compra ['.$idOperadorCorrente.';'.$idCompra.']';
    //					
    $tipoAuditoria  = $tipoAcao;	    					
    //
    $textoAuditoria = 'Validação de Compra => ';
    $textoAuditoria = $textoAuditoria.'idOperadorCorrente: '.$idOperadorCorrente;
    $textoAuditoria = $textoAuditoria.';textoDirecionar: '.$textoDirecionar;
    $textoAuditoria = $textoAuditoria.';tipoAcao: '.$tipoAcao;	
    $textoAuditoria = $textoAuditoria.';tipoResposta: '.$tipoResposta;			
    $textoAuditoria = $textoAuditoria.';textoRetorno: '.$textoRetorno;			
    $textoAuditoria = $textoAuditoria.';ativoRetorno: '.$ativoRetorno;	
    		
    $textoAuditoria = $textoAuditoria.';nomeUsuario: '.$nomeUsuario;			
    $textoAuditoria = $textoAuditoria.';numeroCelular: '.$numeroCelular;			
    $textoAuditoria = $textoAuditoria.';idCompra: '.$idCompra;			
    $textoAuditoria = $textoAuditoria.';numeroCPF: '.$numeroCPF;			
    $textoAuditoria = $textoAuditoria.';idOperador: '.$idOperador;			
    $textoAuditoria = $textoAuditoria.';textoObservacao: '.$textoObservacao;	
    $textoAuditoria = $textoAuditoria.';dataInicio: '.$dataInicio;	
    $textoAuditoria = $textoAuditoria.';dataFinal: '.$dataFinal;	
    $textoAuditoria = $textoAuditoria.';ativoProducao: '.$ativoProducao;			
    $textoAuditoria = $textoAuditoria.';textoEstorno: '.$textoEstorno;		
    $textoAuditoria = $textoAuditoria.';idOperadorEstorno: '.$idOperadorEstorno;	
    $textoAuditoria = $textoAuditoria.';idParceiroCorrente: '.$idParceiroCorrente;			

    inserirAuditoria($descricaoAuditoria, $tipoAuditoria, $textoAuditoria);
?>
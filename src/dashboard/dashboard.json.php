<?php 

    header( 'Cache-Control: no-cache' );
	header( 'Content-type: application/xml; charset="utf-8"', true );

    global $ativoProducao;

    include "../../src/geral/login/login.sessions.php";
    include "../../funcoes/funcoes.php";
    include "../../config/config.php";    
    include "../../modell/index.php";    

    $dataAcao = (isset($_REQUEST['dataAcao'])) ? $_REQUEST['dataAcao'] : null;
    $nomeFuncao = (isset($_REQUEST['nomeFuncao'])) ? $_REQUEST['nomeFuncao'] : null;


    if ($nomeFuncao == 'geral_pessoa') {
        /*          PESSOA           */
        $coluna[] = array(
            'label'	=> 'Dias de Evento',
            'type'	=> 'string',                
            );
        $coluna[] = array(            
            'label'	=> 'Número de Pessoas',            
            'type'	=> 'number',                  
            );
        $coluna[] = array(            
            'label'	=> 'Número de Pessoas',            
            'type'	=> 'number',    
            'role'  => 'annotation',              
            );
        


        $retorno['cols'] = $coluna;
                    
        $dados = dashboardPessoa($dataAcao);

        if ($dados){
            foreach($dados as $item){
                $linha[] = array(   'v' => date_dd_mm_yyyy($item['dataAcao']),
                                    'f'	=> null,);   
                
                $linha[] = array(   'v' => $item['totalPessoas'],
                'f'	=> null,);  
                $linha[] = array(   'v' => $item['totalPessoas'],
                'f'	=> null,);                 

                $acumulaLinha[] =  array( 'c' => $linha);
                $linha = null;     
                
                
            }

            $retorno['rows'] = $acumulaLinha;    
        }

    } else if ($nomeFuncao == 'geral_quilo_dia') {
        /*          QUILO           */
        $coluna[] = array(
            'label'	=> 'Dias de Evento',
            'type'	=> 'string',                
            );
        $coluna[] = array(            
            'label'	=> 'Trocados',            
            'type'	=> 'number',                  
            );
        $coluna[] = array(            
            'label'	=> 'Trocados',            
            'type'	=> 'number',    
            'role'  => 'annotation',              
            );
        $coluna[] = array(            
            'label'	=> 'Amazonas Shopping',            
            'type'	=> 'number',                  
            );
        $coluna[] = array(            
            'label'	=> 'Amazonas Shopping',            
            'type'	=> 'number',    
            'role'  => 'annotation',              
            );
        


        $retorno['cols'] = $coluna;
                    
        $dados = dashboardQuantidade($dataAcao);

        if ($dados){
            foreach($dados as $item){
                $linha[] = array(   'v' => date_dd_mm_yyyy($item['dataAcao']),
                                    'f'	=> null,);   
                
                $linha[] = array(   'v' => $item['totalQuilos'],
                'f'	=> null,);  
                $linha[] = array(   'v' => $item['totalQuilos'],
                'f'	=> null,); 
                
                $linha[] = array(   'v' => $item['totalQuilosExternos'],
                'f'	=> null,);  
                $linha[] = array(   'v' => $item['totalQuilosExternos'],
                'f'	=> null,); 

                $acumulaLinha[] =  array( 'c' => $linha);
                $linha = null;     
                
                
            }

            $retorno['rows'] = $acumulaLinha;    
        }

    } elseif ($nomeFuncao == 'geral_quilo_percentual') {
        /*          QUILO PERCENTUAL       */
        $coluna[] = array(
            'label'	=> 'Dias de Evento',
            'type'	=> 'string',                
            );
        $coluna[] = array(            
            'label'	=> 'Trocados',            
            'type'	=> 'number',     
            'pattern' => '0#,## %',               
            );
        $coluna[] = array(            
            'label'	=> 'Trocados',            
            'type'	=> 'number',    
            'role'  => 'annotation',  
            'pattern' => '0#,## %',              
            );
        $coluna[] = array(            
            'label'	=> 'Amazonas Shopping',            
            'type'	=> 'number', 
            'pattern' => '0#,## %',                   
            );
        $coluna[] = array(            
            'label'	=> 'Amazonas Shopping',            
            'type'	=> 'number',    
            'role'  => 'annotation',    
            'pattern' => '0#,## %',      
            );
        


        $retorno['cols'] = $coluna;
                    
        $dados = dashboardQuantidade($dataAcao);

        if ($dados){
            foreach($dados as $item){
                $linha[] = array(   'v' => date_dd_mm_yyyy($item['dataAcao']),
                                    'f'	=> null,);   
                
                $linha[] = array(   'v' => $item['percentualQuilos'],
                'f'	=> null,);  
                $linha[] = array(   'v' => $item['percentualQuilos'],
                'f'	=> null,); 
                
                $linha[] = array(   'v' => $item['percentualQuilosExternos'],
                'f'	=> null,);  
                $linha[] = array(   'v' => $item['percentualQuilosExternos'],
                'f'	=> null,); 

                $acumulaLinha[] =  array( 'c' => $linha);
                $linha = null;     
                
                
            }

            $retorno['rows'] = $acumulaLinha;    
        }

    } else if ($nomeFuncao == 'geral_doacoes') {
        /*          GERAL           */
        $coluna[] = array(
            'label'	=> 'Dias de Evento',
            'type'	=> 'string',                
            );
        $coluna[] = array(            
            'label'	=> 'Quantidade de Compras',                          
            'type'	=> 'number',            
            );
        $coluna[] = array(            
            'label'	=> 'Quantidade de Compras',                          
            'type'	=> 'number',            
            'role'  => 'annotation',
            );
        
        $coluna[] = array(              
            'label'	=> 'Validações Efetuadas',              
            'type'	=> 'number',                
            );
        $coluna[] = array(              
            'label'	=> 'Validações Efetuadas',              
            'type'	=> 'number',    
            'role'  => 'annotation',            
            );
        


        $retorno['cols'] = $coluna;
                    
        $dados = dashboardQuantidade($dataAcao);

        if ($dados){
            foreach($dados as $item){
                $linha[] = array(   'v' => date_dd_mm_yyyy($item['dataAcao']),
                                    'f'	=> null,);   
                
                $linha[] = array(   'v' => $item['totalCompra'],
                'f'	=> null,); 
                $linha[] = array(   'v' => $item['totalCompra'],
                'f'	=> null,); 

                $linha[] = array(   'v' => $item['totalValidacao'],
                'f'	=> null,);  
                $linha[] = array(   'v' => $item['totalValidacao'],
                'f'	=> null,); 
                
                $acumulaLinha[] =  array( 'c' => $linha);
                $linha = null;     
                
                
            }

            $retorno['rows'] = $acumulaLinha;    
        }

    } else if ($nomeFuncao == 'genero') {
        /*          GENERO           */
       $retorno[] = array(
           'Gênero',
           'Quantidade'
           );
       
       
           
       $dados = dashboardGenero($ativoProducao);

       if ($dados){
           foreach($dados as $item){        

           $retorno[] = array( $item['nomeDescricao'], intval($item['valorTotal']));  
           
          


           }            
       }

    } else if ($nomeFuncao == 'turno') {
        /*          TURNO           */
       $retorno[] = array(
           'Turno',
           'Quantidade'
           );
       
       
           
       $dados = dashboardTurno('global');

       if ($dados){
           foreach($dados as $item){        

           $retorno[] = array( $item['nomeTurno'], intval($item['valorTotal']));  
           
           }            
       }

    } else if ($nomeFuncao == 'turno_trocados') {
        /*          TURNO TROCADOS           */
       $retorno[] = array(
           'Turno',
           'Quantidade'
           );
       
       
           
       $dados = dashboardTurno('trocados');

       if ($dados){
           foreach($dados as $item){        

           $retorno[] = array( $item['nomeTurno'], intval($item['valorTotal']));  
           
           }            
       }

    } else if ($nomeFuncao == 'geral_quilo') {
        /*          GERAL DE QUILOS DOADOS           */
        $retorno[] = array(
            'Trocados',
            'Amazonas Shopping'
            );
        
        
            
        $dados = dashboardGeral();

        if ($dados){         
            foreach($dados as $item){
                $retorno[] = array( 'Trocados', intval($item['totalQuilograma']));  
                $retorno[] = array( 'Amazonas Shopping', intval($item['totalQuilogramaExterno']));  
            }          
        }

    } else if ($nomeFuncao == 'geral') {
        /*          GERAL           */
        
        
        $dados = dashboardGeral();

        if ($dados){         
            foreach($dados as $item){
                $retorno[] = array( 
                        'totalQuantidadeDoacao' => intval($item['totalQuantidadeDoacao']),
                        'totalQuilograma' => intval($item['totalQuilograma']),
                        'totalQuilogramaExterno' => intval($item['totalQuilogramaExterno']),
                        'idadeMedia' => formatar_numero($item['idadeMedia'],0),
                        'totalCancelamento' => intval($item['totalCancelamento']),
                        'totalPessoas' => intval($item['totalPessoas']),
                    );  
                
            }          
        }      
    }
    echo (json_encode( $retorno ));

?>

<div class="">
    <div class="panel panel-default">
        <div class="panel-heading ">
			<strong>Parcelas Vencidas até o dia <?=convertDataParaBanco($dataVencimento, 'd/m/Y')?></strong>
        </div>
		<?php if($dadosContratoParcelaVencida) {?>
			<input class="form-control" id="buscarModelo" type="text" placeholder="Buscar..">
		<?php }?>	
        <div class="table-responsive" style="overflow-y: auto; max-height:350px;">
            <table class="table table-hover table-striped sortable"  >
            <thead>
                    <tr>
                        <th><strong>Cliente</strong></th>
                        <th><strong>Contrato</strong></th>
                        <th><strong>Parcela</strong></th>
                        <th><strong>Vencimento</strong></th>
                        <th><strong>Valor (R$)</strong></th>
                        <th><strong>Pagamento (R$)</strong></th> 
                        <th></th>                      
                    </tr>
                </thead>                
                <tbody id="tableModelo">
                    <?php foreach($dadosContratoParcelaVencida as $item) {
                        $textoAviso = '';
                        //
                        if ($item['valorPagamentoTotal'] > 0){
                            $textoAviso = $textoAviso.'- Pagamento Parcial!';	
                        } else {
                            $textoAviso = $textoAviso.'- Sem Pagamento!';
                        }
                        
                        ?>
                    <tr class=" <?=($item['valorPagamentoTotal'] > 0) ? 'warning' : 'danger' ?>" title="<?=$textoAviso?>">
                        <td><?=$item['nomeFantasia']?></td>
                        <td><?=$item['idContrato']?></td>
                        <td><?=$item['numeroParcela']?></td>
                        <td><?=convertDataParaBanco($item['dataVencimento'], 'd/m/Y')?></td>                        
                        <td><?=formatar_moeda($item['valorParcela'],2)?></td>
                        <td><?=formatar_moeda($item['valorPagamentoTotal'],2)?></td>
                        <td>
                            <button type="button" class="btn btn-primary" onclick="return enviaFormularioSimples('formPagamento<?=$item['idContrato'].$item['numeroParcela']?>')"  title="Acessar os Pagamentos">Pagamentos</button>                            
                        </td>
                    </tr>
                    <?php $numeroLinhas++; }?>
                </tbody>
            </table>
        </div>
        <br>
        <div class="panel-footer">
            <?=$numeroLinhas." Registros encontrados..."?>
			<label for="Nome"> &nbsp; </label>
			<div class="form-group">            
			</div>	
        </div>
    </div>    
</div>

  

<?php foreach($dadosContratoParcelaVencida as $item) {?>
    <form role="form" name="modalForm" id="formPagamento<?=$item['idContrato'].$item['numeroParcela']?>" action="?_p=pag" method="post">					                           		
        <input type="hidden" name="idContrato"  value="<?=$item['idContrato']?>" />
        <input type="hidden" name="numeroParcela"  value="<?=$item['numeroParcela']?>" />
    </form>
<?}?>
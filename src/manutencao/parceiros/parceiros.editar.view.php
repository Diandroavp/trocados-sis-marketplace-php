<!-- Esse trecho é importante para colocar os IDs e Nomes para os Inputs -->
<?php foreach($dadosParceiros as $item) {?>
	<div class="modal fade" data-backdrop="static" id="ModeloEditar<?=$item['idParceiro']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<form role="form" name="modalForm" id="modalFormEditar<?=$item['idParceiro']?>" action="<?=$textoDirecionar?>" method="post">
					<div class="modal-header bg-primary">
						<button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×   </span><span class="sr-only">Fechar</span>

						</button>
						<h4 class="modal-title" id="myModalLabel">Editando Registro [<?=$item['idParceiro']?>]</h4>
					</div>
					<div class="modal-body">
                        <div class="form-group">		
                            <input type="hidden" name="tipoAcao"  value="E" />
                            <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
                            <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
                            <input type="hidden" name="idParceiro"  value="<?=$item['idParceiro']?>" />
                        
                            <fieldset>
                                <legend>Dados do Grupo</legend>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label for="Código">Código</label>
                                        <input class="form-control" Disabled  value="<?=$item['idParceiro']?>" />                               
                                    </div>
                                </div>
                                <div class="row">                                    
                                    <div class="col-sm-12">
                                        <label for="nomeParceiro">Nome</label>
                                        <input class="form-control" type="text" name="nomeParceiro" id="nomeParceiro" value="<?=$item['nomeParceiro']?>" required  autocomplete="off" />                               
                                    </div>                                    
                                </div>    
                                <div class="row">                                    
                                    <div class="col-sm-3">
                                        <label for="ordemVisual">Ordem Visual</label>
                                        <input class="form-control" type="text" name="ordemVisual" id="ordemVisual" value="<?=$item['ordemVisual']?>" required  autocomplete="off" />                               
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="nomeImagem">Imagem</label>
                                        <input class="form-control" type="text" name="nomeImagem"  value="<?=$item['nomeImagem']?>" required  autocomplete="off" />                               
                                    </div>              
                                    <div class="col-sm-3">
                                        <label for="tipoTransacao">Tipo de Transação</label>
                                        <div class="form-group">
                                            <select class="form-control"  name="tipoTransacao" >                     
                                                <?php foreach($paramTipoTransacao as $itemP) {?>
                                                <option value="<?=$itemP['tipoTransacao']?>" <?=(($itemP['tipoTransacao'] == $item['tipoTransacao']) ? 'selected':'')?> ><?=$itemP['nomeTipoTransacao']?></option>
                                                <?php }?>  
                                            </select>
                                        </div>                                      
                                    </div>                         
                                </div>                                    

                                <div class="row">
                                    <div class="col-sm-3">                                                                            
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoEntrega" id="ativoEntrega" value="1" <?=($item['ativoEntrega'] ? 'checked' : '' )?>> Entrega</label>                                          
                                        </div>
                                    </div>
                                    <div class="col-sm-3">                                                                            
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoTrocados" id="ativoTrocados" value="1" <?=($item['ativoTrocados'] ? 'checked' : '' )?>> Trocados</label>                                          
                                        </div>
                                    </div>
                                    <div class="col-sm-3">                                                       
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoRetirada" id="ativoRetirada" value="1" <?=($item['ativoRetirada'] ? 'checked' : '' )?>> Retirada</label>                                          
                                        </div>
                                    </div>  
                                    <div class="col-sm-3">                                                                            
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoParceiro" id="ativoParceiro" value="1" <?=($item['ativoParceiro'] ? 'checked' : '' )?>> Ativo</label>                                          
                                        </div>
                                    </div> 
                                    <div class="col-sm-3">                                                                            
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoHome" id="ativoHome" value="1" <?=($item['ativoHome'] ? 'checked' : '' )?>> Ativo Home</label>                                          
                                        </div>
                                    </div>               
                                </div>   
                                
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label for="numeroCelular">Número de Whatsapp para contato ( Somente Número ) Ex.: 92000000000</label>
                                        <input class="form-control somente-numero" type="text" name="numeroCelular" id="numeroCelular" value="<?=$item['numeroCelular']?>" maxlength=11 required  autocomplete="off" />                               
                                    </div> 
                                </div> 

                                <div class="row">
                                    <div class="col-sm-12">
                                        <label for="idParceirosGrupo">Grupo</label>
                                        <div class="form-group">
                                            <select class="form-control" id="idParceirosGrupo" name="idParceirosGrupo" >                     
                                                <?php foreach($dadosParceirosGrupo as $itemP) {?>
                                                <option value="<?=$itemP['idParceirosGrupo']?>" <?=(($itemP['idParceirosGrupo'] == $item['idParceirosGrupo']) ? 'selected':'')?>><?=$itemP['nomeParceirosGrupo']?></option>
                                                <?php }?>  
                                            </select>
                                        </div>
                                                                  
                                    </div>                                    
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <label for="idUnidade">Unidade</label>
                                        <div class="form-group">
                                            <select class="form-control" id="idUnidade" name="idUnidade" > 
                                                <option value="" <?=(('' == $item['idUnidade']) ? 'selected':'')?>></option>                    
                                                <?php foreach($dadosUnidade as $itemP) {?>
                                                <option value="<?=$itemP['id']?>" <?=(($itemP['id'] == $item['idUnidade']) ? 'selected':'')?>><?=$itemP['nome_fantasia']?></option>
                                                <?php }?>  
                                            </select>
                                        </div>                                                                  
                                    </div>                                    
                                </div>
                                                              
                            </fieldset>

                            <fieldset>
                                <legend>Dados da Mantenedora Financeira</legend>
                                
                                <div class="row">                                    
                                    <div class="col-sm-8">
                                        <label for="idUnidadeMantenedora">Unidade Mantenedora</label>
                                        <div class="form-group">
                                            <select class="form-control" id="idUnidadeMantenedora" name="idUnidadeMantenedora" > 
                                                <option value="" <?=(('' == $item['idUnidadeMantenedora']) ? 'selected':'')?>></option>                    
                                                <?php foreach($dadosUnidade as $itemP) {?>
                                                <option value="<?=$itemP['id']?>" <?=(($itemP['id'] == $item['idUnidadeMantenedora']) ? 'selected':'')?>><?=$itemP['nome_fantasia']?></option>
                                                <?php }?>  
                                            </select>
                                        </div>                                                                  
                                    </div>  
                                    <div class="col-sm-4">
                                        <label for="percentualMantenedora">Percentual de Repasse</label>
                                        <input class="form-control somente-numero" type="text" name="percentualMantenedora" id="percentualMantenedora" value="<?=$item['percentualMantenedora']?>" required  autocomplete="off" />                               
                                    </div>                                    
                                </div>                        
                            </fieldset>

                            <fieldset>
                                <legend>Dados Responsável Frete</legend>
                                
                                <div class="row">                                    
                                    <div class="col-sm-12">
                                        <label for="idUnidadeFrete">Unidade Frete</label>
                                        <div class="form-group">
                                            <select class="form-control" id="idUnidadeFrete" name="idUnidadeFrete" > 
                                                <option value="" <?=(('' == $item['idUnidadeFrete']) ? 'selected':'')?>></option>                    
                                                <?php foreach($dadosUnidade as $itemP) {?>
                                                <option value="<?=$itemP['id']?>" <?=(($itemP['id'] == $item['idUnidadeFrete']) ? 'selected':'')?>><?=$itemP['nome_fantasia']?></option>
                                                <?php }?>  
                                            </select>
                                        </div>                                                                  
                                    </div>  
                                    <div class="col-sm-4">
                                        <label for="valorFreteLocal">Valor Frete Local</label>
                                        <input class="form-control somente-numero" type="text" name="valorFreteLocal" id="valorFreteLocal" value="<?=$item['valorFreteLocal']?>" required  autocomplete="off" />                               
                                    </div> 
                                    <div class="col-sm-4">
                                        <label for="diasEntregaLocal">Dias para Entrega Local</label>
                                        <input class="form-control somente-numero" type="number" name="diasEntregaLocal" id="diasEntregaLocal" value="<?=$item['diasEntregaLocal']?>" required  autocomplete="off" />                               
                                    </div> 
                                    <div class="col-sm-4">
                                        <label for="codigoIbgeLocal">Código IBGE Local</label>
                                        <input class="form-control somente-numero" type="text" title="Código do IBGE da cidade onde a Unidade está localizada, ou onde a Unidade entende que é um perímetro local." name="codigoIbgeLocal" id="codigoIbgeLocal" value="<?=$item['codigoIbgeLocal']?>" required  autocomplete="off" />                               
                                    </div>              
                                    
                                    
                                    <div class="col-sm-4">
                                        <label for="valorFreteOutros">Valor Frete Outras Localidades</label>
                                        <input class="form-control somente-numero" type="text" name="valorFreteOutros" id="valorFreteOutros" value="<?=$item['valorFreteOutros']?>" required  autocomplete="off" />                               
                                    </div> 
                                    <div class="col-sm-8">
                                        <label for="diasEntregaOutros">Dias para Entrega Outras Localidades</label>
                                        <input class="form-control somente-numero" type="number" name="diasEntregaOutros" id="diasEntregaOutros" value="<?=$item['diasEntregaOutros']?>" required  autocomplete="off" />                               
                                    </div> 
                                </div>                        
                            </fieldset>


                            <fieldset>
                                <legend>Dados para Retirada</legend>
                                <div class="row">                                    
                                    <div class="col-sm-12">
                                        <label for="textoEndereco">Endereço</label>
                                        <input class="form-control" type="text" name="textoEndereco" id="textoEndereco" value="<?=$item['textoEndereco']?>" required  autocomplete="off" />                               
                                    </div>                                    
                                </div>    
                                <div class="row">                                    
                                    <div class="col-sm-4">
                                        <label for="dataEntrega">Entrega</label>
                                        <input class="form-control" type="date" name="dataEntrega" id="dataEntrega" value="<?=convertDataParaBanco($item['dataEntrega'], 'Y-m-d')?>" required  autocomplete="off" />                                                                                                               
                                    </div>

                                    <div class="col-sm-4">
                                        <label for="horaEntregaInicial">Inicio</label>                                        
                                        <input class="form-control" type="datetime-local" name="horaEntregaInicial" id="horaEntregaInicial" value="<?=date_yyyy_mm_dd_hh_mm_ss($item['horaEntregaInicial'])?>" required  autocomplete="off" />                                                                       
                                    </div>

                                    <div class="col-sm-4">
                                        <label for="horaEntregaFinal">Final</label>
                                        <input class="form-control" type="datetime-local" name="horaEntregaFinal" id="horaEntregaFinal" value="<?=date_yyyy_mm_dd_hh_mm_ss($item['horaEntregaFinal'])?>" required  autocomplete="off" />                                         
                                    </div>                                    
                                </div>  
                                                              
                            </fieldset>

                        </div>
                    </div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
						<button type="button" class="btn btn-success"  onclick="return enviaFormularioSimples('modalFormEditar<?=$item['idParceiro']?>')"  >Salvar Mudanças</button>
					</div>
				</form>
			</div>
		</div>
	</div>

<?php  }?>



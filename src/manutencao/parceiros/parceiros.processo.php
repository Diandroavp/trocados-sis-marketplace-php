<?php	
	$textoDirecionar = "?_p=".$_REQUEST["_p"];	
	$idOperadorCorrente = $_SESSION["_SESSION_idOperador"];
	// Tipo de Pesquisa
	$tipoPesquisa = (isset($_REQUEST['tipoPesquisa'])) ? $_REQUEST['tipoPesquisa'] : null;
	$textoPesquisa = (isset($_REQUEST['textoPesquisa'])) ? $_REQUEST['textoPesquisa'] : null;
	// CRUD
	$tipoAcao = (isset($_REQUEST['tipoAcao'])) ? $_REQUEST['tipoAcao'] : 'S';
	
		$idParceiro = (isset($_REQUEST['idParceiro'])) ? $_REQUEST['idParceiro'] : null;  
		$nomeParceiro = (isset($_REQUEST['nomeParceiro'])) ? $_REQUEST['nomeParceiro'] : null;  	
		$ativoParceiro = (isset($_REQUEST['ativoParceiro'])) ? $_REQUEST['ativoParceiro'] : 0;  
		$ativoRetirada = (isset($_REQUEST['ativoRetirada'])) ? $_REQUEST['ativoRetirada'] : 0;  
		$textoEndereco = (isset($_REQUEST['textoEndereco'])) ? $_REQUEST['textoEndereco'] : null;  
		$nomeImagem = (isset($_REQUEST['nomeImagem'])) ? $_REQUEST['nomeImagem'] : null;  
		$dataEntrega = (isset($_REQUEST['dataEntrega'])) ? $_REQUEST['dataEntrega'] : date('Y-m-d');  
		$horaEntregaInicial = (isset($_REQUEST['horaEntregaInicial'])) ? $_REQUEST['horaEntregaInicial'] : date('Y-m-d\TH:i:s');  
		$horaEntregaFinal = (isset($_REQUEST['horaEntregaFinal'])) ? $_REQUEST['horaEntregaFinal'] : date('Y-m-d\TH:i:s');  
		$idUnidade = (isset($_REQUEST['idUnidade']) && ($_REQUEST['idUnidade'] <> '')) ? $_REQUEST['idUnidade'] : null;  
		$ativoTrocados = (isset($_REQUEST['ativoTrocados'])) ? $_REQUEST['ativoTrocados'] : 0;  
		$ativoEntrega = (isset($_REQUEST['ativoEntrega'])) ? $_REQUEST['ativoEntrega'] : 0;  		
		$ordemVisual = (isset($_REQUEST['ordemVisual'])) ? $_REQUEST['ordemVisual'] : null;  
		$idParceirosGrupo = (isset($_REQUEST['idParceirosGrupo'])) ? $_REQUEST['idParceirosGrupo'] : null;  
		$ativoHome = (isset($_REQUEST['ativoHome'])) ? $_REQUEST['ativoHome'] : 0;  
		
		$idUnidadeMantenedora = (isset($_REQUEST['idUnidadeMantenedora'])) ? $_REQUEST['idUnidadeMantenedora'] : null;  
		$percentualMantenedora = (isset($_REQUEST['percentualMantenedora'])) ? $_REQUEST['percentualMantenedora'] : null;  
		$idUnidadeFrete = (isset($_REQUEST['idUnidadeFrete'])) ? $_REQUEST['idUnidadeFrete'] : null;  
		$valorFreteLocal = (isset($_REQUEST['valorFreteLocal'])) ? $_REQUEST['valorFreteLocal'] : null;  
		$diasEntregaLocal = (isset($_REQUEST['diasEntregaLocal'])) ? $_REQUEST['diasEntregaLocal'] : null; 
		$valorFreteOutros = (isset($_REQUEST['valorFreteOutros'])) ? $_REQUEST['valorFreteOutros'] : null;  
		$diasEntregaOutros = (isset($_REQUEST['diasEntregaOutros'])) ? $_REQUEST['diasEntregaOutros'] : null;  
		$codigoIbgeLocal = (isset($_REQUEST['codigoIbgeLocal'])) ? $_REQUEST['codigoIbgeLocal'] : null;  
		$numeroCelular = (isset($_REQUEST['numeroCelular'])) ? $_REQUEST['numeroCelular'] : null;  
		$numeroCelular = limpaCaracter($numeroCelular);
		$tipoTransacao = (isset($_REQUEST['tipoTransacao'])) ? $_REQUEST['tipoTransacao'] : null;  
		
	  
	// Retorno de Resposta
	$tipoResposta = (isset($_REQUEST['tipoResposta'])) ? $_REQUEST['tipoResposta'] : null;
	$textoRetorno = (isset($_REQUEST['textoRetorno'])) ? $_REQUEST['textoRetorno'] : null;
	$ativoRetorno = (isset($_REQUEST['ativoRetorno'])) ? $_REQUEST['ativoRetorno'] : null;	
	

    if (isset($_REQUEST['tipoAcao']) && ($tipoAcao != 'S')){
		
		$dadosResposta = parceiros(	$tipoAcao, 
									$idParceiro, 
									$nomeParceiro, 
									$ativoParceiro, 
									$ativoRetirada, 
									$textoEndereco, 
									$nomeImagem, 
									convertDataParaBanco($dataEntrega),
									convertDataParaBanco($horaEntregaInicial, null, true),
									convertDataParaBanco($horaEntregaFinal, null, true),
									$idUnidade, 
									$ativoTrocados, 
									$ativoEntrega, 
									$ordemVisual, 
									$idParceirosGrupo, 
									$ativoProducao, 
									$ativoHome,
									
									$idUnidadeMantenedora, $percentualMantenedora, 
									
									$idUnidadeFrete, $valorFreteLocal, $diasEntregaLocal, $valorFreteOutros, $diasEntregaOutros, 
									
									$codigoIbgeLocal, $numeroCelular, $tipoTransacao);
								
		if ($dadosResposta and ($dadosResposta[0]['ativoRetorno'])){
			//AUDITORIA
			include "parceiros.auditoria.php";
		}
		//
		include "parceiros.processo.resposta.php";
    }
	if (isset($_REQUEST['tipoResposta'])){
		showMessage(($ativoRetorno == 1) ? "S" : "E", $_CONFIGURACAO_TITULO." - Informativo", $textoRetorno);
	}	


	// Montando Tipo Transação
	$paramTipoTransacao = array();
	$paramTipoTransacao[] = array('tipoTransacao'=> 'cd', 'nomeTipoTransacao'	=> 'Compra Direta');
	$paramTipoTransacao[] = array('tipoTransacao'=> 'vc', 'nomeTipoTransacao'	=> 'Gerar Vale Compra');
	
	// Montando tipo de Pesquisa
	$paramPesquisa = array();
	$paramPesquisa[] = array('idParceiro', 'Código');
	$paramPesquisa[] = array('nomeParceiro', 'Nome');	
	if (!$tipoPesquisa){
		$tipoPesquisa = $paramPesquisa[1][0];
	}	

	// 
	if (($tipoAcao == 'S')) {
		// Dados da grid principal
		$dadosParceiros = parceiros($tipoAcao, 
									(($textoPesquisa != '') && ($tipoPesquisa == 'idParceiro')) ? $textoPesquisa : null,
									(($textoPesquisa != '') && ($tipoPesquisa == 'nomeParceiro')) ? $textoPesquisa : null,
									$ativoParceiro, 
									$ativoRetirada, 
									$textoEndereco, 
									$nomeImagem, 
									convertDataParaBanco($dataEntrega),
									date_yyyy_mm_dd_hh_mm_ss($horaEntregaInicial),
									date_yyyy_mm_dd_hh_mm_ss($horaEntregaFinal),
									$idUnidade, 
									$ativoTrocados, 
									$ativoEntrega, 
									$ordemVisual, 
									$idParceirosGrupo, 
									$ativoProducao,
									$ativoHome,
									$idUnidadeMantenedora, $percentualMantenedora, $idUnidadeFrete, $valorFreteLocal, $diasEntregaLocal, $valorFreteOutros, $diasEntregaOutros, $codigoIbgeLocal, $numeroCelular, $tipoTransacao);

		$dadosParceirosGrupo = parceirosGrupo($tipoAcao,null,null,null,null,null);
		$dadosUnidade = unidadeConsultar($tipoAcao, null, null, $ativoProducao);
	}
		
    $numeroLinhas = 0;
?>


<!-- Esse trecho é importante para colocar os IDs e Nomes para os Inputs -->
<?php foreach($dadosParceirosGrupo as $item) {?>
	<div class="modal fade" data-backdrop="static" id="ModeloEditar<?=$item['idParceirosGrupo']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<form role="form" name="modalForm" id="modalFormEditar<?=$item['idParceirosGrupo']?>" action="<?=$textoDirecionar?>" method="post">
					<div class="modal-header bg-primary">
						<button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×   </span><span class="sr-only">Fechar</span>

						</button>
						<h4 class="modal-title" id="myModalLabel">Editando Registro [<?=$item['idParceirosGrupo']?>]</h4>
					</div>
					<div class="modal-body">
                        <div class="form-group">		
                            <input type="hidden" name="tipoAcao"  value="E" />
                            <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
                            <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
                            <input type="hidden" name="idParceirosGrupo"  value="<?=$item['idParceirosGrupo']?>" />
                        
                            <fieldset>
                                <legend>Dados do Grupo</legend>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label for="Código">Código</label>
                                        <input class="form-control" Disabled  value="<?=$item['idParceirosGrupo']?>" />                               
                                    </div>
                                </div>
                                <div class="row">                                    
                                    <div class="col-sm-12">
                                        <label for="nomeParceirosGrupo">Nome</label>
                                        <input class="form-control" type="text" name="nomeParceirosGrupo" id="nomeParceirosGrupo" value="<?=$item['nomeParceirosGrupo']?>" required  autocomplete="off" />                               
                                    </div>                                    
                                </div>    
                                <div class="row">                                    
                                    <div class="col-sm-12">
                                        <label for="nomeImagem">Imagem</label>
                                        <input class="form-control" type="text" name="nomeImagem"  value="<?=$item['nomeImagem']?>" required  autocomplete="off" />                               
                                    </div>                                    
                                </div>    
                                <div class="row">                                    
                                    <div class="col-sm-7">
                                        <label for="ordemParceiros">Ordem Visual</label>
                                        <input class="form-control" type="text" name="ordemParceiros" id="ordemParceiros" value="<?=$item['ordemParceiros']?>" required  autocomplete="off" />                               
                                    </div>        
                                    <div class="col-sm-5">                                                                            
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoParceirosGrupo" id="ativoParceirosGrupo" value="1" <?=($item['ativoParceirosGrupo'] ? 'checked' : '' )?>> Grupo Ativo</label>                                          
                                        </div>
                                    </div>                             
                                </div>    

                                                              
                            </fieldset>
                        </div>
                    </div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
						<button type="button" class="btn btn-success"  onclick="return enviaFormularioSimples('modalFormEditar<?=$item['idParceirosGrupo']?>')"  >Salvar Mudanças</button>
					</div>
				</form>
			</div>
		</div>
	</div>

<?php  }?>

<?php	
    $textoDirecionar = "?_p=".$_REQUEST["_p"];	
	$idOperadorCorrente = $_SESSION["_SESSION_idOperador"];
	// Tipo de Pesquisa
	$tipoPesquisa = (isset($_REQUEST['tipoPesquisa'])) ? $_REQUEST['tipoPesquisa'] : null;
	$textoPesquisa = (isset($_REQUEST['textoPesquisa'])) ? $_REQUEST['textoPesquisa'] : null;
	// CRUD
	$tipoAcao = (isset($_REQUEST['tipoAcao'])) ? $_REQUEST['tipoAcao'] : 'S';
	
	$idParceirosGrupo = (isset($_REQUEST['idParceirosGrupo'])) ? $_REQUEST['idParceirosGrupo'] : null;  
	$nomeParceirosGrupo = (isset($_REQUEST['nomeParceirosGrupo'])) ? $_REQUEST['nomeParceirosGrupo'] : null;  
	$ativoParceirosGrupo = (isset($_REQUEST['ativoParceirosGrupo'])) ? $_REQUEST['ativoParceirosGrupo'] : 0;  
	$nomeImagem = (isset($_REQUEST['nomeImagem'])) ? $_REQUEST['nomeImagem'] : null;  
	$ordemParceiros = (isset($_REQUEST['ordemParceiros'])) ? $_REQUEST['ordemParceiros'] : null;  
	  
	// Retorno de Resposta
	$tipoResposta = (isset($_REQUEST['tipoResposta'])) ? $_REQUEST['tipoResposta'] : null;
	$textoRetorno = (isset($_REQUEST['textoRetorno'])) ? $_REQUEST['textoRetorno'] : null;
	$ativoRetorno = (isset($_REQUEST['ativoRetorno'])) ? $_REQUEST['ativoRetorno'] : null;	
	

    if (isset($_REQUEST['tipoAcao']) && ($tipoAcao != 'S')){
		
		$dadosResposta = parceirosGrupo($tipoAcao, $idParceirosGrupo, $nomeParceirosGrupo, $ativoParceirosGrupo, $nomeImagem, $ordemParceiros);
								
		if ($dadosResposta and ($dadosResposta[0]['ativoRetorno'])){
			//AUDITORIA
			include "parceirosGrupo.auditoria.php";
		}
		//
		include "parceirosGrupo.processo.resposta.php";
    }
	if (isset($_REQUEST['tipoResposta'])){
		showMessage(($ativoRetorno == 1) ? "S" : "E", $_CONFIGURACAO_TITULO." - Informativo", $textoRetorno);
	}	

	// Montando tipo de Pesquisa
	$paramPesquisa = array();
	$paramPesquisa[] = array('idParceirosGrupo', 'Código');
	$paramPesquisa[] = array('nomeParceirosGrupo', 'Nome');
	if (!$tipoPesquisa){
		$tipoPesquisa = $paramPesquisa[1][0];
	}		

	// 
	if (($tipoAcao == 'S')) {
		// Dados da grid principal
		$dadosParceirosGrupo = parceirosGrupo($tipoAcao, 
										(($textoPesquisa != '') && ($tipoPesquisa == 'idParceirosGrupo')) ? $textoPesquisa : null,
										(($textoPesquisa != '') && ($tipoPesquisa == 'nomeParceirosGrupo')) ? $textoPesquisa : null,
										$ativoParceirosGrupo, 
										$nomeImagem, 
										$ordemParceiros);
	}
		
    $numeroLinhas = 0;
?>


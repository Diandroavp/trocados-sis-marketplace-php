<!-- Esse trecho é importante para colocar os IDs e Nomes para os Inputs -->
<?php foreach($dadosCategoria as $item) {?>
	<div class="modal fade" data-backdrop="static" id="ModeloEditar<?=$item['idCategoria']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<form role="form" name="modalForm" id="modalFormEditar<?=$item['idCategoria']?>" action="<?=$textoDirecionar?>" method="post">
					<div class="modal-header bg-primary">
						<button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×   </span><span class="sr-only">Fechar</span>

						</button>
						<h4 class="modal-title" id="myModalLabel">Editando Registro [<?=$item['idCategoria']?>]</h4>
					</div>
					<div class="modal-body">
                        <div class="form-group">		
                            <input type="hidden" name="tipoAcao"  value="E" />
                            <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
                            <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
                            <input type="hidden" name="idCategoria"  value="<?=$item['idCategoria']?>" />
                        
                            <fieldset>
                                <legend>Dados da Categoria</legend>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label for="Código">Código</label>
                                        <input class="form-control" Disabled  value="<?=$item['idCategoria']?>" />                               
                                    </div>
                                </div>
                                <div class="row">                                                                        
                                    <div class="col-sm-12">
                                        <label for="nomeCategoria">Descrição</label>
                                        <input class="form-control" type="text" name="nomeCategoria" id="nomeCategoria" value="<?=$item['nomeCategoria']?>" required  autocomplete="off" />                               
                                    </div>                               	         
                                    <div class="col-sm-4">
                                        <label for="ordemCategoria">Ordem</label>
                                        <input class="form-control" type="text" name="ordemCategoria" id="ordemCategoria" value="<?=$item['ordemCategoria']?>" required  autocomplete="off" />                               
                                    </div> 
                                    <div class="col-sm-8">
                                        <label for="nomeImagem">Imagem</label>
                                        <input class="form-control" type="text" name="nomeImagem"  value="<?=$item['nomeImagem']?>" required  autocomplete="off" />                               
                                    </div>  
                                </div>      
                                <div class="row">
                                    <div class="col-sm-6"> 
                                        <div class="checkbox">
                                        <br>                                        
                                            <label><input type="checkbox" name="ativoCategoria" id="ativoCategoria" value="1" <?=($item['ativoCategoria'] ? 'checked' : '' )?>> Ativo</label>                                          
                                        </div>
                                    </div>
                                    <div class="col-sm-6">   
                                        <div class="checkbox"> 
                                        <br>
                                            <label><input type="checkbox" name="ativoEvidencia" id="ativoEvidencia" value="1" <?=($item['ativoEvidencia'] ? 'checked' : '' )?>> Evidenciar</label>                                                                                                                   
                                        </div>
                                    </div>
                                </div>                         
                            </fieldset>
                        </div>
                    </div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
						<button type="button" class="btn btn-success"  onclick="return enviaFormularioSimples('modalFormEditar<?=$item['idCategoria']?>')"  >Salvar Mudanças</button>
					</div>
				</form>
			</div>
		</div>
	</div>

<?php  }?>


<!-- Esse trecho é importante para colocar os IDs e Nomes para os Inputs -->
<div class="modal fade " data-backdrop="static" id="myModalAdicionar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-sm">
        <div class="modal-content" >
            <form role="form" name="modalForm" id="modalFormAdicionar" action="<?=$textoDirecionar?>" method="post" autocomplete="off">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×   </span><span class="sr-only">Fechar</span>

                    </button>
                    <h4 class="modal-title" id="myModalLabel">Novo Registro</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">		
                        <input type="hidden" name="tipoAcao"  value="I" />
                        <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
	                    <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
                        
                        <fieldset>
                            <legend>Dados da Categoria</legend>
                           
                                <div class="row">                                                                        
                                    <div class="col-sm-12">
                                        <label for="nomeCategoria">Descrição</label>
                                        <input class="form-control" type="text" name="nomeCategoria" id="nomeCategoria"  required  autocomplete="off" />                               
                                    </div>                               	         
                                    <div class="col-sm-4">
                                        <label for="ordemCategoria">Ordem</label>
                                        <input class="form-control" type="text" name="ordemCategoria" id="ordemCategoria"  required  autocomplete="off" />                               
                                    </div> 
                                    <div class="col-sm-8">
                                        <label for="nomeImagem">Imagem</label>
                                        <input class="form-control" type="text" name="nomeImagem"   required  autocomplete="off" />                               
                                    </div>  
                                </div>      
                                <div class="row">
                                    <div class="col-sm-6">  
                                        <div class="checkbox">   
                                            <br>                                    
                                            <label><input type="checkbox" name="ativoCategoria" id="ativoCategoria" value="1"  checked> Ativo</label>                                          
                                        </div>
                                    </div>
                                    <div class="col-sm-6">   
                                        <div class="checkbox">                      
                                            <br>                                                     
                                            <label><input type="checkbox" name="ativoEvidencia" id="ativoEvidencia" value="1"  > Evidenciar</label>                                                                                                                   
                                        </div> 
                                    </div>
                                </div>                                                   
                                 
                        </fieldset>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="button" onclick="return enviaFormularioSimples('modalFormAdicionar')" class="btn btn-success" >Salvar Mudanças</button>
                </div>
            </form>
        </div>
    </div>
</div>
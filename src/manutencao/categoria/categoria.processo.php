<?php	
    $textoDirecionar = "?_p=".$_REQUEST["_p"];	
	$idOperadorCorrente = $_SESSION["_SESSION_idOperador"];
	// Tipo de Pesquisa
	$tipoPesquisa = (isset($_REQUEST['tipoPesquisa'])) ? $_REQUEST['tipoPesquisa'] : null;
	$textoPesquisa = (isset($_REQUEST['textoPesquisa'])) ? $_REQUEST['textoPesquisa'] : null;
	// CRUD
	$tipoAcao = (isset($_REQUEST['tipoAcao'])) ? $_REQUEST['tipoAcao'] : 'S';
	
	$idCategoria = (isset($_REQUEST['idCategoria'])) ? $_REQUEST['idCategoria'] : null;  
	$ordemCategoria = (isset($_REQUEST['ordemCategoria'])) ? $_REQUEST['ordemCategoria'] : null;  
	$nomeCategoria = (isset($_REQUEST['nomeCategoria'])) ? $_REQUEST['nomeCategoria'] : null;  
	$nomeImagem = (isset($_REQUEST['nomeImagem'])) ? $_REQUEST['nomeImagem'] : null;  
	$ativoCategoria = (isset($_REQUEST['ativoCategoria'])) ? $_REQUEST['ativoCategoria'] : 0;  
	$ativoEvidencia = (isset($_REQUEST['ativoEvidencia'])) ? $_REQUEST['ativoEvidencia'] : 0;  
	  
	// Retorno de Resposta
	$tipoResposta = (isset($_REQUEST['tipoResposta'])) ? $_REQUEST['tipoResposta'] : null;
	$textoRetorno = (isset($_REQUEST['textoRetorno'])) ? $_REQUEST['textoRetorno'] : null;
	$ativoRetorno = (isset($_REQUEST['ativoRetorno'])) ? $_REQUEST['ativoRetorno'] : null;	
	

    if (isset($_REQUEST['tipoAcao']) && ($tipoAcao != 'S')){
		
		$dadosResposta = categoria($tipoAcao, $idCategoria, $nomeCategoria, $ordemCategoria, $nomeImagem, $ativoCategoria, $ativoEvidencia);
								
		if ($dadosResposta and ($dadosResposta[0]['ativoRetorno'])){
			//AUDITORIA
			include "categoria.auditoria.php";
		}
		//
		include "categoria.processo.resposta.php";
    }
	if (isset($_REQUEST['tipoResposta'])){
		showMessage(($ativoRetorno == 1) ? "S" : "E", $_CONFIGURACAO_TITULO." - Informativo", $textoRetorno);
	}	

	// Montando tipo de Pesquisa
	$paramPesquisa = array();
	$paramPesquisa[] = array('idCategoria', 'Código');
	$paramPesquisa[] = array('nomeCategoria', 'Descrição');
	if (!$tipoPesquisa){
		$tipoPesquisa = $paramPesquisa[1][0];
	}	

	// 
	if (($tipoAcao == 'S')) {
		// Dados da grid principal
		$dadosCategoria = categoria($tipoAcao, 
									(($textoPesquisa != '') && ($tipoPesquisa == 'idCategoria')) ? $textoPesquisa : null,
									(($textoPesquisa != '') && ($tipoPesquisa == 'nomeCategoria')) ? $textoPesquisa : null, 
									$ordemCategoria, 
									$nomeImagem, 
									$ativoCategoria, 
									$ativoEvidencia);
	}
		
    $numeroLinhas = 0;
?>


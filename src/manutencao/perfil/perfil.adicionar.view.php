<!-- Esse trecho é importante para colocar os IDs e Nomes para os Inputs -->
<div class="modal fade " data-backdrop="static" id="myModalAdicionar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-md">
        <div class="modal-content" >
            <form role="form" name="modalForm" id="modalFormAdicionar" action="<?=$textoDirecionar?>" method="post" autocomplete="off">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×   </span><span class="sr-only">Fechar</span>

                    </button>
                    <h4 class="modal-title" id="myModalLabel">Novo Registro</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">		
                        <input type="hidden" name="tipoAcao"  value="I" />
                        <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
	                    <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
                        
                        <fieldset>
                                <legend>Dados da Perfil</legend>
                                <div class="row">
                                    <div class="col-sm-8">
                                        <label for="nomePerfil">Descrição</label>
                                        <input class="form-control" type="text" name="nomePerfil" value="" required  autocomplete="off" />                               
                                    </div>              
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoCompleto"  value="1" > Acesso Completo</label>                                          
                                        </div>
                                    </div>                  	                                
                                </div>     
                            </fieldset>  
                            <br>
                            <fieldset>
                                <legend>Cadastro</legend>            
                                <div class="row">                                    
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoOperadores"  value="1" > Operadores</label>                                          
                                        </div>
                                    </div>                                    
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoProdutos"  value="1" > Produto</label>                                          
                                        </div>
                                    </div>
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoProdutoPorCategoria"  value="1" > Produto x Categoria</label>                                          
                                        </div>
                                    </div>                                  
                                </div>                            
                            </fieldset>
                            <br>
                            <fieldset>
                                <legend>Dashboard</legend>            
                                <div class="row">
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoDashboard"  value="1" > Dashboard</label>                                          
                                        </div>
                                    </div>                                      
                                </div>                            
                            </fieldset>
                            <br>
                            <fieldset>
                                <legend>Movimento</legend>            
                                <div class="row">
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoValidarCompra"  value="1" > Validar Compra</label>                                          
                                        </div>
                                    </div>
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoHistoricoValidacao"  value="1" > Histórico Validação</label>                                          
                                        </div>
                                    </div>                                    
                                </div>                            
                            </fieldset>
                            <br>
                            <fieldset>
                                <legend>Financeiro</legend>            
                                <div class="row">                                    
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoComprasRealizadas"  value="1" > Compras Realizadas</label>                                          
                                        </div>
                                    </div>
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoSaldoUnidades"  value="1" > Saldo Unidade</label>                                          
                                        </div>
                                    </div>                                    
                                </div>                            
                            </fieldset>
                            <br>
                            <fieldset>
                                <legend>Estoque</legend>            
                                <div class="row">                                    
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoEstoqueNotas"  value="1" > Notas</label>                                          
                                        </div>
                                    </div>                                    
                                </div>                            
                            </fieldset>
                            <br>
                            <fieldset>
                                <legend>Consulta</legend>            
                                <div class="row">                                   
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoConsultaEstoqueDisponivel"  value="1" > Estoque Disponível</label>                                          
                                        </div>
                                    </div>                                    
                                </div>                            
                            </fieldset>
                            <br>
                            <fieldset>
                                <legend>Manutenção</legend>            
                                <div class="row">
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoCategoria"  value="1" > Categoria</label>                                          
                                        </div>
                                    </div>
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoGrupoParceiros"  value="1" > Grupo Parceiro</label>                                          
                                        </div>
                                    </div>
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoParceiros"  value="1" > Parceiros</label>                                          
                                        </div>
                                    </div>
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoPerfil"  value="1" > Perfil</label>                                          
                                        </div>
                                    </div>
                                </div>                            
                            </fieldset>
                            <br>
                            <fieldset>
                                <legend>Tabela</legend>            
                                <div class="row">
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoMedidas"  value="1" > Medidas</label>                                          
                                        </div>
                                    </div>
                                </div>                            
                            </fieldset>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="button" onclick="return enviaFormularioSimples('modalFormAdicionar')" class="btn btn-success" >Salvar Mudanças</button>
                </div>
            </form>
        </div>
    </div>
</div>
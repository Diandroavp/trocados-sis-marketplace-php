<?php	
    $textoDirecionar = "?_p=".$_REQUEST["_p"];	
	$idOperadorCorrente = $_SESSION["_SESSION_idOperador"];
	// Tipo de Pesquisa
	$tipoPesquisa = (isset($_REQUEST['tipoPesquisa'])) ? $_REQUEST['tipoPesquisa'] : null;
	$textoPesquisa = (isset($_REQUEST['textoPesquisa'])) ? $_REQUEST['textoPesquisa'] : null;
	// CRUD
	$tipoAcao = (isset($_REQUEST['tipoAcao'])) ? $_REQUEST['tipoAcao'] : 'S';
	
	$idPerfil = (isset($_REQUEST['idPerfil'])) ? $_REQUEST['idPerfil'] : null;  	
	$nomePerfil = (isset($_REQUEST['nomePerfil'])) ? $_REQUEST['nomePerfil'] : null;  
	$ativoDashboard = (isset($_REQUEST['ativoDashboard'])) ? $_REQUEST['ativoDashboard'] : 0;  
	$ativoCategoria = (isset($_REQUEST['ativoCategoria'])) ? $_REQUEST['ativoCategoria'] : 0;  
	$ativoGrupoParceiros = (isset($_REQUEST['ativoGrupoParceiros'])) ? $_REQUEST['ativoGrupoParceiros'] : 0;  
	$ativoMedidas = (isset($_REQUEST['ativoMedidas'])) ? $_REQUEST['ativoMedidas'] : 0;  
	$ativoOperadores = (isset($_REQUEST['ativoOperadores'])) ? $_REQUEST['ativoOperadores'] : 0;  
	$ativoParceiros = (isset($_REQUEST['ativoParceiros'])) ? $_REQUEST['ativoParceiros'] : 0;  
	$ativoProdutos = (isset($_REQUEST['ativoProdutos'])) ? $_REQUEST['ativoProdutos'] : 0;  
	$ativoProdutoPorCategoria = (isset($_REQUEST['ativoProdutoPorCategoria'])) ? $_REQUEST['ativoProdutoPorCategoria'] : 0;  
	$ativoValidarCompra = (isset($_REQUEST['ativoValidarCompra'])) ? $_REQUEST['ativoValidarCompra'] : 0;  
	$ativoHistoricoValidacao = (isset($_REQUEST['ativoHistoricoValidacao'])) ? $_REQUEST['ativoHistoricoValidacao'] : 0;  
	$ativoComprasRealizadas = (isset($_REQUEST['ativoComprasRealizadas'])) ? $_REQUEST['ativoComprasRealizadas'] : 0;  
	$ativoSaldoUnidades = (isset($_REQUEST['ativoSaldoUnidades'])) ? $_REQUEST['ativoSaldoUnidades'] : 0;  
	$ativoEstoqueNotas = (isset($_REQUEST['ativoEstoqueNotas'])) ? $_REQUEST['ativoEstoqueNotas'] : 0;  
	$ativoConsultaEstoqueDisponivel = (isset($_REQUEST['ativoConsultaEstoqueDisponivel'])) ? $_REQUEST['ativoConsultaEstoqueDisponivel'] : 0;  
	$ativoPerfil = (isset($_REQUEST['ativoPerfil'])) ? $_REQUEST['ativoPerfil'] : 0;  
	$ativoCompleto = (isset($_REQUEST['ativoCompleto'])) ? $_REQUEST['ativoCompleto'] : 0;  

	// Retorno de Resposta
	$tipoResposta = (isset($_REQUEST['tipoResposta'])) ? $_REQUEST['tipoResposta'] : null;
	$textoRetorno = (isset($_REQUEST['textoRetorno'])) ? $_REQUEST['textoRetorno'] : null;
	$ativoRetorno = (isset($_REQUEST['ativoRetorno'])) ? $_REQUEST['ativoRetorno'] : null;	
	

    if (isset($_REQUEST['tipoAcao']) && ($tipoAcao != 'S')){
		
		$dadosResposta = perfil($tipoAcao,
								$idPerfil,
								$nomePerfil, 
								$ativoDashboard, 
								$ativoCategoria,
								$ativoGrupoParceiros,
								$ativoMedidas,
								$ativoOperadores,
								$ativoParceiros,
								$ativoProdutos,
								$ativoProdutoPorCategoria,
								$ativoValidarCompra,
								$ativoHistoricoValidacao,
								$ativoComprasRealizadas,
								$ativoSaldoUnidades,
								$ativoEstoqueNotas,
								$ativoConsultaEstoqueDisponivel,
								$ativoPerfil,
								$ativoCompleto);
								
		if ($dadosResposta and ($dadosResposta[0]['ativoRetorno'])){
			//AUDITORIA
			include "Perfil.auditoria.php";
		}
		//
		include "Perfil.processo.resposta.php";
    }
	if (isset($_REQUEST['tipoResposta'])){
		showMessage(($ativoRetorno == 1) ? "S" : "E", $_CONFIGURACAO_TITULO." - Informativo", $textoRetorno);
	}	

	// Montando tipo de Pesquisa
	$paramPesquisa = array();
	$paramPesquisa[] = array('idPerfil', 'Código');
	$paramPesquisa[] = array('nomePerfil', 'Nome');
	if (!$tipoPesquisa){
		$tipoPesquisa = $paramPesquisa[1][0];
	}	

	// 
	if (($tipoAcao == 'S')) {
		// Dados da grid principal
		$dadosPerfil = perfil($tipoAcao, 
							(($textoPesquisa != '') && ($tipoPesquisa == 'idPerfil')) ? $textoPesquisa : null,							
							(($textoPesquisa != '') && ($tipoPesquisa == 'nomePerfil')) ? $textoPesquisa : null,
							$ativoDashboard, 
							$ativoCategoria,
							$ativoGrupoParceiros,
							$ativoMedidas,
							$ativoOperadores,
							$ativoParceiros,
							$ativoProdutos,
							$ativoProdutoPorCategoria,
							$ativoValidarCompra,
							$ativoHistoricoValidacao,
							$ativoComprasRealizadas,
							$ativoSaldoUnidades,
							$ativoEstoqueNotas,
							$ativoConsultaEstoqueDisponivel,
							$ativoPerfil,
							$ativoCompleto);
	}
		
    $numeroLinhas = 0;
?>


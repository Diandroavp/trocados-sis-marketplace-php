<?php 
    //AUDITORIA
    $descricaoAuditoria = 'Perfil ['.$idOperadorCorrente.';'.$idPerfil.']';
    //					
    $tipoAuditoria  = $tipoAcao;	    					
    //
    $textoAuditoria = 'Cadastro de Perfil => ';
    $textoAuditoria = $textoAuditoria.'idOperadorCorrente: '.$idOperadorCorrente;
    $textoAuditoria = $textoAuditoria.';textoDirecionar: '.$textoDirecionar;
    $textoAuditoria = $textoAuditoria.';tipoAcao: '.$tipoAcao;	
    $textoAuditoria = $textoAuditoria.';tipoResposta: '.$tipoResposta;			
    $textoAuditoria = $textoAuditoria.';textoRetorno: '.$textoRetorno;			
    $textoAuditoria = $textoAuditoria.';ativoRetorno: '.$ativoRetorno;	
    		
    $textoAuditoria = $textoAuditoria.';idPerfil: '.$idPerfil;					
    $textoAuditoria = $textoAuditoria.';nomePerfil: '.$nomePerfil;		
    $textoAuditoria = $textoAuditoria.';ativoDashboard: '.$ativoDashboard;		
    $textoAuditoria = $textoAuditoria.';ativoCategoria: '.$ativoCategoria;		
    $textoAuditoria = $textoAuditoria.';ativoGrupoParceiros: '.$ativoGrupoParceiros;		
    $textoAuditoria = $textoAuditoria.';ativoMedidas: '.$ativoMedidas;		
    $textoAuditoria = $textoAuditoria.';ativoOperadores: '.$ativoOperadores;		
    $textoAuditoria = $textoAuditoria.';ativoParceiros: '.$ativoParceiros;		
    $textoAuditoria = $textoAuditoria.';ativoProdutos: '.$ativoProdutos;		
    $textoAuditoria = $textoAuditoria.';ativoValidarCompra: '.$ativoValidarCompra;		
    $textoAuditoria = $textoAuditoria.';ativoHistoricoValidacao: '.$ativoHistoricoValidacao;		
    $textoAuditoria = $textoAuditoria.';ativoComprasRealizadas: '.$ativoComprasRealizadas;		
    $textoAuditoria = $textoAuditoria.';ativoSaldoUnidades: '.$ativoSaldoUnidades;		
    $textoAuditoria = $textoAuditoria.';ativoEstoqueNotas: '.$ativoEstoqueNotas;		
    $textoAuditoria = $textoAuditoria.';ativoConsultaEstoqueDisponivel: '.$ativoConsultaEstoqueDisponivel;		
    $textoAuditoria = $textoAuditoria.';ativoPerfil: '.$ativoPerfil;		
    $textoAuditoria = $textoAuditoria.';ativoCompleto: '.$ativoCompleto;		

        
    inserirAuditoria($descricaoAuditoria, $tipoAuditoria, $textoAuditoria);
?>
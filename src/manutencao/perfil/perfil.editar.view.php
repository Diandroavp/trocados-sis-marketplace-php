<!-- Esse trecho é importante para colocar os IDs e Nomes para os Inputs -->
<?php foreach($dadosPerfil as $item) {?>
	<div class="modal fade" data-backdrop="static" id="ModeloEditar<?=$item['idPerfil']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<form role="form" name="modalForm" id="modalFormEditar<?=$item['idPerfil']?>" action="<?=$textoDirecionar?>" method="post">
					<div class="modal-header bg-primary">
						<button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×   </span><span class="sr-only">Fechar</span>

						</button>
						<h4 class="modal-title" id="myModalLabel">Editando Registro [<?=$item['idPerfil']?>]</h4>
					</div>
					<div class="modal-body">
                        <div class="form-group">		
                            <input type="hidden" name="tipoAcao"  value="E" />
                            <input type="hidden" name="tipoPesquisa"  value="<?=$tipoPesquisa?>" />
                            <input type="hidden" name="textoPesquisa"  value="<?=$textoPesquisa?>" />
                            <input type="hidden" name="idPerfil"  value="<?=$item['idPerfil']?>" />
                        
                            <fieldset>
                                <legend>Dados da Perfil</legend>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label for="Código">Código</label>
                                        <input class="form-control" Disabled  value="<?=$item['idPerfil']?>" />                               
                                    </div>                                                                  
                                    <div class="col-sm-6">
                                        <label for="nomePerfil">Descrição</label>
                                        <input class="form-control" type="text" name="nomePerfil" value="<?=$item['nomePerfil']?>" required  autocomplete="off" />                               
                                    </div>              
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoCompleto"  value="1" <?=($item['ativoCompleto'] ? 'checked' : '' )?>> Acesso Completo</label>                                          
                                        </div>
                                    </div>                  	                                
                                </div>     
                            </fieldset>  
                            <br>
                            <fieldset>
                                <legend>Cadastro</legend>            
                                <div class="row">                                    
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoOperadores"  value="1" <?=($item['ativoOperadores'] ? 'checked' : '' )?>> Operadores</label>                                          
                                        </div>
                                    </div>                                    
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoProdutos"  value="1" <?=($item['ativoProdutos'] ? 'checked' : '' )?>> Produto</label>                                          
                                        </div>
                                    </div>
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoProdutoPorCategoria"  value="1" <?=($item['ativoProdutoPorCategoria'] ? 'checked' : '' )?>> Produto x Categoria</label>                                          
                                        </div>
                                    </div>                                  
                                </div>                            
                            </fieldset>
                            <br>
                            <fieldset>
                                <legend>Dashboard</legend>            
                                <div class="row">
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoDashboard"  value="1" <?=($item['ativoDashboard'] ? 'checked' : '' )?>> Dashboard</label>                                          
                                        </div>
                                    </div>                                      
                                </div>                            
                            </fieldset>
                            <br>
                            <fieldset>
                                <legend>Movimento</legend>            
                                <div class="row">
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoValidarCompra"  value="1" <?=($item['ativoValidarCompra'] ? 'checked' : '' )?>> Validar Compra</label>                                          
                                        </div>
                                    </div>
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoHistoricoValidacao"  value="1" <?=($item['ativoHistoricoValidacao'] ? 'checked' : '' )?>> Histórico Validação</label>                                          
                                        </div>
                                    </div>                                    
                                </div>                            
                            </fieldset>
                            <br>
                            <fieldset>
                                <legend>Financeiro</legend>            
                                <div class="row">                                    
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoComprasRealizadas"  value="1" <?=($item['ativoComprasRealizadas'] ? 'checked' : '' )?>> Compras Realizadas</label>                                          
                                        </div>
                                    </div>
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoSaldoUnidades"  value="1" <?=($item['ativoSaldoUnidades'] ? 'checked' : '' )?>> Saldo Unidade</label>                                          
                                        </div>
                                    </div>                                    
                                </div>                            
                            </fieldset>
                            <br>
                            <fieldset>
                                <legend>Estoque</legend>            
                                <div class="row">                                    
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoEstoqueNotas"  value="1" <?=($item['ativoEstoqueNotas'] ? 'checked' : '' )?>> Notas</label>                                          
                                        </div>
                                    </div>                                    
                                </div>                            
                            </fieldset>
                            <br>
                            <fieldset>
                                <legend>Consulta</legend>            
                                <div class="row">                                   
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoConsultaEstoqueDisponivel"  value="1" <?=($item['ativoConsultaEstoqueDisponivel'] ? 'checked' : '' )?>> Estoque Disponível</label>                                          
                                        </div>
                                    </div>                                    
                                </div>                            
                            </fieldset>
                            <br>
                            <fieldset>
                                <legend>Manutenção</legend>            
                                <div class="row">
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoCategoria"  value="1" <?=($item['ativoCategoria'] ? 'checked' : '' )?>> Categoria</label>                                          
                                        </div>
                                    </div>
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoGrupoParceiros"  value="1" <?=($item['ativoGrupoParceiros'] ? 'checked' : '' )?>> Grupo Parceiro</label>                                          
                                        </div>
                                    </div>
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoParceiros"  value="1" <?=($item['ativoParceiros'] ? 'checked' : '' )?>> Parceiros</label>                                          
                                        </div>
                                    </div>
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoPerfil"  value="1" <?=($item['ativoPerfil'] ? 'checked' : '' )?>> Perfil</label>                                          
                                        </div>
                                    </div>
                                </div>                            
                            </fieldset>
                            <br>
                            <fieldset>
                                <legend>Tabela</legend>            
                                <div class="row">
                                    <div class="col-4 col-sm-4">
                                        <div class="checkbox">   
                                            <br>                                      
                                            <label><input type="checkbox" name="ativoMedidas"  value="1" <?=($item['ativoMedidas'] ? 'checked' : '' )?>> Medidas</label>                                          
                                        </div>
                                    </div>
                                </div>                            
                            </fieldset>
                        </div>
                    </div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
						<button type="button" class="btn btn-success"  onclick="return enviaFormularioSimples('modalFormEditar<?=$item['idPerfil']?>')"  >Salvar Mudanças</button>
					</div>
				</form>
			</div>
		</div>
	</div>

<?php  }?>

<div class="container " style="overflow-y: auto; min-height:450px; ">
    <div class="page-header">
        <h4>Movimento/Validar</h4>      
    </div>
	<div class="well">
        <form role="form" name="modalForm" id="formPesquisa" action="<?=$textoDirecionar?>" method="post" autocomplete="off">
        <input type="hidden" name="tipoAcao"  value="S" />
        <input type="hidden" name="idParceiroCorrente"  value="<?=$idParceiroCorrente?>" />  
            <div class="row">				
                <div class="col-sm-2">
                    <label for="Nome">Tipo de Pesquisa</label>
					<div class="form-group">
					  <select class="form-control" id="tipoPesquisa" name="tipoPesquisa" required>							
						<?php foreach($paramPesquisa as $item) {?>
						  <option value="<?=$item[0]?>" <?=(($item[0] == $tipoPesquisa) ? 'selected':'')?>><?=$item[1]?></option>
						<?php }?>  
					  </select>
					</div>
                </div>					
				<div class="col-sm-8">
                    <label for="Nome">Texto para pesquisa</label>
					<div class="form-group">
						<input class="form-control somente-numero" name="textoPesquisa" id="textoPesquisa" value="<?=$textoPesquisa?>" />  
					</div>
                </div>				
				<div class="col-sm-2">
					<label for="Nome"> &nbsp; </label>
					<div class="form-group text-right">
						<button type="button" onclick="return enviaFormularioSimples('formPesquisa')" class="btn btn-primary" > Localizar </button>
					</div>						
				</div>  							
            </div>
        </form>		
    </div>
	
    <div class="panel panel-default">
        <div class="panel-heading ">
			<strong>Listagem das Compras</strong>
        </div>
		<?php if($dadosCompraValidar) {?>
			<input class="form-control" id="buscarModelo" type="text" placeholder="Buscar..">
		<?php }?>	
        <div class="table-responsive" style="overflow-y: auto; max-height:350px;">
            <table class="table table-hover table-striped sortable"  >
                <thead>
                    <tr>
                        <th><strong>Celular</strong></th>
                        <th><strong>Usuário</strong></th>
                        <th><strong>CPF</strong></th>
                        <th><strong>Produto</strong></th>
                        <th><strong>Data Compra</strong></th>
                        <th><strong>Parceiro</strong></th>
                        
                        <th><strong></strong></th>						
                    </tr>
                </thead>                
                <tbody id="tableModelo"> 
                    <?php foreach($dadosCompraValidar as $item) {?>
                    <tr>
                        <td><?=formatarTelefone($item['celular'])?></td>
                        <td><?=$item['nome']?></td>
                        <td><?=formatarCPForCNPJ($item['cpf'])?></td>
                        <td><?=$item['nomeProduto']?></td>
                        <td><?=date_dd_mm_yyyy_hh_mm_ss($item['dataCompra'])?></td>
                        <td><?=$item['nomeParceiro']?></td>                        
                        <td>     
                            <button class="btn btn-primary" data-toggle="modal" data-target="#ModeloValidacao<?=$item['idCompra']?>" title="Validar Compra" >Validar</button>                                                                                
                        </td>
                    </tr>
                    <?php $numeroLinhas++; }?>
                </tbody>
            </table>
        </div>
        <br>
        <div class="panel-footer">
            <?=$numeroLinhas." Registros encontrados..."?>
			<label for="Nome"> &nbsp; </label>
			<div class="form-group text-right">
				* Na ausência de texto para pesquisa, os dados mostrados na tela serão as doações realizadas até o dia corrente.
			</div>	
        </div>
    </div>    
</div>
<?php
    include "validar.validacao.view.php";
?>


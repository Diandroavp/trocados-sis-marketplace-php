 <div class="<?=$classMensagem?> alert-dismissible" style="position: absolute;  width:100%; margin-top:50px; text-align: center; z-index: 5555;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <?=$textMensagem?>
  </div>
  <script>
	$(".alert-dismissible").fadeTo(6000, 500).slideUp(1000, function(){
		$(".alert-dismissible").alert('close');
	});
  </script>
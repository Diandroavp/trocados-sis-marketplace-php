<?php

    function echos($valor){
        echo('<br>');
        echo('<br>');
        echo('<br>');
        echo('<br>');
        echo('<br>');
        echo('<br>');
        echo('<br>');
        echo('<br>');
        echo($valor);
    }

    function print_rs($valor){
        echo('<br>');
        echo('<br>');
        echo('<br>');
        echo('<br>');
        echo('<br>');
        echo('<br>');
        echo('<br>');
        echo('<br>');
        print_r($valor);
    }

    function simOuNao($valor) {
        if ($valor == 1){
            return 'SIM';
        } else if ($valor == 0){
            return 'NÃO';
        } else {
            return '-';
        }
    }

    function retorna_url_imagem($imagem) {
        if ( $imagem == '' ) {
            return _geturlbase()."/images/fotopadrao.jpg";
        } else {
            return _geturlbase().'/admin/imagens/'.$imagem;
        }
    }

    //function utf8($txt) {
    //	return mb_convert_encoding($txt, "UTF-8", "HTML-ENTITIES");
    //}
    //function iso8859($txt) {
    //	return mb_convert_encoding($txt, "ISO-8859-1", "HTML-ENTITIES");
    //}

    
function convertDataParaBanco($date, $formato = null, $time=false){
	
	if($date <> ''){
		global $formatoDataBanco;
		 
        if ($formato <> null){
            $padraoFormato = $formato;
        } else {
            $padraoFormato = $formatoDataBanco;
        }
		 
	  /*
	   Datas que funcionam...
	   1 ) '15-05-1990'
	   2 ) '1990-05-15'
	   
	   3) '05/15/1990'
	   4) '1990/05/15'   
	  */ 
	  
	  $dataFormatada = '';
	  $dataDia = '';
	  $dataMes = '';
	  $dataAno = '';
	  
	  
	  if (strpos($date, '-') > 0){
	   /*1 ) '15-05-1990'*/
	   if (substr($date,2,1) == '-') {
		if ((substr($date,3,2) <= 12)){
		 $dataFormatada = date($padraoFormato, strtotime($date));
		} else {
		 /*05-15-1990*/
		 $dataDia = substr($date,3,2);
		 $dataMes = substr($date,0,2);
		 $dataAno = substr($date,6,4);

		 $hora = substr($databanco,11,2);
		 $min = substr($databanco,14,2);
		 $seg = substr($databanco,17,4);
		 
		 if ($time) {
			$dataFormatada = date($padraoFormato, strtotime($dataDia.'-'.$dataMes.'-'.$dataAno.'T'.$hora.':'.$min.':'.$seg));     
		 } else {
			$dataFormatada = date($padraoFormato, strtotime($dataDia.'-'.$dataMes.'-'.$dataAno));   
		 }
		 
		}    
	   } 
	   /*2 ) '1990-05-15'*/
	   elseif(substr($date,4,1) == '-'){
		if ((substr($date,5,2) <= 12)){
		 $dataFormatada = date($padraoFormato, strtotime($date));
		} else {
		 /*1990-15-05*/
		 $dataDia = substr($date,5,2);
		 $dataMes = substr($date,8,2);
		 $dataAno = substr($date,0,4);

		 $hora = substr($databanco,11,2);
		 $min = substr($databanco,14,2);
		 $seg = substr($databanco,17,4);
		 
		 if ($time) {
		 	$dataFormatada = date($padraoFormato, strtotime($dataDia.'-'.$dataMes.'-'.$dataAno.'T'.$hora.':'.$min.':'.$seg));     
		 } else {
			$dataFormatada = date($padraoFormato, strtotime($dataDia.'-'.$dataMes.'-'.$dataAno));
		 }
		}    
	   }  
	  } elseif (strpos($date, '/') > 0){ 
	   /*3) '05/15/1990'*/
	   if (substr($date,2,1) == '/') {
		if ((substr($date,3,2) <= 12)){
		 $dataFormatada = date($padraoFormato, strtotime($date));
		} else {
		 /*05/15/1990*/
		 $dataDia = substr($date,3,2);
		 $dataMes = substr($date,0,2);
		 $dataAno = substr($date,6,4);

		 $hora = substr($databanco,11,2);
		 $min = substr($databanco,14,2);
		 $seg = substr($databanco,17,4);
		 
		 if ($time) {
		 	$dataFormatada = date($padraoFormato, strtotime($dataDia.'-'.$dataMes.'-'.$dataAno.'T'.$hora.':'.$min.':'.$seg));     
		 } else {
			$dataFormatada = date($padraoFormato, strtotime($dataDia.'-'.$dataMes.'-'.$dataAno));   
		 }
		}    
	   } 
	   /*4) '1990/05/15'*/
	   elseif(substr($date,4,1) == '/'){
		if ((substr($date,5,2) <= 12)){
		 $dataFormatada = date($padraoFormato, strtotime($date));
		} else {
		 /*1990/15/05*/
		 $dataDia = substr($date,5,2);
		 $dataMes = substr($date,8,2);
		 $dataAno = substr($date,0,4);  
		 
		 $hora = substr($databanco,11,2);
		 $min = substr($databanco,14,2);
		 $seg = substr($databanco,17,4);
		 
		 if ($time) {
		 	$dataFormatada = date($padraoFormato, strtotime($dataDia.'-'.$dataMes.'-'.$dataAno.'T'.$hora.':'.$min.':'.$seg));     
		 }else {
			$dataFormatada = date($padraoFormato, strtotime($dataDia.'-'.$dataMes.'-'.$dataAno));     
		 }
		}    
	   }  
	  }  
	  
	return $dataFormatada;  } else { return null; }	
 }

 function date_yyyy_mm_dd_hh_mm_ss($data,$seconds=true){
	 global $formatoDataBanco;
	if ($data != "") {
		$databanco = $data;
		$month = substr($databanco,5,2);
		$date = substr($databanco,8,2);
		$year = substr($databanco,0,4);
		
		$hour = substr($databanco,11,2);
		$min = substr($databanco,14,2);
		$seg = substr($databanco,17,4);

		if ($seconds){
			return date('Y-m-d\TH:i:s',strtotime($year."-".$month."-".$date.'T'.$hour.':'.$min.':'.$seg));			
		} else {
			return $year."-".$month."-".$date.' '.$hour.':'.$min;
		}
		
	} else {
		return "";
	}
}


function _date_to_hour($data,$seconds=true){
	$databanco = $data;
    $hour = substr($databanco,10,3);
    $minute = substr($databanco,13,3);
	
    $second = substr($databanco,16,3);
	//
	if ($seconds) {
		$result = $hour.$minute.$second;
	} else {
		$result = $hour.$minute;
	}
	//
	return $result;
	//1899-12-30 18:20:00.000
}



function formatar_moeda($valor,$casas=null){
	if ($valor != ""){

		$valor = "R$ ".number_format($valor,$casas);

		$valor = str_replace(",", "#", $valor);
		$valor = str_replace(".", ",", $valor);
		$valor = str_replace("#", ".", $valor);

		return $valor;
	}else {
		return "-";
	}
}

function formatar_percentual($valor,$casas=null){
	if ($valor != ""){

		$valor = number_format($valor,$casas)."%";

		$valor = str_replace(",", "#", $valor);
		$valor = str_replace(".", ",", $valor);
		$valor = str_replace("#", ".", $valor);

		return $valor;
	}else {
		return "-";
	}	
}

function formatar_numero($valor,$casas=null){
	if ($valor == "0"){
		return "0,00";
	}else if ($valor != ""){

		$valor = number_format($valor,$casas);

		$valor = str_replace(",", "#", $valor);
		$valor = str_replace(".", ",", $valor);
		$valor = str_replace("#", ".", $valor);

		return $valor;
	}else {
		return "-";
	}

}

function formatar_datetime($data){
	global $formatoDataBanco;
	
	$dataRetorno = '';
	
	if ($data <> ''){
		$myDateTime = DateTime::createFromFormat('Y-m-d', $data);
		$dataRetorno = $myDateTime->format($formatoDataBanco);
	}
	
	return $dataRetorno;
}


function date_dd_mm_yyyy($data){
	if ($data != "") {
		$databanco = $data;
		$month = substr($databanco,5,2);
		$date = substr($databanco,8,2);
		$year = substr($databanco,0,4);

		return $date."/".$month."/".$year;
	} else {
		return "";
	}
}


function date_dd_mm_yyyy_hh_mm_ss($data){
	if ($data != "") {
		$databanco = $data;
		$month = substr($databanco,5,2);
		$date = substr($databanco,8,2);
		$year = substr($databanco,0,4);

		$min = substr($databanco,11,2);
		$seg = substr($databanco,14,2);
		$mseg = substr($databanco,17,4);


		return $date."/".$month."/".$year.' '.$min.':'.$seg.':'.$mseg;
	} else {
		return "";
	}
}

function shortentext($text, $chars) {
       // Change to the number of characters you want to display
   if (strlen($text) > $chars) {
	    $text = str_replace("\n","",$text);
        $text = $text." ";
        $text = substr($text,0,$chars);
        $text = substr($text,0,strrpos($text,' '));
        $text = $text."...";
   }
   return $text;
}



function formatarCPForCNPJ($numero = '00000000000'){

    if (strlen($numero) <= 11) {
        $parte_um     = substr($numero, 0, 3);
        $parte_dois   = substr($numero, 3, 3);
        $parte_tres   = substr($numero, 6, 3);
        $parte_quatro = substr($numero, 9, 2);

        return "$parte_um.$parte_dois.$parte_tres-$parte_quatro";
    } else if (strlen($numero) <= 14) {
        $parte_um     = substr($numero, 0, 2);
        $parte_dois   = substr($numero, 2, 3);
        $parte_tres   = substr($numero, 5, 3);
        $parte_quatro = substr($numero, 8, 4);
        $parte_cinco  = substr($numero, 12, 2);

        return "$parte_um.$parte_dois.$parte_tres/$parte_quatro-$parte_cinco";
    }
	

}


function formatarTelefone($numero = '00000000000'){
	$parte_um     = substr($numero, 0, 2);
	$parte_dois   = substr($numero, 2, 5);
	$parte_tres   = substr($numero, 7, 4);

	return  "($parte_um)$parte_dois-$parte_tres";

}

function formatarCEP($numero = '00000000'){

	$parte_um     = substr($numero, 0, 5);
	$parte_dois   = substr($numero, 5, 3);

	return  "$parte_um-$parte_dois";

}




function limitarTexto($texto, $limite){
	$contador = mb_strlen($texto);

	if ( $contador >= $limite ) {
		$texto = mb_substr($texto, 0, mb_strrpos(mb_substr($texto, 0, $limite), ' '), 'UTF-8') ;
		return $texto;
	}
	else{
		return $texto;
	}
}


function limpaCaracter($valor){
 $valor = trim($valor);
 $valor = str_replace(".", "", $valor);
 $valor = str_replace(",", "", $valor);
 $valor = str_replace("-", "", $valor);
 $valor = str_replace("/", "", $valor);
 $valor = str_replace("(", "", $valor);
 $valor = str_replace(")", "", $valor);
 return $valor;
}


function tipoContato($valor) {
	if ($valor == 1){
		return 'Financeiro';
	} else if ($valor == 2){
		return 'CEO';
	} else {
		return 'OUTROS';
	}
}


?>
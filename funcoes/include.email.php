<?php

//OBSERVACAO.. PRECISA DO OPENSSL

	global 	$host_email,
			$port_email,
			$Operador_email,
			$senha_email,
			$email_remetente,
			$nome_remetente;

	include 'funcoes/PHPMailer/PHPMailerAutoload.php';

	$Mailer = new PHPMailer();

	//Define que será usado SMTP
	$Mailer->IsSMTP();

	//Enviar e-mail em HTML
	$Mailer->isHTML(true);

	//Aceitar carasteres especiais
	$Mailer->Charset = 'UTF-8';

	//Configurações
	$Mailer->SMTPAuth = true;
	$Mailer->SMTPSecure = 'ssl';

	//nome do servidor
	$Mailer->Host = $host_email;//'smtp.gmail.com';
	//Porta de saida de e-mail
	$Mailer->Port = $port_email;//465;

	//Dados do e-mail de saida - autenticação
	$Mailer->Username = $Operador_email;//'indicadores@zoetecnologia.com.br';
	$Mailer->Password = $senha_email;//'zoe@indicador';

	//E-mail remetente (deve ser o mesmo de quem fez a autenticação)
	$Mailer->From = $email_remetente;//'indicadores@zoetecnologia.com.br';

	//Nome do Remetente
	$Mailer->FromName = ($nome_remetente);//'Administrador';

	//Assunto da mensagem
	$Mailer->Subject = ($titulo);//'Titulo - Recuperar Senha';

	//Corpo da Mensagem
	$Mailer->Body = ($conteudo);//'Conteudo do E-mail em Html';

	//Corpo da mensagem em texto
	$Mailer->AltBody = $conteudo_texto;//'conteudo do E-mail em texto';

	//Destinatario
	$Mailer->AddAddress($destinatario);

	if($Mailer->Send()){
		$retorno = true;
	}else{
		$retorno = false;
	}

?>

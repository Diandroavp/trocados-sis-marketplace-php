<!-- Esse trecho é importante para colocar os IDs e Nomes para os Inputs -->
	<div class="modal fade z_index" data-backdrop="static" id="ModeloEditarImagem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index:9999;">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">				
					<div class="modal-header bg-primary">
						<button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×   </span><span class="sr-only">Fechar</span>

						</button>
						<h4 class="modal-title" id="myModalLabel">Edição de Imagem</h4>
					</div>
					<div class="modal-body">
                        <?php
                            include "include.imagem.php";
                        ?>
                    </div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>												
						<button type="button" class="btn btn-success" data-dismiss="modal" onclick="levarImagem()">
							<span class="docs-tooltip" data-toggle="tooltip" >
							Salvar Mudanças
							</span>
						</button>
					</div>				
			</div>
		</div>
	</div>

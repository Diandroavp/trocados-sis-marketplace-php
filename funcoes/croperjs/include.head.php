<head>
   <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1"></META>
   <meta Developer="Diandro Augusto - ZoeTecnologia.com.br" />
   <meta author="ZoeTecnologia.com.br" />
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <meta charset="utf-8">
   <meta http-equiv="x-ua-compatible" content="ie=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <meta name="description" content="JavaScript image cropper.">
   <meta name="author" content="Chen Fengyuan">
   <title>Cropper.js</title>
   <!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.0/css/all.css" crossorigin="anonymous">
   <link rel="stylesheet" href="https://unpkg.com/bootstrap@4/dist/css/bootstrap.min.css" crossorigin="anonymous">-->
   <link rel="stylesheet" href="./funcoes/croperjs/css/cropper.css">
   <link rel="stylesheet" href="./funcoes/croperjs/css/main.css">
</head>

